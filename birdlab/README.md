### Birdlab

---

Birdlab je *hypervisor*, který vytváří on-demand testovací prostředí pro Birda.
Podle definičního souboru vytvoří a spustí sadu virtuálních strojů, spojí je do
sítí podle definice a nastaví příslušným rozhraním IP adresy.

Celé prostředí sídlí v `ROOT=/var/lib/virt`, definice prostředí v 
`$ROOT/etc/virtlist`.

S ohledem na malý počet členů týmu návrh systému neřeší kolize mezi
jednotlivými uživateli.

<br>

#### Ukázková konfigurace

```
# Tento host slouží k instalaci a nastavování image OpenBSD 5.8
Host openbsd58-install
  Image openbsd58
  Memory 512M
  VNC 52 # Je přístupný na IP adrese hypervizora na VNC portu 52
  Type openbsd

# Tento host je konfigurační template pro vytváření dalších hostů s OpenBSD 5.8
Host openbsd58
  Image openbsd58
  Memory 512M
  Type openbsd
  Cow yes # Všechny změny na disku zapisuj do paměti a po ukončení zahoď

Host freebsd10-install
  Image freebsd10
  Memory 512M
  VNC 51
  Type freebsd

Host freebsd10
  Image freebsd10
  Memory 512M
  Type freebsd
  Cow yes

Host debian8-install
  Image debian8
  Memory 512M
  VNC 11
  Type linux

Host debian8
  Image debian8
  Memory 512M
  Type linux
  Cow yes

Host debian5-install
  Image debian5
  Memory 512M
  VNC 12
  Type linux

Host debian5
  Image debian5
  Memory 512M
  Type linux
  Cow yes

# Naklonuj konfiguraci hosta openbsd58 (3x) a vytvoř tak hosty ob1, ob2, ob3
Copy openbsd58 ob1 ob2 ob3
Copy freebsd10 fb1 fb2 fb3
Copy debian8   d1  d2  d3  d4

# Naklonuj hosta debian8 a vytvoř minidebian; uprav nastavení klonu
Copy debian8 minidebian
Host minidebian
  Memory 64M

# Vytvoř 6 dalších debianů s 64M paměti
Copy minidebian md1 md2 md3 md4 md5 md6

# Vytvoř síťový segment, připoj na něj uvedené hosty a nastav jim příslušné IP adresy
Net md1 192.168.1.1/24 md2 192.168.1.2/24 md3 192.168.1.3/24 md4 192.168.1.4/24 md5 192.168.1.5/24 md6 192.168.1.6/24

# Vytvoř p2p linky mezi uvedenými dvojicemi hostů
Link fb1 192.168.55.64/32 d1 192.168.55.71/32
Link ob1 192.168.55.77/32 d1 192.168.55.33/32
Link ob1 192.168.55.17/32 fb1 192.168.55.241/32

Link d2 192.168.55.15/32 d3 192.168.55.117/32

Link d3 192.168.93.15/32 d4 192.168.15.93/32

Net fb1 192.168.44.1/24 fb2 192.168.44.2/24 fb3 192.168.44.3/24
Net d1 192.168.66.1/24 d2 192.168.66.2/24 d3 192.168.66.3/24
Net ob1 192.168.77.1/24 ob2 192.168.77.2/24 ob3 192.168.77.3/24
```

<br>

#### Instalace nového virtuálu

---

1. Stáhnout `.iso` do `/var/lib/virt/iso` (typ souboru **..disc1.iso** nebo
**bootonly.iso**),
2. vytvořit nový image pomocí `qemu-img create -f qcow2
/var/lib/virt/img/<host> 40G`,
3. přidat do `etc/virtlist` (viz. vzorová konfigurace) instalační a spouštěcí
hosty,
4. spustit instalaci `vctl start <host>-install -cdrom
/var/lib/virt/iso/<.iso>`,
5. spustit virtuál a přes VNC instalovat,
6. upravit soubor `sshd_config`, `PermitRootLogin yes`,
7. po instalaci ručně pustit příkaz `vctl postinst <host>-install`.

<br>

Manipulaci s virtuály zajišťuje příkaz `vctl`:
`vctl prepare` vytvoř potřebná rozhraní, spusť potřebné daemony
`vctl status` vypiš stav všech virtuálů
`vctl cleanup` zruš rozhraní a vypni daemony, ukliď po sobě; nevypíná virtuály
`vctl start <host> [args...]` nastartuj virtuál <host>; argumenty navíc
se předají přímo Qemu.
`vctl ssh <host>` připoj se na terminál <host>ovi
`vctl mon <host>` otevři monitorovací socket Qemu
`vctl stop <host>` vypni virtuál <host>; pokouší se o to několika způsoby,
poslední je sigkill.

Virtuály běží v Qemu a jejich síťová rozhraní jsou připojená do openVswitche
(s tím se dá povídat přes ovs-vsctl, základní příkaz je ovs-vsctl show).

Linky a sítě jsou vytvořené jako jednotlivé VLANy v OVS, tag se generuje
hashovací funkcí z definice sítě.

Každý virtuál má k dispozici `/mnt/nfs`, kam se namontuje `$ROOT/nfs`.
Při startu se spustí `/mnt/nfs/rc.local`. V základu se tímto skriptem nastaví
sítě. Adresář `$ROOT/nfs/net` obsahuje další důležité soubory definující
nastavení sítě v závislosti na MAC adrese virtuálního rozhraní.

Je-li tedy třeba po startu také přeložit Birda a spustit jej se správným
configem, `$ROOT/nfs/rc.local` je správné místo pro zavolání takové věci.

TODO: IPv6
