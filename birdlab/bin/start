#!/bin/bash
. $(dirname $(readlink -f $0))/virt-lib

NAME=$1
shift

usage() {
	echo -e "Usage: $0 name [custom options for qemu]\n    or $0 -m list of names\n    or $0 -t name suffix [custom options for qemu]"
	exit 2
}

check_stale() {
  local NAME=$1
  if [ -e $ROOT/run/$NAME.pid ]; then
    echo -n "Host $NAME pidfile found"
    if [ -d /proc/$(<$ROOT/run/$NAME.pid) ]; then
      echo " running with PID $(<$ROOT/run/$NAME.pid)"
      exit 1
    else
      echo " stale ... removing"
      rm $ROOT/run/$NAME.pid
    fi
  fi
}
[ -z "$NAME" ] && usage

if [ "$NAME" = "-m" ]; then
	set -e
	for N in "$@"; do
		echo Starting "$N"
		$0 "$N"
	done
	exit 0
fi

if [ "$NAME" = "-t" ]; then
  NAME=$1
  SUFFIX=$2
  ([ -z "$SUFFIX" ] || [ -z "$NAME" ]) && usage
  ID=0
  if [ -e $ROOT/run/tmp-$NAME-$SUFFIX ]; then
    echo "Host $NAME tmpfile found."
    check_stale $NAME-$SUFFIX
    rm $(readlink -f $ROOT/run/tmp-$NAME-$SUFFIX) $ROOT/run/tmp-$NAME-$SUFFIX
  fi

  while [ -e $ROOT/run/tmp-$ID ]; do
    ID=$((ID+1))
  done
  HEXID=$(printf "%02x" $ID)

  echo "$SUFFIX" > $ROOT/run/tmp-$ID
  ln -s tmp-$ID $ROOT/run/tmp-$NAME-$SUFFIX
  shift 2
else
  check_stale $NAME
fi

NAME=$NAME${SUFFIX:+-$SUFFIX}
gethost $NAME

if [ "$?" != 0 ]; then
	echo "Host $NAME not configured"
	exit 2
fi

if [ "$1" = "--install" ]; then
	INSTALL="-cdrom $2 -boot d"
	shift 2
fi

RCGFILE=$ROOT/nfs/rc/rc-gen-$HOSTNAME
truncate -s0 $RCGFILE

declare -a HOSTNICVLAN
if [ -n "$HOSTNIC" ]; then
	declare -a HOSTNIC=( $HOSTNIC )
	for N in ${HOSTNIC[@]}; do
		IFS=';' read NETNAME MAC TAG IP4 IP4PEER <<<"$N"
		TAPID=$(mactotap $MAC)
		HOSTNICDEV="$HOSTNICDEV -netdev tap,id=$TAPID,ifname=$TAPID,script=no -device e1000,netdev=$TAPID,mac=$MAC"
    echo "N=\$(ifname \${IFBYMAC[$MAC]} $NETNAME)" >>$RCGFILE
    echo "ifnameset $MAC \$N" >>$RCGFILE
    if [ -n "$IP4PEER" ]; then
      echo "ip4ptp \$N $IP4 $IP4PEER" >>$RCGFILE
    else
      echo "ip4 \$N $IP4" >>$RCGFILE
    fi
    echo "ifup \$N" >>$RCGFILE

		HOSTNICVLAN+=( $TAPID,$TAG )
	done
fi

if [ -n "$HOSTVARS" ]; then
	declare -a HOSTVARS=( $HOSTVARS )
	for V in ${HOSTVARS[@]}; do
		echo "export $V" >> $RCGFILE
	done
	declare -a HOSTVARNAMES
	for V in ${HOSTVARS[@]}; do
		HOSTVARNAMES+=( ${V%%=*} )
	done
	echo "declare -a VARNAMES=(${HOSTVARNAMES[@]})" >> $RCGFILE
fi

if [ -n "$HOSTDUMMY" ]; then
	declare -a HOSTDUMMY=( $HOSTDUMMY )
	for D in ${HOSTDUMMY[@]}; do
		IFS=';' read DNAME IP4 IP6 <<<"$D"
    echo "N=\$(dummy $DNAME)" >>$RCGFILE
    echo "INTERFACES+=(\$N)" >>$RCGFILE
    echo "ip4 \$N $IP4" >>$RCGFILE
    echo "ip6 \$N $IP6" >>$RCGFILE
	done
fi

if [ -n "$HOSTGRE" ]; then
	declare -a HOSTGRE=( $HOSTGRE )
	for G in ${HOSTGRE[@]}; do
		IFS=';' read GRENAME PEER IP4 IP4PEER <<<"$G"
    echo "N=\$(gre $GRENAME $HOSTIP4 ${CONF[$PEER;IP4]})" >>$RCGFILE
    echo "INTERFACES+=(\$N)" >>$RCGFILE
    echo "ip4ptp \$N $IP4 $IP4PEER" >>$RCGFILE
	done
fi

if [ -n "$HOSTSOCK" ]; then
  declare -a HOSTSOCK=( $HOSTSOCK )
  for S in ${HOSTSOCK[@]}; do
    IFS=';' read SOCKNAME APPEND MAC IP4 <<<"$S"
		TAPID=$(mactotap $MAC)
    HOSTNICDEV="$HOSTNICDEV -netdev socket,$APPEND,id=$SOCKNAME -device e1000,netdev=$SOCKNAME,mac=$MAC"
    echo "N=\$(ifname \${IFBYMAC[$MAC]} $SOCKNAME)" >>$RCGFILE
    echo "IFBYMAC[$MAC]=\$N" >>$RCGFILE
    if [ -n "$IP4PEER"]; then
      echo "ip4ptp \$N $IP4 $IP4PEER" >>$RCGFILE
    else
      echo "ip4 \$N $IP4" >>$RCGFILE
    fi
    echo "ifup \$N" >>$RCGFILE
  done
fi

echo -e "hostname $HOSTNAME\nexport HOSTNAME=$HOSTNAME" > $ROOT/nfs/net/hostname-${HOSTMAC//:}

$ROOT/bin/omapi add $NAME $HOSTMAC $HOSTIP4

qemu-system-$HOSTARCH -enable-kvm -m $HOSTMEMORY -hda $ROOT/img/$HOSTIMAGE $HOSTCOW -display none ${HOSTVNCID:+-vnc 172.20.20.180:$HOSTVNCID} -netdev tap,id=ctl,ifname=$HOSTTAP,script=no -device e1000,netdev=ctl,mac=$HOSTMAC $HOSTNICDEV -pidfile $ROOT/run/$NAME.pid -monitor unix:$ROOT/run/$NAME.sock,server,nowait -serial unix:$ROOT/run/$NAME.serial,server,nowait -daemonize $INSTALL "$@"
#-runas $VIRTUSER

ip link set $HOSTTAP up
vsctl add-port $VIRTBR $HOSTTAP tag=1
echo vsctl add-port $VIRTBR $HOSTTAP tag=1

for T in ${HOSTNICVLAN[@]}; do
	IFS=, read TAPID TAG <<<"$T"
	ip link set $TAPID up
  if ip link show br-$TAG 2>/dev/null >/dev/null; then :; else
    brctl addbr br-$TAG
    ip link set br-$TAG up
  fi
  echo "brctl addif br-$TAG $TAPID"
  brctl addif br-$TAG $TAPID
#	vsctl add-port $VIRTBR $TAPID tag=$TAG
done
