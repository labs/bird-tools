log "multitab.log" all;
debug protocols all;

router id 1.1.1.1;

protocol static {
  ipv4 { import all; };
  route 1.1.1.1/32 via 55.55.55.5;
  route 2.2.2.2/32 via 55.55.55.5;
  route 6.5.4.2/32 unreachable;
}

protocol static {
  ipv6 { import all; };
  route 3f3f:5::/64 via fe80::d4c9:31ff:fedd:bfb5%du;
}

vpn4 table v4;
vpn6 table v6;

protocol static {
  vpn4;
  route 3:5 7.7.7.7/32 via 55.55.55.5 mpls 55;
  route 4:4 6.6.0.0/16 via 55.55.55.5 mpls 55;
}

protocol static zezeze {
  vpn6;
  route 4:4 2001:0:66::/48 via 55.55.55.5 mpls 55;
  route 5:6 2001:0:66::/48 via 55.55.55.5 mpls 55;
  route 7:8 3f3f:5::/64 via fe80::d4c9:31ff:fedd:bfb5%du mpls 48;
  route 7:7 ::/64 via 55.55.55.5 mpls 33;
}

protocol device {}

protocol bfd {
  interface "du" {};
}

protocol bgp {
  ipv4 { export all; import all; };
  ipv6 { export all; import all; };
  bfd;
  vpn4 mpls { export all;  import all;};
  neighbor 55.55.55.99 as 65555;
  local 55.55.55.55 as 65544;
}

protocol bgp {
  ipv4 { export all; import all; };
  ipv6 { export all; import all; };
  bfd;
  vpn4 mpls { export all;  import all;};
  neighbor 55.55.55.55 as 65544;
  local 55.55.55.99 as 65555;
}

protocol babel {
  ipv4 { import all; export all; };
  interface "du" {};
}

protocol ospf {
  ipv4 { import all; export all; };
  area 0 { interface "du" { }; };
}

ipv4 table bagr;

protocol static {
  ipv4 { table bagr; import all; };
  route 3.1.1.1/32 via 55.55.55.5;
  route 4.2.2.2/32 via 55.55.55.5;
}

flow4 table f4;

protocol static {
  flow4;
  
  route flow4 {
    dst 10.0.0.0/8;
    proto = 23;
    dport > 24 && < 30 || 40..50,60..70,80;
    sport > 24 && < 30 || = 40 || 50,60..70,80;
    icmp type 80;
    icmp code 90;
    tcp flags 0x03/0x0f;
    length 2048..65535;
    dscp = 63;
    fragment dont_fragment, is_fragment || !first_fragment;
  } action { rate 64 GBps; };

  route flow4 {
    dst 12.0.0.0/32;
    tcp flags ! 0/0x999;
  } action { rate 6666 kbps; };

  route flow4 {
    dst 220.0.254.0/24;
    tcp flags 0x99/0x999;
  } action { last; rt 6145, 124395; };

  route flow4 {
    dst 220.0.254.192/28;
    tcp flags ! 0xfff/0xfff;
  } action { sample; dscp 13; };

  route flow4 {
    dst 15.0.0.0/8;
    tcp flags ! 0x999/0x999;
  } action { rate 123456 mBps; last; sample; };

  route flow4 {
    dst 11.0.0.0/8;
    proto = 0x12;
    sport > 0x5678 && < 0x9abc || 0xdef0 || 0x1234,0x5678,0x9abc..0xdef0;
    dport = 50;
    tcp flags 0x000/0xf00;
  } action { rate 1084436 mbps; };
}
