## Netlab

This tool focuses on testing of designed [BIRD](https://gitlab.nic.cz/labs/bird)
configurations and their topologies. There are many different default
[test-cases](#-test-case) that support different protocols (OSPFv2, OSPFv3,
iBGP, eBGP, etc.).

<br>

### Test-case

---

In general, test-case represents specific testing scenario. Each case has its
own configuration file, [test-suite](#-test-suite) and configuration files of
individual devices.

In Netlab test-cases are folders inside the root directory. Each test-case
represents a simplified model of a real topology. At the same time the test-case
folder contains a folder with saved outputs.

Each device is named according to this convention; name `m` and number
of device (mostly `1 ~ 8`)

<br>

#### Current cases

---

All currently available cases:
- `cf-babel-auth`, tests the *authentication* option for the *babel* protocols,
- `cf-babel-base`, tests the basic topology with *babel* protocols,
- `cf-bgp-auth`, tests the matching/different passwords among the *ebgp*
protocols,
- `cf-bgp-base`, tests the basic topology with *ibgp* and *ebgp* protocols,
- `cf-bgp-int`, tests the protocols inside the templates, part of systems uses
IPv4, part uses IPv6,
- `cf-bgp-merged`, tests the *merge paths* option within the *ebgp*/*ibgp*
topology,
- `cf-ebgp-loop`, tests the *ebgp* protocols within the circle topology,
- `cf-ebgp-star`, tests the *ebgp* protocols with one central device,
- `cf-ibgp-flat`, tests the *ibgp* protocols interconnected within single
autonomous system,
- `cf-ibgp-loop`, tests the *ibgp* protocols within the circle topology,
- `cf-ospf-area`, tests the *ospf* protocols among different areas,
- `cf-ospf-authentication`, tests the *authentication* option for the *ospf*
protocols,
- `cf-ospf-base`, tests the basic topology with *ospf* protocols,
- `cf-ospf-bfd`, tests the *bfd* option within the *ospf* protocols,
- `cf-ospf-custom`, tests additional options *real broadcast*, *ttl security*,
*link lsa suppression* for the *ospf* protocols,
- `cf-ospf-default`, tests the topology, where the part of the device has
default values, part has not.
- `cf-ospf-nbma`, tests the *nbma* networks (also option *strict nbma*) within
the topology,
- `cf-ospf-priority`, tests the *ospf* protocols with explicitly set values of
*priority* option,
- `cf-ospf-ptmp`, tests the *ptmp* networks within the topology,
- `cf-ospf-vrf`, tests the topology with three different *vrf* cycles with
single common device,
- `cf-rip-base`, tests the basic topology with *rip* protocols.

<br>


#### Case structure

---

```
  /netlab
    ├─cf-<name_1>/
    | ├─bird_m1.conf
    | ├─bird_m2.conf
    | ├─bird_m3.conf
    | ├─bird_m4.conf
    | ├─config
    | ├─data/
    | └─test-cf-<name_1>.py
    ├─cf-<name_2>
```

- Bird config files - `bird_m1`, `bird_m2`, `bird_m3`, `bird_m4`
- Netlab config file - `config`
- Test-suite - `test-<name_1>.py`
- Saved data - `data/`

<br>

##### Netlab configuration

---

The main file of each case is configuration file (`config`). The file contains
many variables:

- `ve` - name of the interface
- `vrf` - create an interface for VRF domains
- `if_net` - create an Ethernet bridge device
- `if_link`- create an virtual LAN interface
- `if_veth` - create an virtual ethernet interface
- `if_dummy` - create a dummy network interface
- `NETLAB_NODES` - contains all device names in the current case
- `netlab_init` - initialize devices in `NETLAB_NODES`
- `netlab_start` - start creating all devices

<br>

##### Test-suite

---

In general, test-suite is file that contains testing functions. In Netlab, 
test-suite is a Python file with functions that tests the given case.
There are usually default tests and individual tests.

In addition, there is contionuous check of the type of log messages.
Only the types **DBG**, **INFO**, **TRACE** are allowed (with optional
**<WARN>** `cannot find next hop for LSA`)

<br>


##### Test outputs

---

After running the test case in **save** mode, there is new folder `data`. This
folder contains saved testing tables. They will be used for checking data in
**check** mode.

Contained files:
- `krt<4|6>-<device>` - saved content of kernel tables,
- `master<4|5|6>-<device>` - saved content of Bird tables.

<br>

##### Configuration files

---

These files contains configuration of each device. Definitions of all protocols
in the single router, router id, master tables.

<br>

### Usage

---

Netlab works by default in two different modes:
1. [Standard mode](#standard-mode)
2. [Testing mode](#testing-mode)

<br>

#### Standard mode

---

The purpose of the standard mode is to run and stop the prepared
configuration.

<br>

**start** - start the standard mode run following as root:
```
./start -c <test-case>
```
Via inserting `test-case` parameter to the `-c` switch runs the selected
[case](#-test-cases).

<br>

**stop** - for completing the standard mode run as root:
```
./stop
```

<br>

#### Testing mode

---

During this process, the [test-suits](#test-suit) will run in two separate
scenarios.

<br>

**save** - to run this process, run as root:
```
./runtest -m save <test-case>
```
After the completion the output files will be saved into the `data` folder
(inside the test-case). These results will be used to compare the output in the
next step.

<br>

**check** - run as root:
```
./runtest -m check <test-case>
```
It compares the outputs from **save** step with the current outputs that are
temporarilly saved inside the `temp` folder.

