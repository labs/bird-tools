<!--cf-babel-auth-->
##### Introduction
This case tests the **authentication** option for the **babel** protocols.
Option `key` also specifes an encryption method. There are used different
authentication methods within the topology (e.g. `algorithm hmac sha256`,
`algorithm hmac sha512`, `algorithm blake2s256`).

Furthermore, this test case has three successive stages:
1. testing the original settings,
2. reconfiguration with another config files (suffix `_v2`) and testing them,
3. run bird command `configure undo` and testing the original settings.

There are four central devices. Each device has one neighbor outside
the central network.

There are protocols **direct**(neccessary for announcing) and protocol **babel**
(both IPv4 and IPv6).

<br>

##### Topology
---

Original settings:
```
       ┌─────┐
  m1-x-|m3 m5|---m7
  |    | \ / |    |
  |    |  O  |    |
  |    | / \ |    |
  m2---|m4 m6|-x-m8
       └─────┘
```

List of encryption methods with relevant links:
- `algorithm blake2b512`,  m3-m1, with mismatched algs,
- `algorithm hmac sha256`, m1-m2, with matching keys,
- `algorithm hmac sha512`, m2-m4, with matching keys,
- `algorithm blake2s128`,  m5-m7, with matching keys.
- `algorithm hmac sha256`, m7-m8, with matching keys,
- `algorithm hmac sha256`, m8-m6, with mismatched keys,

After reconfiguration:
```
       ┌─────┐
  m1---|m3 m5|-x-m7
  |    | \ / |    |
  x    |  O  |    |
  |    | / \ |    |
  m2---|m4 m6|---m8
       └─────┘
```

List of encryption methods with relevant links:
- `algorithm blake2b256`,  m3-m1, with matching keys,
- `algorithm hmac sha256`, m1-m2, with mismatched keys,
- `algorithm hmac sha512`, m2-m4, with matching keys,
- `algorithm blake2s128`,  m5-m7, with mismatched keys.
- `algorithm hmac sha256`, m7-m8, with matching keys,
- `algorithm hmac sha256`, m8-m6, with matching keys,

<br>

##### Test suite

---

In this test case we are saving:
1. bird tables (`master<4|6>_m<device_numebr>`),
2. krt tables (`krt<4|6>_m<device_number>`).

