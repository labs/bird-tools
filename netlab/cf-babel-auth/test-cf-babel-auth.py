import pytest

import tests.kernel as tk
import tests.config as cf


LIMIT = 150
EXPECTED_DEVICES = ("m1", "m2", "m3", "m4", "m5", "m6", "m7", "m8")
LOG_OPT = [ "<AUTH> ..." ]

LOGS_VE1 = [ "<AUTH> babel1: Authentication failed for .* on ve1 - no matching key" ]
LOGS_VE2 = [ "<AUTH> babel1: Authentication failed for .* on ve2 - no matching key" ]


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait():
    """Wait until the time (limit) runs out"""
    tk.wait(LIMIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_neighbors_babel(exp_devs: str):
    """Get the list of BIRD neighbors and check it"""
    tk.test_babel_neighbors(key="neighbors", dev=exp_devs, opts="babel1")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4", exp_devs, "master4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv6(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6", exp_devs, "master6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv4(exp_devs: str):
    """IPv4: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt4", exp_devs, "inet")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv6(exp_devs: str):
    """IPv6: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt6", exp_devs, "inet6")

@pytest.mark.parametrize(
    "exp_devs, exp_messages",
    [
        pytest.param("m1", LOGS_VE2),
        pytest.param("m5", []),
        pytest.param("m8", LOGS_VE1)
    ],
)
def test_logging_reconfigure(exp_devs, exp_messages):
    """
    Check the log files before the reconfiguration. There should be only DBG,
    INFO and TRACE messages.
    """
    tk.test_logs(exp_devs, LOG_OPT, exp_messages)
    tk.clean_log(exp_devs)


@pytest.mark.parametrize("exp_devs", ["m1", "m5", "m8"])
def test_babel_reconfigure(exp_devs: str):
    """Reconfigure some devices with config file 'bird_v2.conf'"""
    tk.run_configure(exp_devs, "bird_v2.conf")


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait_v2():
    """After reconfiguration wait until the time (limit) runs out."""
    tk.wait(LIMIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_neighbors_babel_v2(exp_devs: str):
    """Get the list of BIRD neighbors and check it"""
    tk.test_babel_neighbors(key="neighbors-v2", dev=exp_devs, opts="babel1")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4_v2(exp_devs: str):
    """
    IPv4: get the content of BIRD tables after reconfiguration and check it.
    """
    tk.test_bird_routes("master4-v2", exp_devs, "master4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv6_v2(exp_devs: str):
    """
    IPv6: get the content of BIRD tables after reconfiguration and check it.
    """
    tk.test_bird_routes("master6-v2", exp_devs, "master6")

@pytest.mark.parametrize(
    "exp_devs, exp_messages",
    [
        pytest.param("m1", LOGS_VE1),
        pytest.param("m5", LOGS_VE1),
        pytest.param("m8", [])
    ],
)
def test_logging_configure_undo(exp_devs, exp_messages):
    """
    Check the log files after reconfiguration. There should be only DBG, INFO
    and TRACE messages.
    """
    tk.test_logs(exp_devs, LOG_OPT, exp_messages)
    tk.clean_log(exp_devs)


@pytest.mark.parametrize("exp_devs", ["m1", "m5", "m8"])
def test_bgp_configure_undo(exp_devs: str):
    """
    Reconfigure some devices with cmd 'configure undo' (back to the original
    file)
    """
    tk.run_configure_undo(exp_devs)


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait_v3():
    """After configure undo, wait until the time (limit) runs out"""
    tk.wait(LIMIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_neighbors_babel_v3(exp_devs: str):
    """Get the list of BIRD neighbors and check it"""
    tk.test_babel_neighbors(key="neighbors-v3", dev=exp_devs, opts="babel1")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4_v3(exp_devs: str):
    """
    IPv4: get the content of BIRD tables after configure undo and check it.
    """
    tk.test_bird_routes("master4-v3", exp_devs, "master4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv6_v3(exp_devs: str):
    """
    IPv6: get the content of BIRD tables after configure undo and check it.
    """
    tk.test_bird_routes("master6-v3", exp_devs, "master6")


@pytest.mark.parametrize(
    "exp_devs, exp_messages",
    [
        pytest.param("m1", LOGS_VE2),
        pytest.param("m2", LOGS_VE2),
        pytest.param("m3", LOGS_VE1),
        pytest.param("m4", []),
        pytest.param("m5", []),
        pytest.param("m6", LOGS_VE2),
        pytest.param("m7", LOGS_VE2),
        pytest.param("m8", LOGS_VE1)
    ],
)
def test_logging(exp_devs, exp_messages):
    """Check the log files. There should only DBG, INFO and TRACE messages"""
    tk.test_logs(exp_devs, LOG_OPT, exp_messages)
