<!--cf-bgp-auth-->
##### Introduction
The purpose of this case is testing the matching/different passwords among
the **ebgp** protocols. Each border protocol (`m11`, `m14`, `m21`, `m24`, `m31`,
`m34`, `m41`, `m44`) has option of the `ebgp` protocol **password**.
As a part of the test case there are two connected devices with matching
passwords and mismatching passwords.

Furthermore, this test case has three successive stages:
1. testing the original settings,
2. reconfiguration with another config files (suffix `_v2`) and testing them,
3. run bird command `configure undo` and testing the original settings.

There are four autonomous systems(futher only *as*) `as1`, `as2`, `as3`
and `as4`. In each of these *as* there are four devices. The designation of each
device looks as follows `m<number_of_as><number_of_device>` (e.g. `m21` is
device number `1` in *as* `2`).

List of autonomous systems with relevant devices:
- `as1` - m11, m12, m13, m14,
- `as2` - m21, m22, m23, m24,
- `as3` - m31, m32, m33, m34,
- `as4` - m41, m42, m43, m44.

There are several protocols inside this topology. Within each of the systems,
the individual devices are interconnected with protocols `ibgp` (IPv4+IPv6) and
protocols `ospf` (IPv4+IPv6). The individual autonomous systems are connected
with protocols `ebgp` (IPv4+IPv6).

List of protocols with relevant links:
- `ebgp<4|6>` - m44-m11, m14-m21, m24-m31, m41-m44,
- `ibgp<4|6>` - m11-m12-m13-m14, m21-m22-m23-m24, m31-m32-m33-m34, m41-m42-m43-m44,
- `ospf<4|6>` - m11-m12-m13-m14, m21-m22-m23-m24, m31-m32-m33-m34, m41-m42-m43-m44.

<br>

##### Topology

---

Original settings IPv4:
```
       ┌──────────────────────────┐
       |      m12 ──── m13        |
       |      /           \       |
       |     /     as1     \      |
       |    /               \     |
       └── m11 ──────────── m14 ──┘
          /                   X
 ┌────── m44 ───┐        ┌──── m21 ──────┐
 |      /       |        |      \        |
 |     /        |        |       \       |
 |    /         |        |        \      |
 |  m43         |        |        m22    |
 |   |          |        |         |     |
 |   | as4      |        |     as2 |     |
 |  m42         |        |        m23    |
 |     \        |        |        /      |
 |      \       |        |       /       |
 |       \      |        |      /        |
 └────── m41 ───┘        └─── m24 ───────┘
           \                  /
      ┌─── m34 ──────────── m31 ───┐
      |      \              /      |
      |       \     as3    /       |
      |        \          /        |
      |        m33 ──── m32        |
      └────────────────────────────┘
```

List of protocols with the related passwords:
- ebgp4(IPv4): m14 -> m21, passwords do not match (`bird14211`, `bird1421`),
- ebgp4(IPv4): m34 -> m41, passwords match (`borg2345`, `borg2345`).

Original settings IPv6:
```
       ┌──────────────────────────┐
       |      m12 ──── m13        |
       |      /           \       |
       |     /     as1     \      |
       |    /               \     |
       └── m11 ──────────── m14 ──┘
          X                   \
 ┌────── m44 ───┐        ┌──── m21 ──────┐
 |      /       |        |      \        |
 |     /        |        |       \       |
 |    /         |        |        \      |
 |  m43         |        |        m22    |
 |   |          |        |         |     |
 |   | as4      |        |     as2 |     |
 |  m42         |        |        m23    |
 |     \        |        |        /      |
 |      \       |        |       /       |
 |       \      |        |      /        |
 └────── m41 ───┘        └─── m24 ───────┘
           \                  /
      ┌─── m34 ──────────── m31 ───┐
      |      \              /      |
      |       \     as3    /       |
      |        \          /        |
      |        m33 ──── m32        |
      └────────────────────────────┘
```
List of protocols with the related passwords:
- ebgp6(IPv6): m24 -> m31, passwords match (`xxxx1234`, `xxxx1234`),
- ebgp6(IPv6): m44 -> m11, passwords do not match (`abcd1234`, `abcd12345`).

After reconfiguration IPv4:
```
       ┌──────────────────────────┐
       |      m12 ──── m13        |
       |      /           \       |
       |     /     as1     \      |
       |    /               \     |
       └── m11 ──────────── m14 ──┘
          /                   \
 ┌────── m44 ───┐        ┌──── m21 ──────┐
 |      /       |        |      \        |
 |     /        |        |       \       |
 |    /         |        |        \      |
 |  m43         |        |        m22    |
 |   |          |        |         |     |
 |   | as4      |        |     as2 |     |
 |  m42         |        |        m23    |
 |     \        |        |        /      |
 |      \       |        |       /       |
 |       \      |        |      /        |
 └────── m41 ───┘        └─── m24 ───────┘
           X                  /
      ┌─── m34 ──────────── m31 ───┐
      |      \              /      |
      |       \     as3    /       |
      |        \          /        |
      |        m33 ──── m32        |
      └────────────────────────────┘
```
List of protocols with the related passwords:
- ebgp4(IPv4): m14 -> m21, passwords match (`bird14211`, `bird14211`),
- ebgp4(IPv4): m34 -> m41, passwords do not match (`borg2345`, `borg234`).

After reconfiguration IPv6:
```
       ┌──────────────────────────┐
       |      m12 ──── m13        |
       |      /           \       |
       |     /     as1     \      |
       |    /               \     |
       └── m11 ──────────── m14 ──┘
          /                   \
 ┌────── m44 ───┐        ┌──── m21 ──────┐
 |      /       |        |      \        |
 |     /        |        |       \       |
 |    /         |        |        \      |
 |  m43         |        |        m22    |
 |   |          |        |         |     |
 |   | as4      |        |     as2 |     |
 |  m42         |        |        m23    |
 |     \        |        |        /      |
 |      \       |        |       /       |
 |       \      |        |      /        |
 └────── m41 ───┘        └─── m24 ───────┘
           \                  X
      ┌─── m34 ──────────── m31 ───┐
      |      \              /      |
      |       \     as3    /       |
      |        \          /        |
      |        m33 ──── m32        |
      └────────────────────────────┘
```
List of protocols with the related passwords:
- ebgp6(IPv6): m24 -> m31, passwords do not match (`xyz1234`, `xxxx1234`),
- ebgp6(IPv6): m44 -> m11, passwords match (`abcd12345`, `abcd12345`).


<br>

##### Test suite

---

In this test case we are saving:
1. krt tables (`krt<4|6>_m<as_number><device_number>`),
2. bird tables (`master<4|6>_m<as_number><device_number>`).
