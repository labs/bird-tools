import pytest

import tests.kernel as tk
import tests.config as cf


LIMIT = 60
EXPECTED_DEVICES = (
    "m11", "m12", "m13", "m14",
    "m21", "m22", "m23", "m24",
    "m31", "m32", "m33", "m34",
    "m41", "m42", "m43", "m44"
)

LOG_OPT = ["<WARN> Next hop address .* resolvable through recursive route"]


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait():
    """Wait until the time (limit) runs out"""
    tk.wait(LIMIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4", exp_devs, "master4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv6(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6", exp_devs, "master6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv4(exp_devs: str):
    """IPv4: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt4", exp_devs, "inet")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv6(exp_devs: str):
    """IPv6: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt6", exp_devs, "inet6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_logging_reconfigure(exp_devs: str):
    """
    Check the log files before the reconfiguration. There should be only DBG,
    INFO and TRACE messages.
    """
    tk.test_logs(exp_devs, LOG_OPT, [])
    tk.clean_log(exp_devs)


@pytest.mark.parametrize("exp_devs", ["m11", "m21", "m24", "m41"])
def test_bgp_reconfigure(exp_devs: str):
    """Reconfigure some devices with config file 'bird_v2.conf'"""
    tk.run_configure(exp_devs, "bird_v2.conf")


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait_v2():
    """After reconfiguration wait until the time (limit) runs out."""
    tk.wait(LIMIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4_v2(exp_devs: str):
    """
    IPv4: get the content of BIRD tables after reconfiguration and check it.
    """
    tk.test_bird_routes("master4-v2", exp_devs, "master4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv6_v2(exp_devs: str):
    """
    IPv6: get the content of BIRD tables after reconfiguration and check it.
    """
    tk.test_bird_routes("master6-v2", exp_devs, "master6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_logging_configure_undo(exp_devs: str):
    """
    Check the log files after reconfiguration. There should be only DBG, INFO
    and TRACE messages.
    """
    tk.test_logs(exp_devs, LOG_OPT, [])
    tk.clean_log(exp_devs)


@pytest.mark.parametrize("exp_devs", ["m11", "m21", "m24", "m41"])
def test_bgp_configure_undo(exp_devs: str):
    """
    Reconfigure some devices with cmd 'configure undo' (back to the original
    file)
    """
    tk.run_configure_undo(exp_devs)


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait_v3():
    """After configure undo, wait until the time (limit) runs out"""
    tk.wait(LIMIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4_v3(exp_devs: str):
    """
    IPv4: get the content of BIRD tables after configure undo and check it.
    """
    tk.test_bird_routes("master4-v3", exp_devs, "master4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv6_v3(exp_devs: str):
    """
    IPv6: get the content of BIRD tables after configure undo and check it.
    """
    tk.test_bird_routes("master6-v3", exp_devs, "master6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_logging(exp_devs: str):
    """Check the log files. There should only DBG, INFO and TRACE messages"""
    tk.test_logs(exp_devs, LOG_OPT, [])

