#!/usr/bin/python3

import sys

asn_cnt, net_cnt, link_cnt = [ int(_) for _ in sys.argv[1:] ]

print('log "bird.log" all;')
print('router id 2;')
print('protocol device {}')

print('protocol static {')
print('  ipv6;')
for net in range(1, net_cnt+1):
    print(f'  route 2001:db8:f{net:03x}::/48 unreachable;')
print('}')
print()

for asn in range(1, asn_cnt+1):
    for link in range(1, link_cnt+1):
        print(f'protocol bgp b_{asn:03x}_{link:03x}', '{')
        print(f'  local 2001:db8:1:1:dd2::{asn:03x}:{link:03x} as {asn+4210000000};')
        print(f'  neighbor 2001:db8:1:1:dd1::{asn:03x}:{link:03x} as 4210000000;')
        print( '  free bind;')
        print( '  strict bind;')
        print( '  multihop;')
        print( '  ipv6 {')
        print( '    import none;')
        print( '    export filter {')
        print(f'      bgp_med = {link:#03x};')
        print( '      accept;')
        print( '    };')
        print( '  };')
        print( '}')
        print()
