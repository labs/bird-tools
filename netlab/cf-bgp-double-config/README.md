<!--cf-bgp-base-->
##### Introduction

A basic test of two nodes, both requesting a connection to the other, yet the
config is there twice and the first one is disabled. This replicates a bug in
2.0.12 where protocols defined this way don't connect at all.

<br>

##### Test suite

---

In this test case we are saving:
1. bird tables (`master6_m[12]`),
