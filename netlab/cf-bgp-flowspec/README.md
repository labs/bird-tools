<!--cf-bgp-base-->
##### Introduction
Testing basic flowspec validation procedures. Machines `m2` and `m3` export two
different flowspecs for the same prefix. Route via `m3` is preferred and
machine `m3` is cycled to check revalidation.

<br>

##### Topology

---

```
       ┌───────────────────────────┐
       |      m1 ─────── m3        |
       |      |          |         |
       |      |    as1   |         |
       |      |          |         |
       |      └─── m2 ───┘         |
       |                           |
       └───────────────────────────┘
```

<br>

##### Test suite

---

In this test case we are saving:
1. krt tables (`krt<4|6>_m<device_number>`).
2. bird tables (`master<4|6|flow4|flow6>_m<device_number>`),
