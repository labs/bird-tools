log "bird.log" all;

router id 10.1.1.1;

ipv4 table master4 { trie; };
ipv6 table master6 { trie; };

flow4 table f4;
flow6 table f6;

protocol device {
	scan time 10;
}

protocol kernel kernel4 {
	scan time 10;
	ipv4 {
		export all;
	};
}

protocol kernel kernel6 {
	scan time 10;
	ipv6 {
		export all;
	};
}

protocol static static4 {
	ipv4;
	route 10.6.6.0/24 via 10.1.1.6 { bgp_path.prepend(42); bgp_path.prepend(43); };
	route 10.7.7.0/24 via 10.1.1.7 { bgp_path.prepend(42); bgp_path.prepend(43); };
}

protocol static static6 {
	ipv6;
	route 2001:db8:66::/48 via 2001:db8:1:1::66 { bgp_path.prepend(42); bgp_path.prepend(43); };
	route 2001:db8:77::/48 via 2001:db8:1:1::77 { bgp_path.prepend(42); bgp_path.prepend(43); };
}

protocol ospf v2 ospf4 {
	ipv4;
	area 0 {
		interface "ve0" { stub; };
		interface "ve2" { hello 5; type ptp; };
		interface "ve3" { hello 5; type ptp; };
	};
}

protocol ospf v3 ospf6 {
	ipv6;
	area 0 {
		interface "ve0" { stub; };
		interface "ve2" { hello 5; type ptp; };
		interface "ve3" { hello 5; type ptp; };
	};
}

protocol bfd {}

#~~~~~~~~~~~~~~~~~ IPv4 IBGP ~~~~~~~~~~~~~~~~~
template bgp ibgp4 {
	bfd graceful;
	ipv4 {
		import all;
		export where source ~ [ RTS_STATIC, RTS_BGP ];
	};
	ipv6 { 
		import all;
		export where source ~ [ RTS_STATIC, RTS_BGP ];
	};
	flow4 {
		import all;
		export none;
		validate on;
	};
	flow6 {
		import all;
		export none;
		validate on;
	};
}

protocol bgp ibgp4_2 from ibgp4 {
	local 10.1.1.1 as 1;
	neighbor 10.1.2.1 as 1;
}

protocol bgp ibgp4_3 from ibgp4 {
	local 10.1.1.1 as 1;
	neighbor 10.1.3.1 as 1;
}
