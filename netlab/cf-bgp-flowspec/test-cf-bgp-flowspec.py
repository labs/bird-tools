import pytest

import tests.kernel as tk
import tests.config as cf


LIMIT = 20
EXPECTED_DEVICES = (
    "m1",
)

LOG_OPT = [
        "<WARN> Next hop address .* resolvable through recursive route",
        "<ERR> bfd1: Socket error: Network is unreachable",
        "<RMT> bfd.*: Bad packet from .* - (mismatched remote|unknown session) id [(][0-9]+[)]",
        ]


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait():
    """Wait until the time (limit) runs out"""
    tk.wait(LIMIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv4(exp_devs: str):
    """IPv4: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt4", exp_devs, "inet")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv6(exp_devs: str):
    """IPv6: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt6", exp_devs, "inet6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4", exp_devs, "master4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv6(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6", exp_devs, "master6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_flow4(exp_devs: str):
    """FlowSpecv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("f4", exp_devs, "f4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_flow6(exp_devs: str):
    """FlowSpecv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("f6", exp_devs, "f6")


@pytest.mark.skipif(False, reason="")
def test_stop_two():
    """Stop node m2"""
    tk.kill_node("m2", "STOP")


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait_two_down():
    """Wait until the time (limit) runs out"""
    tk.wait(10)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4_d2(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-d2", exp_devs, "master4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv6_d2(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6-d2", exp_devs, "master6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_flow4_d2(exp_devs: str):
    """flow4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("f4-d2", exp_devs, "f4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_flow6_d2(exp_devs: str):
    """flow6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("f6-d2", exp_devs, "f6")


@pytest.mark.skipif(False, reason="")
def test_cont_two():
    tk.kill_node("m2", "CONT")


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait_two_up():
    """Wait until the time (limit) runs out"""
    tk.wait(10)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4_u2(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-u2", exp_devs, "master4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv6_u2(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6-u2", exp_devs, "master6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_flow4_u2(exp_devs: str):
    """flow4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("f4-u2", exp_devs, "f4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_flow6_u2(exp_devs: str):
    """flow6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("f6-u2", exp_devs, "f6")


@pytest.mark.skipif(False, reason="")
def test_stop_three():
    """Stop node m3"""
    tk.kill_node("m3", "STOP")


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait_three_down():
    """Wait until the time (limit) runs out"""
    tk.wait(10)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4_d3(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-d3", exp_devs, "master4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv6_d3(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6-d3", exp_devs, "master6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_flow4_d3(exp_devs: str):
    """flow4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("f4-d3", exp_devs, "f4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_flow6_d3(exp_devs: str):
    """flow6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("f6-d3", exp_devs, "f6")


@pytest.mark.skipif(False, reason="")
def test_cont_three():
    tk.kill_node("m3", "CONT")


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait_three_up():
    """Wait until the time (limit) runs out"""
    tk.wait(10)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4_u3(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-u3", exp_devs, "master4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv6_u3(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6-u3", exp_devs, "master6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_flow4_u3(exp_devs: str):
    """flow4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("f4-u3", exp_devs, "f4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_flow6_u3(exp_devs: str):
    """flow6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("f6-u3", exp_devs, "f6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_logging(exp_devs: str):
    """Check the log files. There should only DBG, INFO and TRACE messages"""
    tk.test_logs(exp_devs, LOG_OPT, [])
