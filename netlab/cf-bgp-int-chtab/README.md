<!--cf-bgp-int-chtab-->
##### Introduction
This case tests is similar to the basic topology with **ibgp** and **ebgp**
protocols. Unlike the basic topology are the **ibgp** protocols structured into
templates.
Within each of the systems, the individual devices are interconnected
with protocols `ibgp` (IPv4+IPv6) and protocols `ospf` (IPv4+IPv6).
The individual autonomous systems are connected with protocols `ebgp`
(IPv4+IPv6).

All the BGP protocols have import and export tables enabled and this test
checks their display, as well as "show route in" and "show route for" for all
the regular and channel tables.

As the channel tables have unstable output in v2, the test data is available only for v3.


