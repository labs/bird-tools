import pytest

import tests.kernel as tk
import tests.config as cf

from itertools import product

INTERNAL_DEVICES = (
    "m12", "m13",
    "m22", "m23",
    "m32", "m33",
    "m42", "m43"
    )

EXTERNAL_DEVICES = (
    "m11", "m14",
    "m21", "m24",
    "m31", "m34",
    "m41", "m44"
)

LIMIT = 60
EXPECTED_DEVICES = INTERNAL_DEVICES + EXTERNAL_DEVICES
LOG_OPT = ["<WARN> Next hop address .* resolvable through recursive route"]


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait():
    """Wait until the time (limit) runs out"""
    tk.wait(LIMIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv4(exp_devs: str):
    """IPv4: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt4", exp_devs, "inet")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv6(exp_devs: str):
    """IPv6: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt6", exp_devs, "inet6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4", exp_devs, "master4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv6(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6", exp_devs, "master6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_for_ipv4(exp_devs: str):
    """Get a route by using SHOW ROUTE FOR"""
    tk.test_bird_routes(f"master4-for", exp_devs, f"master4", opts="for 10.102.42.42")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_for_ipv6(exp_devs: str):
    """Get a route by using SHOW ROUTE FOR"""
    tk.test_bird_routes(f"master6-for", exp_devs, f"master6", opts="for 2001:db8:102::dead:beef")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_in_ipv4(exp_devs: str):
    """Get a route by using SHOW ROUTE IN"""
    tk.test_bird_routes(f"master4-in", exp_devs, f"master4", opts="in 10.100.0.0/15")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_in_ipv6(exp_devs: str):
    """Get a route by using SHOW ROUTE IN"""
    tk.test_bird_routes(f"master6-in", exp_devs, f"master6", opts="in 2001:db8:100::/47")


PROTO_COMBINATIONS = [ i for i in product(EXPECTED_DEVICES, ("ibgp1", "ibgp2"))] + [ i for i in product(EXTERNAL_DEVICES, ("ibgp3", "ebgp1"))]

@pytest.mark.parametrize("exp_devs,proto", PROTO_COMBINATIONS)
@pytest.mark.parametrize("channel", ("ipv4", "ipv6"))
@pytest.mark.parametrize("prefix", ("import", "export"))
def test_bird_routes_chtab_internal(exp_devs: str, proto: str, channel: str, prefix: str):
    """Get internal node channel tables"""
    tk.test_bird_routes(f"{proto}-{channel}-{prefix}", exp_devs, f"{proto}.{channel}", prefix=prefix)


@pytest.mark.parametrize("exp_devs,proto", PROTO_COMBINATIONS)
@pytest.mark.parametrize("prefix", ("import", "export"))
def test_bird_routes_chtab_for_ipv4(exp_devs: str, proto: str, prefix: str):
    """Get a route from node channel tables by using SHOW ROUTE FOR"""
    tk.test_bird_routes(f"{proto}-ipv4-{prefix}-for", exp_devs, f"{proto}.ipv4", prefix=prefix, opts="for 10.102.42.42")


@pytest.mark.parametrize("exp_devs,proto", PROTO_COMBINATIONS)
@pytest.mark.parametrize("prefix", ("import", "export"))
def test_bird_routes_chtab_for_ipv6(exp_devs: str, proto: str, prefix: str):
    """Get a route from node channel tables by using SHOW ROUTE FOR"""
    tk.test_bird_routes(f"{proto}-ipv6-{prefix}-for", exp_devs, f"{proto}.ipv6", prefix=prefix, opts="for 2001:db8:102::dead:beef")


@pytest.mark.parametrize("exp_devs,proto", PROTO_COMBINATIONS)
@pytest.mark.parametrize("prefix", ("import", "export"))
def test_bird_routes_chtab_in_ipv4(exp_devs: str, proto: str, prefix: str):
    """Get a route from node channel tables by using SHOW ROUTE IN"""
    tk.test_bird_routes(f"{proto}-ipv4-{prefix}-in", exp_devs, f"{proto}.ipv4", prefix=prefix, opts="in 10.100.0.0/15")


@pytest.mark.parametrize("exp_devs,proto", PROTO_COMBINATIONS)
@pytest.mark.parametrize("prefix", ("import", "export"))
def test_bird_routes_chtab_in_ipv6(exp_devs: str, proto: str, prefix: str):
    """Get a route from node channel tables by using SHOW ROUTE IN"""
    tk.test_bird_routes(f"{proto}-ipv6-{prefix}-in", exp_devs, f"{proto}.ipv6", prefix=prefix, opts="in 2001:db8:100::/47")


@pytest.mark.skipif(cf.save == True, reason="mode: save")
@pytest.mark.parametrize("exp_devs,proto", PROTO_COMBINATIONS)
def test_reload(exp_devs: str, proto: str):
    tk.reload_bird_protocol(exp_devs, proto)


def test_wait_reload():
    tk.wait(1)


@pytest.mark.skipif(cf.save == True, reason="mode: save")
@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv4_r(exp_devs: str):
    """IPv4: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt4", exp_devs, "inet")


@pytest.mark.skipif(cf.save == True, reason="mode: save")
@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv6_r(exp_devs: str):
    """IPv6: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt6", exp_devs, "inet6")


@pytest.mark.skipif(cf.save == True, reason="mode: save")
@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4_r(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4", exp_devs, "master4")


@pytest.mark.skipif(cf.save == True, reason="mode: save")
@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv6_r(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6", exp_devs, "master6")


@pytest.mark.skipif(cf.save == True, reason="mode: save")
@pytest.mark.parametrize("exp_devs,proto", PROTO_COMBINATIONS)
@pytest.mark.parametrize("channel", ("ipv4", "ipv6"))
@pytest.mark.parametrize("prefix", ("import", "export"))
def test_bird_routes_chtab_internal_r(exp_devs: str, proto: str, channel: str, prefix: str):
    """Get internal node channel tables"""
    tk.test_bird_routes(f"{proto}-{channel}-{prefix}", exp_devs, f"{proto}.{channel}", prefix=prefix)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_logging(exp_devs: str):
    """Check the log files. There should only DBG, INFO and TRACE messages"""
    tk.test_logs(exp_devs, LOG_OPT, [])
