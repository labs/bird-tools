<!--cf-bgp-int-->
##### Introduction
This case tests the protocols inside the templates, part of systems uses
IPv4, part uses IPv6 (single template `ibgpt`). Protocols ospf4 and ospf6
remain separate (OSPFv2 and OSPFv3-IPv6).

List of devices and their protocols:
- `m11-m24`, ibgp(IPv4),
- `m31-m44`, ibgp(IPv6),
- `m44-m11`, ebgp(IPv4),
- `m14-m21`, ebgp(IPv4),
- `m24-m31`, ebgp(IPv6),
- `m34-m41`, ebgp(IPv6).

There are four autonomous systems(futher only *as*) `as1`, `as2`, `as3`
and `as4`. In each of these *as* there are four devices. The designation of each
device looks as follows `m<number_of_as><number_of_device>` (e.g. `m21` is
device number `1` in *as* `2`).

List of autonomous systems with relevant devices:
- `as1` - m11, m12, m13, m14,
- `as2` - m21, m22, m23, m24,
- `as3` - m31, m32, m33, m34,
- `as4` - m41, m42, m43, m44.

<br>

##### Topology

---

```
       ┌──────────────────────────┐
       |      m12 ──── m13        |
       |      /           \       |
       |     /     as1     \      |
       |    /               \     |
       └── m11 ──────────── m14 ──┘
          /                   \
 ┌────── m44 ───┐        ┌──── m21 ──────┐
 |      /       |        |      \        |
 |     /        |        |       \       |
 |    /         |        |        \      |
 |  m43         |        |        m22    |
 |   |          |        |         |     |
 |   | as4      |        |     as2 |     |
 |  m42         |        |        m23    |
 |     \        |        |        /      |
 |      \       |        |       /       |
 |       \      |        |      /        |
 └────── m41 ───┘        └─── m24 ───────┘
           \                  /
      ┌─── m34 ──────────── m31 ───┐
      |      \              /      |
      |       \     as3    /       |
      |        \          /        |
      |        m33 ──── m32        |
      └────────────────────────────┘
```

<br>

##### Test suite

---

In this test case we are saving:
1. krt tables (`krt<4|6>_m<as_number><device_number>`).
2. bird tables (`master<4|6>_m<as_number><device_number>`),
