<!--cf-bgp-merged-->
##### Introduction
This case is testing the **merge paths** option within the **ebgp**/**ibgp**
topology. The option `merge paths` is part of the kernel protocols.

There are several protocols inside this topology. Within each of the systems,
the individual devices are interconnected with protocols `ibgp` (IPv4+IPv6) and
protocols `ospf` (IPv4+IPv6). The individual autonomous systems are connected
with protocols `ebgp` (IPv4+IPv6).

List of protocols:
- `ebgp<4|6>` - m44-m11, m14-m21, m24-m31, m41-m44,
- `ibgp<4|6>` - m11-m12-m13-m14, m21-m22-m23-m24, m31-m32-m33-m34, m41-m42-m43-m44,
- `ospf<4|6>` - m11-m12-m13-m14, m21-m22-m23-m24, m31-m32-m33-m34, m41-m42-m43-m44.

There are four autonomous systems(futher only *as*) `as1`, `as2`, `as3`
and `as4`. In each of these as there are four devices. The designation of each
device looks as follows `m<number_of_as><number_of_device>` (e.g. `m21` is
device number `1` in system `1`)

List of autonomous systems with relevant devices:
- `as1` - m11, m12, m13, m14,
- `as2` - m21, m22, m23, m24,
- `as3` - m31, m32, m33, m34,
- `as4` - m41, m42, m43, m44.

<br>

##### Topology

---

```
       ┌──────────────────────────┐
       |      m12 ──── m13        |
       |      /           \       |
       |     /     as1     \      |
       |    /               \     |
       └── m11 ──────────── m14 ──┘
          /                   \
 ┌────── m44 ───┐        ┌──── m21 ──────┐
 |      /       |        |      \        |
 |     /        |        |       \       |
 |    /         |        |        \      |
 |  m43         |        |        m22    |
 |   |          |        |         |     |
 |   | as4      |        |     as2 |     |
 |  m42         |        |        m23    |
 |     \        |        |        /      |
 |      \       |        |       /       |
 |       \      |        |      /        |
 └────── m41 ───┘        └─── m24 ───────┘
           \                  /
      ┌─── m34 ──────────── m31 ───┐
      |      \              /      |
      |       \     as3    /       |
      |        \          /        |
      |        m33 ──── m32        |
      └────────────────────────────┘
```

<br>

##### Test suite

---

In this test case we are saving:
1. krt tables (`krt<4|6>_m<as_number><device_number>`).
2. bird tables (`master<4|6>_m<as_number><device_number>`),
