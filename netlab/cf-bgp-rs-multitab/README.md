<!--cf-bgp-rs-multitab-->
##### Introduction

This case tests BGP route server in multi table configuration. The routes are
filtered according to ROAs fed by StayRTR. RPKI reloads are tested as well.
