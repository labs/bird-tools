import pytest

import tests.kernel as tk
import tests.config as cf


LIMIT = 60
EXPECTED_DEVICES = ("m250", "m1", "m2", "m3", "m4", "m5", "m6", "m7", "m8", "m9", "m10")
RTR_SOURCES = ("m66", "m67", "m68")

class Updater:
    def __init__(self, where, num):
        self.where = where
        self.num = num

    def _announce(self):
        tk.kill_process(self.where, "stayrtr.pid", "HUP")

    def update(self):
        tk.process_command(f"cp {self.where}/rpki.json {self.where}/rpki.json.orig")
        tk.process_command(f"sed -i '/[^0-9]{self.num}[.]0[.]0[.]0/s#6499.#777#' {self.where}/rpki.json")
        self._announce()

    def revert(self):
        tk.process_command(f"cp {self.where}/rpki.json.orig {self.where}/rpki.json")
        self._announce()


up2 = Updater("m68", 102)
up5 = Updater("m67", 105)
up7 = Updater("m66", 107)


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait():
    """Wait until the time (limit) runs out"""
    tk.wait(LIMIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv4(exp_devs: str):
    """IPv4: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt4", exp_devs, "inet")


#@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
#def test_krt_routes_ipv6(exp_devs: str):
#    """IPv6: get the content of KERNEL tables and check it"""
#    tk.test_krt_routes("krt6", exp_devs, "inet6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4", exp_devs, "master4", opts="")


#@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
#def test_bird_routes_ipv6(exp_devs: str):
#    """IPv6: get the content of BIRD tables and check it"""
#    tk.test_bird_routes("master6", exp_devs, "master6", opts="")


def test_rtr_update_2():
    up2.update()


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait_rtr_update_2():
    """Wait until the time (limit) runs out"""
    tk.wait(LIMIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4_rtr2(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-rtr2", exp_devs, "master4", opts="")


#@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
#def test_bird_routes_ipv6_rtr2(exp_devs: str):
#    """IPv6: get the content of BIRD tables and check it"""
#    tk.test_bird_routes("master6-rtr2", exp_devs, "master6", opts="")


def test_rtr_update_5():
    up5.update()


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait_rtr_update_5():
    """Wait until the time (limit) runs out"""
    tk.wait(LIMIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4_rtr5(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-rtr5", exp_devs, "master4", opts="")


#@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
#def test_bird_routes_ipv6_rtr5(exp_devs: str):
#    """IPv6: get the content of BIRD tables and check it"""
#    tk.test_bird_routes("master6-rtr5", exp_devs, "master6", opts="")


def test_rtr_update_7():
    up7.update()


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait_rtr_update_7():
    """Wait until the time (limit) runs out"""
    tk.wait(LIMIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4_rtr7(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-rtr7", exp_devs, "master4", opts="")


#@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
#def test_bird_routes_ipv6_rtr7(exp_devs: str):
#    """IPv6: get the content of BIRD tables and check it"""
#    tk.test_bird_routes("master6-rtr7", exp_devs, "master6", opts="")


def test_rtr_revert_2():
    rtr = "m68"
    up2.revert()


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait_rtr_revert_2():
    """Wait until the time (limit) runs out"""
    tk.wait(LIMIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4_rrtr2(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-rrtr2", exp_devs, "master4", opts="")


#@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
#def test_bird_routes_ipv6_rrtr2(exp_devs: str):
#    """IPv6: get the content of BIRD tables and check it"""
#    tk.test_bird_routes("master6-rrtr2", exp_devs, "master6", opts="")


def test_rtr_revert_5():
    up5.revert()


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait_rtr_revert_5():
    """Wait until the time (limit) runs out"""
    tk.wait(LIMIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4_rrtr5(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-rrtr5", exp_devs, "master4", opts="")


#@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
#def test_bird_routes_ipv6_rrtr5(exp_devs: str):
#    """IPv6: get the content of BIRD tables and check it"""
#    tk.test_bird_routes("master6-rrtr5", exp_devs, "master6", opts="")


def test_rtr_revert_7():
    up7.revert()


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait_rtr_revert_7():
    """Wait until the time (limit) runs out"""
    tk.wait(LIMIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4_rrtr7(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-rrtr7", exp_devs, "master4", opts="")


#@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
#def test_bird_routes_ipv6_rrtr7(exp_devs: str):
#    """IPv6: get the content of BIRD tables and check it"""
#    tk.test_bird_routes("master6-rrtr7", exp_devs, "master6", opts="")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_logging(exp_devs: str):
   """Check the log files. There should only DBG, INFO and TRACE messages"""
   tk.test_logs(exp_devs, [], [])

