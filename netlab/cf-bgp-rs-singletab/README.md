<!--cf-bgp-rs-singletab-->
##### Introduction

This case tests BGP route server in single table configuration. The routes are
filtered according to ROAs fed by StayRTR. RPKI reloads are tested as well.
