<!--cf-ebgp-graceful-->
##### Description
This is a thorough test of BGP router going up and down. The m0 node goes
repeatedly down an back up again, checking first that graceful restart is
triggered, then the long lived graceful restart feature in the next cycle
and finally cleanup of all routes after all the timeouts fail.

Router failure is induced by SIGSTOP (and SIGCONT restores the run) and
detected by BFD.

At the end of all this, the test also sends SIGSTOP and SIGCONT repeatedly to
the m0 node (a pingpong test) which should at least sometimes hit some
transient states in all running BIRDs, possibly triggering hidden bugs.
