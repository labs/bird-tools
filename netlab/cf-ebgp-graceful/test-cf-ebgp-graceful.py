import pytest

import tests.kernel as tk
import tests.config as cf


LIMIT = 60
LOG_OPT = [
    "<RMT> ebgp4_0: Received: Connection collision resolution",
    "<RMT> ebgp6_0: Received: Connection collision resolution",
    "<RMT> ebgp4_0: Received: Cease",
    "<RMT> ebgp6_0: Received: Cease",
    "<RMT> bfd1: Bad packet from .* - unknown session id",
    "<RMT> bfd.*: Bad packet from .* - mismatched remote id [(][0-9]+[)]",
]

EXPECTED_DEVICES_DOWN = ( "m2", "m1", "m3" )
EXPECTED_DEVICES_UP = ("m0", "m2", "m1", "m3")


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait():
    """Wait until the time (limit) runs out"""
    tk.wait(LIMIT)

@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_UP)
def test_krt_routes_ipv4(exp_devs: str):
    """IPv4: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt4", exp_devs, "inet")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_UP)
def test_krt_routes_ipv6(exp_devs: str):
    """IPv6: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt6", exp_devs, "inet6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_UP)
def test_bird_routes_ipv4(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4", exp_devs, "master4", opts="")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_UP)
def test_bird_routes_ipv6(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6", exp_devs, "master6", opts="")


@pytest.mark.skipif(False, reason="")
def test_stop_zero():
    """Stop the Zero node"""
    tk.kill_node("m0", "STOP")


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait_gr():
    """Wait until the time (limit) runs out"""
    tk.wait(5)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_DOWN)
def test_krt_routes_ipv4_gr(exp_devs: str):
    """IPv4: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt4-gr", exp_devs, "inet")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_DOWN)
def test_krt_routes_ipv6_gr(exp_devs: str):
    """IPv6: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt6-gr", exp_devs, "inet6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_DOWN)
def test_bird_routes_ipv4_gr(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-gr", exp_devs, "master4", opts="")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_DOWN)
def test_bird_routes_ipv6_gr(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6-gr", exp_devs, "master6", opts="")


@pytest.mark.skipif(False, reason="")
def test_cont_zero_gr():
    tk.kill_node("m0", "CONT")


@pytest.mark.skipif(cf.save == True, reason="mode: save")
def test_bird_reconfigure_gr():
    """Reconfigure some devices with config file 'bird.conf'"""
    tk.run_configure("m0", "bird.conf")


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait_gr_rec():
    """Wait until the time (limit) runs out"""
    tk.wait(LIMIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_UP)
def test_krt_routes_ipv4_gr_rec(exp_devs: str):
    """IPv4: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt4-gr-rec", exp_devs, "inet")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_UP)
def test_krt_routes_ipv6_gr_rec(exp_devs: str):
    """IPv6: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt6-gr-rec", exp_devs, "inet6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_UP)
def test_bird_routes_ipv4_gr_rec(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-gr-rec", exp_devs, "master4", opts="")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_UP)
def test_bird_routes_ipv6_gr_rec(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6-gr-rec", exp_devs, "master6", opts="")


def test_wait_gr_rec2():
    """Wait until the time (limit) runs out"""
    tk.wait(3)


@pytest.mark.skipif(cf.save == True, reason="mode: save")
@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_UP)
def test_krt_routes_ipv4_gr_rec2(exp_devs: str):
    """IPv4: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt4-gr-rec", exp_devs, "inet")


@pytest.mark.skipif(cf.save == True, reason="mode: save")
@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_UP)
def test_krt_routes_ipv6_gr_rec2(exp_devs: str):
    """IPv6: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt6-gr-rec", exp_devs, "inet6")


@pytest.mark.skipif(cf.save == True, reason="mode: save")
@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_UP)
def test_bird_routes_ipv4_gr_rec2(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-gr-rec", exp_devs, "master4", opts="")


@pytest.mark.skipif(cf.save == True, reason="mode: save")
@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_UP)
def test_bird_routes_ipv6_gr_rec2(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6-gr-rec", exp_devs, "master6", opts="")


@pytest.mark.skipif(False, reason="")
def test_stop_zero_llgr():
    """Stop the Zero node"""
    tk.kill_node("m0", "STOP")


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait_llgr():
    """Wait until the time (limit) runs out"""
    tk.wait(30)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_DOWN)
def test_krt_routes_ipv4_llgr(exp_devs: str):
    """IPv4: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt4-llgr", exp_devs, "inet")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_DOWN)
def test_krt_routes_ipv6_llgr(exp_devs: str):
    """IPv6: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt6-llgr", exp_devs, "inet6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_DOWN)
def test_bird_routes_ipv4_llgr(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-llgr", exp_devs, "master4", opts="")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_DOWN)
def test_bird_routes_ipv6_llgr(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6-llgr", exp_devs, "master6", opts="")


@pytest.mark.skipif(False, reason="")
def test_cont_zero_llgr_rec():
    tk.kill_node("m0", "CONT")


@pytest.mark.skipif(cf.save == True, reason="mode: save")
def test_bird_reconfigure_llgr():
    """Reconfigure some devices with config file 'bird.conf'"""
    tk.run_configure("m0", "bird.conf")


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait_llgr_rec():
    """Wait until the time (limit) runs out"""
    tk.wait(LIMIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_UP)
def test_krt_routes_ipv4_llgr_rec(exp_devs: str):
    """IPv4: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt4-llgr-rec", exp_devs, "inet")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_UP)
def test_krt_routes_ipv6_llgr_rec(exp_devs: str):
    """IPv6: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt6-llgr-rec", exp_devs, "inet6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_UP)
def test_bird_routes_ipv4_llgr_rec(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-llgr-rec", exp_devs, "master4", opts="")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_UP)
def test_bird_routes_ipv6_llgr_rec(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6-llgr-rec", exp_devs, "master6", opts="")


def test_wait_llgr_rec2():
    """Wait until the time (limit) runs out"""
    tk.wait(3)


@pytest.mark.skipif(cf.save == True, reason="mode: save")
@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_UP)
def test_krt_routes_ipv4_llgr_rec2(exp_devs: str):
    """IPv4: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt4-llgr-rec", exp_devs, "inet")


@pytest.mark.skipif(cf.save == True, reason="mode: save")
@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_UP)
def test_krt_routes_ipv6_llgr_rec2(exp_devs: str):
    """IPv6: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt6-llgr-rec", exp_devs, "inet6")


@pytest.mark.skipif(cf.save == True, reason="mode: save")
@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_UP)
def test_bird_routes_ipv4_llgr_rec2(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-llgr-rec", exp_devs, "master4", opts="")


@pytest.mark.skipif(cf.save == True, reason="mode: save")
@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_UP)
def test_bird_routes_ipv6_llgr_rec2(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6-llgr-rec", exp_devs, "master6", opts="")


@pytest.mark.skipif(False, reason="")
def test_stop_zero_down():
    """Stop the Zero node"""
    tk.kill_node("m0", "STOP")


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait_down():
    """Wait until the time (limit) runs out"""
    tk.wait(70)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_DOWN)
def test_krt_routes_ipv4_down(exp_devs: str):
    """IPv4: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt4-down", exp_devs, "inet")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_DOWN)
def test_krt_routes_ipv6_down(exp_devs: str):
    """IPv6: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt6-down", exp_devs, "inet6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_DOWN)
def test_bird_routes_ipv4_down(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-down", exp_devs, "master4", opts="")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_DOWN)
def test_bird_routes_ipv6_down(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6-down", exp_devs, "master6", opts="")


@pytest.mark.skipif(False, reason="")
def test_cont_zero_down_rec():
    tk.kill_node("m0", "CONT")


@pytest.mark.skipif(cf.save == True, reason="mode: save")
def test_bird_reconfigure_down():
    """Reconfigure some devices with config file 'bird.conf'"""
    tk.run_configure("m0", "bird.conf")


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait_down_rec():
    """Wait until the time (limit) runs out"""
    tk.wait(LIMIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_UP)
def test_krt_routes_ipv4_down_rec(exp_devs: str):
    """IPv4: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt4-down-rec", exp_devs, "inet")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_UP)
def test_krt_routes_ipv6_down_rec(exp_devs: str):
    """IPv6: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt6-down-rec", exp_devs, "inet6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_UP)
def test_bird_routes_ipv4_down_rec(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-down-rec", exp_devs, "master4", opts="")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_UP)
def test_bird_routes_ipv6_down_rec(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6-down-rec", exp_devs, "master6", opts="")


@pytest.mark.skipif(False, reason="")
def test_zero_pingpong():
    """Stop and continue the Zero node repeatedly"""
    for k in range(7):
        tk.run_configure("m0", "bird.conf")
        tk.kill_node("m0", "STOP")
        tk.wait(0.1*k**2)
        tk.kill_node("m0", "CONT")
        tk.wait(0.1*k**2)


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait_pingpong_rec():
    """Wait until the time (limit) runs out"""
    tk.wait(LIMIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_UP)
def test_krt_routes_ipv4_pingpong_rec(exp_devs: str):
    """IPv4: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt4-pingpong-rec", exp_devs, "inet")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_UP)
def test_krt_routes_ipv6_pingpong_rec(exp_devs: str):
    """IPv6: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt6-pingpong-rec", exp_devs, "inet6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_UP)
def test_bird_routes_ipv4_pingpong_rec(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-pingpong-rec", exp_devs, "master4", opts="")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_UP)
def test_bird_routes_ipv6_pingpong_rec(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6-pingpong-rec", exp_devs, "master6", opts="")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES_DOWN)
def test_logging(exp_devs: str):
    """Check the log files. There should only DBG, INFO and TRACE messages"""
    tk.test_logs(exp_devs, LOG_OPT, [])
