<!--cf-ebpg-hostname-->
##### Introduction
This case is testing the ability of **bgp** to advertise hostnames, which is
available via the protocol's `hostname enable` switch. We can set the
*hostname* in the global settings and it will be propagated to the **bgp**
protocol, or we can set it separately for each protocol. The same setting
functionality is available for the *router id* without an enable flag.

There are four devices in this test case. One device (`m1`) is placed in the
middle of *star* topology. The rest of devices (`m2`, `m3`, `m4`) are placed
around the middle one. The device `m1` announces different router IDs and
hostnames to its neighbors through BGP. Each devices represents its own
autonomous system (further only *as*).

<br>

##### Topology

---

```
           ┌─────┐
           | as2 |
           |  m2 |
           └─────┘
              |
              |
              |
           ┌─────┐
           | as1 |
           |  m1 |
           └─────┘
          /       \
         /         \
        /           \
 ┌─────┐             ┌─────┐
 | as3 |             | as4 |
 |  m3 |             |  m4 |
 └─────┘             └─────┘
```

<br>

##### Test suite

---

In this test case we are saving:
1. protocol output (`proto-all_m<as_number><device_number>`),
2. bird tables (`master<4|6>_m<as_number><device_number>`).

