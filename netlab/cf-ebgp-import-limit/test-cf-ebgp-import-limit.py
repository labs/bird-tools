import pytest

import tests.kernel as tk
import tests.config as cf


LIMIT = 60
EXPECTED_DEVICES = ("m0", "m1", "m2", "m3", "m4", "m33")

TEST_WAIT = 15

## 1: Basic route state

def test_01_wait():
    """Wait for everything to settle initially"""
    tk.wait(TEST_WAIT if cf.save else 5)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_01_bird_routes_ipv4(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-01-initial", exp_devs, "master4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_01_logging(exp_devs: str):
    """Check the log files. There should only DBG, INFO and TRACE messages"""
    tk.test_logs(exp_devs, [], [])

## 2: Add one route to exceed the import limit

@pytest.mark.skipif(False, reason="")
def test_02_activate_import_limit():
    tk.enable_bird_protocol("m0", "addimp")


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_02_wait():
    """Wait until the time (limit) runs out"""
    tk.wait(TEST_WAIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_02_bird_routes_ipv4(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-02-import-limit", exp_devs, "master4")


LOGS_M33_02 = [
        "<WARN> (Protocol|Channel) ebgp_warn(\\.ipv4)? hits route import limit \\(5\\), action: warn",
        "<WARN> (Protocol|Channel) ebgp_block(\\.ipv4)? hits route import limit \\(5\\), action: block",
        "<WARN> (Protocol|Channel) ebgp_restart(\\.ipv4)? hits route import limit \\(5\\), action: restart",
        "<WARN> (Protocol|Channel) ebgp_disable(\\.ipv4)? hits route import limit \\(5\\), action: disable",
        "<WARN> ebgp_restart: Route limit exceeded, shutting down",
        "<WARN> ebgp_disable: Route limit exceeded, shutting down",
        ]

LOGS_M3_M4_02 = [
        "<RMT> ebgp_push: Received: Maximum number of prefixes reached"
        ]

@pytest.mark.parametrize("exp_devs, exp_messages", [
    pytest.param("m0", []),
    pytest.param("m1", []),
    pytest.param("m2", []),
    pytest.param("m3", LOGS_M3_M4_02),
    pytest.param("m4", LOGS_M3_M4_02),
    pytest.param("m33", LOGS_M33_02),
    ])
def test_02_logging(exp_devs: str, exp_messages: str):
    """Check the log files. There should only DBG, INFO and TRACE messages"""
    tk.test_logs(exp_devs, [], exp_messages)

## 3A: Release the import limit by withdrawing that one route

@pytest.mark.skipif(False, reason="")
def test_03A_release_import_limit():
    tk.disable_bird_protocol("m0", "addimp")


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_03A_wait():
    """Wait until the time (limit) runs out"""
    tk.wait(TEST_WAIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_03A_bird_routes_ipv4(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-03A-import-release", exp_devs, "master4")


## 3B: Wait until ebgp_restart starts again

@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_03B_wait():
    """Wait until the time (limit) runs out"""
    tk.wait(TEST_WAIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_03B_bird_routes_ipv4(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-03B-import-restart", exp_devs, "master4")


## 3C: Re-enable the switched-off BGP

@pytest.mark.skipif(False, reason="")
def test_03C_restart_disabled_bgp():
    tk.enable_bird_protocol("m33", "ebgp_disable")


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_03C_wait():
    """Wait until the time (limit) runs out"""
    tk.wait(TEST_WAIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_03C_bird_routes_ipv4(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-03C-import-enable", exp_devs, "master4")


## 4: Add one route to push the receive limit

@pytest.mark.skipif(False, reason="")
def test_04_push_receive_limit():
    tk.enable_bird_protocol("m0", "addrec")


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_04_wait():
    """Wait until the time (limit) runs out"""
    tk.wait(TEST_WAIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_04_bird_routes_ipv4(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-04-import-limit", exp_devs, "master4")


@pytest.mark.parametrize("exp_devs, exp_messages", [
    pytest.param("m0", []),
    pytest.param("m1", []),
    pytest.param("m2", []),
    pytest.param("m3", LOGS_M3_M4_02),
    pytest.param("m4", LOGS_M3_M4_02),
    pytest.param("m33", LOGS_M33_02),
    ])
def test_04_logging(exp_devs: str, exp_messages: str):
    """Check the log files. There should only DBG, INFO and TRACE messages"""
    tk.test_logs(exp_devs, [], exp_messages)


## 5: Add one more route to exceed the receive limit

@pytest.mark.skipif(False, reason="")
def test_05_activate_receive_limit():
    tk.enable_bird_protocol("m0", "addimp")


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_05_wait():
    """Wait until the time (limit) runs out"""
    tk.wait(TEST_WAIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_05_bird_routes_ipv4(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-05-import-limit", exp_devs, "master4")


LOGS_M33_05 = LOGS_M33_02 + [
        "<WARN> (Protocol|Channel) ebgp_warn(\\.ipv4)? hits route receive limit \\(6\\), action: warn",
        "<WARN> (Protocol|Channel) ebgp_block(\\.ipv4)? hits route receive limit \\(6\\), action: block",
        "<WARN> (Protocol|Channel) ebgp_restart(\\.ipv4)? hits route receive limit \\(6\\), action: restart",
        "<WARN> (Protocol|Channel) ebgp_disable(\\.ipv4)? hits route receive limit \\(6\\), action: disable",
        ]
@pytest.mark.parametrize("exp_devs, exp_messages", [
    pytest.param("m0", []),
    pytest.param("m1", []),
    pytest.param("m2", []),
    pytest.param("m3", LOGS_M3_M4_02),
    pytest.param("m4", LOGS_M3_M4_02),
    pytest.param("m33", LOGS_M33_05),
    ])
def test_05_logging(exp_devs: str, exp_messages: str):
    """Check the log files. There should only DBG, INFO and TRACE messages"""
    tk.test_logs(exp_devs, [], exp_messages)


## 6A: Release the receive limit by withdrawing that one route

@pytest.mark.skipif(False, reason="")
def test_06A_release_receive_limit():
    tk.disable_bird_protocol("m0", "addimp")


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_06A_wait():
    """Wait until the time (limit) runs out"""
    tk.wait(TEST_WAIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_06A_bird_routes_ipv4(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-06A-import-release", exp_devs, "master4")


## 6B: Wait until ebgp_restart starts again

@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_06B_wait():
    """Wait until the time (limit) runs out"""
    tk.wait(TEST_WAIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_06B_bird_routes_ipv4(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-06B-import-restart", exp_devs, "master4")


## 6C: Re-enable the switched-off BGP

@pytest.mark.skipif(False, reason="")
def test_06C_restart_disabled_bgp():
    tk.enable_bird_protocol("m33", "ebgp_disable")


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_06C_wait():
    """Wait until the time (limit) runs out"""
    tk.wait(TEST_WAIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_06C_bird_routes_ipv4(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-06C-import-enable", exp_devs, "master4")


## 7: Add one more route once more to exceed the receive limit

@pytest.mark.skipif(False, reason="")
def test_07_activate_receive_limit():
    tk.enable_bird_protocol("m0", "addimp")


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_07_wait():
    """Wait until the time (limit) runs out"""
    tk.wait(TEST_WAIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_07_bird_routes_ipv4(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-07-import-limit", exp_devs, "master4")


@pytest.mark.parametrize("exp_devs, exp_messages", [
    pytest.param("m0", []),
    pytest.param("m1", []),
    pytest.param("m2", []),
    pytest.param("m3", LOGS_M3_M4_02),
    pytest.param("m4", LOGS_M3_M4_02),
    pytest.param("m33", LOGS_M33_05),
    ])
def test_07_logging(exp_devs: str, exp_messages: str):
    """Check the log files. There should only DBG, INFO and TRACE messages"""
    tk.test_logs(exp_devs, [], exp_messages)


## 8A: Release the receive limit but trigger import limit

@pytest.mark.skipif(False, reason="")
def test_08A_release_receive_limit():
    tk.disable_bird_protocol("m0", "addrec")


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_08A_wait():
    """Wait until the time (limit) runs out"""
    tk.wait(TEST_WAIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_08A_bird_routes_ipv4(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-08A-import-release", exp_devs, "master4")


## 8C: Re-enable the switched-off BGP

@pytest.mark.skipif(False, reason="")
def test_08C_restart_disabled_bgp():
    tk.enable_bird_protocol("m33", "ebgp_disable")


@pytest.mark.skipif(False, reason="")
def test_08C_wait_always():
    """Wait until the time (limit) runs out"""
    tk.wait(TEST_WAIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_08C_bird_routes_ipv4(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-08C-import-enable", exp_devs, "master4")


## 9A: Release the receive limit but trigger import limit

@pytest.mark.skipif(False, reason="")
def test_09A_release_receive_limit():
    tk.disable_bird_protocol("m0", "addimp")


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_09A_wait():
    """Wait until the time (limit) runs out"""
    tk.wait(TEST_WAIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_09A_bird_routes_ipv4(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-09A-import-release", exp_devs, "master4")


## 9B: Wait until ebgp_restart starts again

@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_09B_wait():
    """Wait until the time (limit) runs out"""
    tk.wait(TEST_WAIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_09B_bird_routes_ipv4(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-09B-import-restart", exp_devs, "master4")


## 9C: Re-enable the switched-off BGP

@pytest.mark.skipif(False, reason="")
def test_09C_restart_disabled_bgp():
    tk.enable_bird_protocol("m33", "ebgp_disable")


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_09C_wait():
    """Wait until the time (limit) runs out"""
    tk.wait(TEST_WAIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_09C_bird_routes_ipv4(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-09C-import-enable", exp_devs, "master4")


@pytest.mark.parametrize("exp_devs, exp_messages", [
    pytest.param("m0", []),
    pytest.param("m1", []),
    pytest.param("m2", []),
    pytest.param("m3", LOGS_M3_M4_02),
    pytest.param("m4", LOGS_M3_M4_02),
    pytest.param("m33", LOGS_M33_05),
    ])
def test_10_logging(exp_devs: str, exp_messages: str):
    """Check the log files. There should only DBG, INFO and TRACE messages"""
    tk.test_logs(exp_devs, [], exp_messages)
