<!--cf-ebpg-loop-->
##### Introduction
This case tests the **ebgp** protocols within the circle topology. Each device
is connected only to its own neighbors (e.g. `m1` to `m2` and `m4`). Each
connection has both variants of protocol (ebgp4-IPv4 and ebgp6-IPv6).

There are four devices in this test case. Together they form a closed loop.
Each devices represents its own autonomous system (further only *as*).

List of autonomous systems with relevant devices:
- `as1` - m1,
- `as2` - m2,
- `as3` - m3,
- `as4` - m4.

The designation of each device looks as follows `m<number_of_as>`.

<br>

##### Topology

---

```
      ┌─────┐      ┌─────┐
      | as1 |──────| as4 |
      |  m1 |      |  m4 |
      └─────┘      └─────┘
         |            |
         |            |
         |            |
      ┌─────┐      ┌─────┐
      | as2 |      | as3 |
      |  m2 |-─────|  m3 |
      └─────┘      └─────┘
```

<br>

##### Test suite

---

In this test case we are saving:
1. krt tables (`krt<4|6>_m<as_number><device_number>`).
2. bird tables (`master<4|6>_m<as_number><device_number>`),
