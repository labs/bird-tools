<!--cf-ebpg-star-->
##### Introduction

This test case is testing BGP roles. The central device `m0` has two upstreams
(`m1` and `m2`), two peers (`m3`, `m4`), and two downstreams (`m5` and `m6`).
BGP roles are defined on device `m0` accordingly. There are also BGP roles
defined on device `m2` for all its downstream connections (`m0`, `m4` and `m6`).

Routes propagate properly through `m0`, as defined by BGP roles. On the
contrary, devices `m4` and `m6` do not have defined roles nor filters, so they
leak routes in both directions. These leaks are detected and logged on `m0` and
`m2`.

##### Topology

---

```
    ┌─────┐         ┌─────┐
    | as11|         | as12|
    |  m1 |         |  m2 |
    └─────┘         └─────┘
         \         /   |   \
          \       /    |    \
           \     /     |     \
 ┌─────┐   ┌─────┐   ┌─────┐  |
 | as13|---| as10|---| as14|  |
 |  m3 |   |  m0 |   |  m4 |  |
 └─────┘   └─────┘   └─────┘  |
           /     \           /
          /       \         /
         /         \       /
    ┌─────┐         ┌─────┐
    | as15|         | as16|
    |  m5 |         |  m6 |
    └─────┘         └─────┘
```

<br>

##### Test suite

---

In this test case we are saving:
1. krt tables (`krt<4|6>_m<as_number><device_number>`).
2. bird tables (`master<4|6>_m<as_number><device_number>`),
