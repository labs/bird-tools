import pytest

import tests.kernel as tk
import tests.config as cf


LIMIT = 60
EXPECTED_DEVICES = ("m0", "m1", "m2", "m3", "m4", "m5", "m6")


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait():
    """Wait until the time (limit) runs out"""
    tk.wait(LIMIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv4(exp_devs: str):
    """IPv4: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt4", exp_devs, "inet")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv6(exp_devs: str):
    """IPv6: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt6", exp_devs, "inet6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4", exp_devs, "master4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv6(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6", exp_devs, "master6")


LOGS_M0 = [
    "<RMT> ebgp_p2: Route leak detected - OTC attribute with mismatched ASN \\(12\\)",
    "<RMT> ebgp_p2: Invalid route 10.2.0.0/16 withdrawn",
    "<RMT> ebgp_p2: Invalid route 2001:db8:2::/48 withdrawn",

    "<RMT> ebgp_d2: Route leak detected - OTC attribute from downstream",
    "<RMT> ebgp_d2: Invalid route 10.2.0.0/16 withdrawn",
    "<RMT> ebgp_d2: Invalid route 2001:db8:2::/48 withdrawn"
]

LOGS_M2 = [
    "<RMT> bgp2: Route leak detected - OTC attribute from downstream",
    "<RMT> bgp2: Invalid route 10.0.0.0/16 withdrawn",
    "<RMT> bgp2: Invalid route 10.6.0.0/16 withdrawn",
    "<RMT> bgp2: Invalid route 10.5.0.0/16 withdrawn",
    "<RMT> bgp2: Invalid route 2001:db8::/48 withdrawn",
    "<RMT> bgp2: Invalid route 2001:db8:5::/48 withdrawn",
    "<RMT> bgp2: Invalid route 2001:db8:6::/48 withdrawn",

    "<RMT> bgp3: Route leak detected - OTC attribute from downstream",
    "<RMT> bgp3: Invalid route 10.0.0.0/16 withdrawn",
    "<RMT> bgp3: Invalid route 10.1.0.0/16 withdrawn",
    "<RMT> bgp3: Invalid route 10.3.0.0/16 withdrawn",
    "<RMT> bgp3: Invalid route 10.4.0.0/16 withdrawn",
    "<RMT> bgp3: Invalid route 10.5.0.0/16 withdrawn",
    "<RMT> bgp3: Invalid route 2001:db8::/48 withdrawn",
    "<RMT> bgp3: Invalid route 2001:db8:1::/48 withdrawn",
    "<RMT> bgp3: Invalid route 2001:db8:3::/48 withdrawn",
    "<RMT> bgp3: Invalid route 2001:db8:4::/48 withdrawn",
    "<RMT> bgp3: Invalid route 2001:db8:5::/48 withdrawn"
]

@pytest.mark.parametrize(
    "exp_devs, exp_messages",
    [
        pytest.param("m0", LOGS_M0),
        pytest.param("m1", []),
        pytest.param("m2", LOGS_M2),
        pytest.param("m3", []),
        pytest.param("m4", []),
        pytest.param("m5", []),
        pytest.param("m6", [])
    ],
)
def test_logging(exp_devs: str, exp_messages: list):
    """Check the log files. There should only DBG, INFO and TRACE messages"""
    tk.test_logs(exp_devs, [], exp_messages)

