<!--cf-ebpg-star-->
##### Introduction
This case tests the **ebgp** protocols with one central device. The devices
`m2`, `m3`, `m4` are connected only to device in the middle of
*star* topology (to `m1`). Each connection has both variants of protocol
(ebgp-IPv4 and ebgp-IPv6).

There are four devices in this test case. One device (`m1`) is placed in
the middle of *star* topology. The rest of devices (`m2`, `m3`, `m4`) are placed
around the middle one. Each devices represents its own autonomous system
(further only *as*).

List of autonomous systems with relevant devices:
- `as1` - m1,
- `as2` - m2,
- `as3` - m3,
- `as4` - m4.

The designation of each device looks as follows `m<number_of_as>`.

<br>

##### Topology

---

```
           ┌─────┐
           | as2 |
           |  m2 |
           └─────┘
              |
              |
              |
           ┌─────┐
           | as1 |
           |  m1 |
           └─────┘
          /       \
         /         \
        /           \
 ┌─────┐             ┌─────┐
 | as3 |             | as4 |
 |  m3 |             |  m4 |
 └─────┘             └─────┘
```

<br>

##### Test suite

---

In this test case we are saving:
1. krt tables (`krt<4|6>_m<as_number><device_number>`).
2. bird tables (`master<4|6>_m<as_number><device_number>`),
