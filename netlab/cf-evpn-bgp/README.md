<!--cf-evpn-bgp-->
##### Introduction

This case tests EVPN/BGP setup with VXLAN as overlay network, OSPFv3 used as IGP
routing protocol and IBGP used for VPN signalling.

Nodes `m11` to `m14` are PE routers, nodes `m21` to `m24` are CE nodes of VPN2,
while `m31` to `m34` are CE nodes of VPN3.

The node `m11` serves as a route reflector to distribute EVPN routes between all
PE routers using IBGP. OSPFv3 is used between PE routers for basic IP routing.

The node `m11` is attached to both VPNs VPN2 and VPN3 with two independent
bridge devices, while he node `m12` has a shared bridge with VLANs, using VID 20
for VPN2 and VID 30 for VPN3.

Each VPN uses specific route target. For VPN2, the route target is `(rt, 1, 2)`,
for VPN3, the route target is `(rt, 1, 3)`.

CE nodes in each VPN also run OSPFv3 just to check they are properly connected
together.

Linux iproute2 commands to configure required bridge and tunnel devices:

```
ip link add name $BRIDGE netns $NS type bridge vlan_filtering 1
ip link add name $TUNNEL type vxlan id $VNI local $ROUTER_ADDR dstport 4789 nolearning
ip link set $TUNNEL master $BRIDGE
bridge link set dev $TUNNEL learning off
```

In case of EVPNs associated with VLANs (like on node `m12`), the tunnel device
must be in the appropriate VLAN:

```
bridge vlan add dev $TUNNEL vid $VID pvid untagged
bridge vlan del dev $TUNNEL vid 1
```

<br>

##### Topology

---

```

     ┌──────┐           ┌──────┐
     | VPN2 |           | VPN2 |
     |  m21 |           | m22  |
     └────\─┘           └─/────┘
           \             /
┌──────┐  ┌─\───────────/─┐  ┌──────┐
| VPN3 |  |  \         /  |  | VPN3 |
|  m31 ----- m11 ─── m12 ----- m32  |
└──────┘  |   |       |   |  └──────┘
          |   |  AS1  |   |
┌──────┐  |   |       |   |  ┌──────┐
| VPN3 ----- m14 ─── m13 ----- VPN2 |
|  m34 |  |  /         \  |  | m24  |
└──────┘  └─/───────────\─┘  └──────┘
           /             \
     ┌────/─┐           ┌─\────┐
     | VPN3 |           | VPN2 |
     |  m33 |           | m23  |
     └──────┘           └──────┘

```
