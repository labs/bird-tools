log "bird.log" all;

router id 2;

protocol device {}
protocol kernel kernel6 {
  ipv6 { export all; };
}

protocol kernel kernel4 {
  ipv4 { export all; };
}

protocol static static6 {
  ipv6;
  route 2001:db8:ff02::/48 via 2001:db8:2:1::dead;
}

protocol static static4 {
  ipv4;
  route 192.0.2.2/32 unreachable;
}

protocol ospf v3 ospf6 {
  ipv6;
  area 0 {
    interface "ve0" { stub; };
    interface "ve1" { hello 2; };
  };
}

protocol ospf v2 ospf4 {
  ipv4;
  area 0 {
    interface "ve0" { stub; };
    interface "ve1" { hello 2; };
  };
}

protocol bfd {}

template bgp bgpt {
  ipv4 { import all; export where source ~ [ RTS_STATIC, RTS_BGP ]; };
  ipv6 { import all; export where source ~ [ RTS_STATIC, RTS_BGP ]; };
}

protocol bgp bgp21 from bgpt {
  local 10.0.7.2 as 65533;
  neighbor 10.0.7.1 as 65533;
  bfd { authentication keyed sha1; password "onetwo"; };
}

protocol bgp bgp23 from bgpt {
  local 10.0.7.2 as 65533;
  neighbor 10.0.7.3 as 65533;
  bfd { authentication keyed sha1; password "twothree"; };
}

protocol bgp bgp24 from bgpt {
  local 10.0.7.2 as 65533;
  neighbor 10.0.7.4 as 65533;
  bfd { authentication simple; password "twofour"; };
}

protocol bgp bgp25 from bgpt {
  local 10.0.7.2 as 65533;
  neighbor 10.0.7.5 as 65533;
  bfd { authentication simple; password "twofive"; };
}
