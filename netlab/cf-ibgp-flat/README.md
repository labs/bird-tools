<!--cf-ibgp-flat-->
##### Introduction
This case tests the **ibgp** protocols interconnected within the single
autonomous system. Each device is connected to the rest of the devices (e.g.
`m1` to `m2`, `m3`, `m4`).
Each device has two templates with protocols (template ibgp4, template ibgp6).
Within each template there are three specifig ibgp protocols (e.g. `ibgp4_1`,
`ibgp4_2`, `ibgp4_3`).

There are four devices in this test case. All four devices are in the single
autonomous system (further only *as*).

List of devices systems:
- `as1` - m1, m2, m3, m4.

The designation of each device looks as follows `m<number_of_device>`.

<br>

##### Topology

---

```
┌────┐    ┌────┐
| m1 |    | m4 |
└────┘    └────┘
    \     /
     \   /
      as1
     /   \
    /     \
┌────┐    ┌────┐
| m2 |    | m3 |
└────┘    └────┘
```

<br>

##### Test suite

---

In this test case we are saving:
1. krt tables (`krt<4|6>_m<as_number><device_number>`).
2. bird tables (`master<4|6>_m<as_number><device_number>`),
