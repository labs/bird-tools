<!--cf-ibgp-loop-big-->
##### Introduction
This case tests the **ibgp** protocols interconnected within single
autonomous system. Each device is connected only to its own neighbors,
in the single closed loop (e.g. `m1` to `m2` and `m4`).

There are four devices in this test case. Together they form a closed loop.
All four devices are in the single autonomous system (further only *as*).

After the setup starts up, several reloads are issued to ensure BGP route
refresh is stable and correct.

This case differs from cf-ibgp-loop-big only by the number of routes.

List of devices systems:
- `as1` - m1, m2, m3, m4.

List of protocols for each device:
- `ospf4` - OSPFv3-IPv4,
- `ospf6` - OSPFv3-IPv6,
- `ibgp4` - IPv4,
- `ibgp6` - IPv6.

The designation of each device looks as follows `m<number_of_device>`.

<br>

##### Topology

---

```
┌────┐     ┌────┐
| m1 |─────| m4 |
└────┘     └────┘
  |           |
  |    as1    |
  |           |
┌────┐     ┌────┐
| m2 |-────| m3 |
└────┘     └────┘
```

<br>

##### Test suite

---

In this test case we are saving:
1. krt tables (`krt<4|6>_m<as_number><device_number>`).
2. bird tables (`master<4|6>_m<as_number><device_number>`),
