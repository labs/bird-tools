import pytest

import tests.kernel as tk
import tests.config as cf


LIMIT = 60
EXPECTED_DEVICES = ("m1", "m2", "m3", "m4")

LOG_OPT = ["<WARN> Next hop address .* resolvable through recursive route"]


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait():
    """Wait until the time (limit) runs out"""
    tk.wait(LIMIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv4(exp_devs: str):
    """IPv4: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt4", exp_devs, "inet")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv6(exp_devs: str):
    """IPv6: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt6", exp_devs, "inet6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4", exp_devs, "master4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv6(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6", exp_devs, "master6")


@pytest.mark.skipif(cf.save == True, reason="mode: save")
@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
@pytest.mark.parametrize("proto", ( f"ibgp{b}_{a}" for a in (1, 2, 3) for b in (4, 6)))
def test_bird_reload_once(exp_devs: str, proto: str):
    tk.reload_bird_protocol(exp_devs, proto, "bgp")


def test_wait_reload_once():
    """Wait until the time (limit) runs out"""
    tk.wait(1)


@pytest.mark.skipif(cf.save == True, reason="mode: save")
@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv4_1(exp_devs: str):
    """IPv4: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt4", exp_devs, "inet")


@pytest.mark.skipif(cf.save == True, reason="mode: save")
@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv6_1(exp_devs: str):
    """IPv6: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt6", exp_devs, "inet6")


@pytest.mark.skipif(cf.save == True, reason="mode: save")
@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4_1(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4", exp_devs, "master4")


@pytest.mark.skipif(cf.save == True, reason="mode: save")
@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv6_1(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6", exp_devs, "master6")


@pytest.mark.skipif(cf.save == True, reason="mode: save")
@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
@pytest.mark.parametrize("proto", ( f"ibgp{b}_{a}" for a in (1, 2, 3) for b in (4, 6)))
def test_bird_reload_twice_1(exp_devs: str, proto: str):
    tk.reload_bird_protocol(exp_devs, proto, "bgp")


def test_wait_reload_twice_1():
    """Wait until the time (limit) runs out"""
    tk.wait(1)


@pytest.mark.skipif(cf.save == True, reason="mode: save")
@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
@pytest.mark.parametrize("proto", ( f"ibgp{b}_{a}" for a in (1, 2, 3) for b in (4, 6)))
def test_bird_reload_twice_2(exp_devs: str, proto: str):
    tk.reload_bird_protocol(exp_devs, proto, "bgp")


def test_wait_reload_twice_2():
    """Wait until the time (limit) runs out"""
    tk.wait(1)


@pytest.mark.skipif(cf.save == True, reason="mode: save")
@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv4_2(exp_devs: str):
    """IPv4: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt4", exp_devs, "inet")


@pytest.mark.skipif(cf.save == True, reason="mode: save")
@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv6_2(exp_devs: str):
    """IPv6: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt6", exp_devs, "inet6")


@pytest.mark.skipif(cf.save == True, reason="mode: save")
@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4_2(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4", exp_devs, "master4")


@pytest.mark.skipif(cf.save == True, reason="mode: save")
@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv6_2(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6", exp_devs, "master6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_logging(exp_devs: str):
    """Check the log files. There should only DBG, INFO and TRACE messages"""
    tk.test_logs(exp_devs, LOG_OPT, [])


@pytest.mark.skipif(cf.save == True, reason="mode: save")
@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
@pytest.mark.parametrize("proto", ( f"ibgp{b}_{a}" for a in (1, 2, 3) for b in (4, 6)))
def test_bird_reload_final(exp_devs: str, proto: str):
    tk.reload_bird_protocol(exp_devs, proto, "bgp")


def test_wait_reload_final():
    """Wait until the time (limit) runs out"""
    tk.wait(1)
