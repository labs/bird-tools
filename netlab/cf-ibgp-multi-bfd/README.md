<!--cf-ibgp-flat-->
##### Introduction
This case tests the **ibgp** protocols interconnected within the single
autonomous system with **bfd** sessions on both IPv4 and IPv6. 
Each device is connected to the rest of the devices (`m1` to `m2` and `m2`, `m1`).
Each device has two ibgp protocols. One for IPv4 and one for IPv6 (e.g. `ibgp4`, `ibgp6`).

There are two devices in this test case and they are in the single
autonomous system (further only *as*).

List of devices systems:
- `as1` - m1, m2.

The designation of each device looks as follows `m<number_of_device>`.

<br>

##### Topology

---

```
┌────┐         ┌────┐
| m1 |---as1---| m2 |
└────┘         └────┘
```

<br>