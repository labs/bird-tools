<!--cf-kernel-fulltable-->
##### Introduction

This case tests putting routes into kernel. Nothing more.

<br>

##### Test suite

---

In this test case we are saving:
1. krt tables (`krt<4|6>_m<as_number><device_number>`).
2. bird tables (`master<4|6>_m<as_number><device_number>`),
