import pytest

import tests.kernel as tk
import tests.config as cf


LIMIT = 20
TEST_WAIT = 10
EXPECTED_DEVICES = ( "m1", )
LOG_OPT = [
        "<WARN> Kernel dropped some netlink messages, will resync on next scan.",
        ]


def test_wait():
    """Wait until the time (limit) runs out"""
    tk.wait(LIMIT if cf.save else TEST_WAIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv6(exp_devs: str):
    """IPv6: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt6", exp_devs, "inet6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv6(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6", exp_devs, "master6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_logging(exp_devs: str):
    """Check the log files. There should only DBG, INFO and TRACE messages"""
    tk.test_logs(exp_devs, LOG_OPT, [])

