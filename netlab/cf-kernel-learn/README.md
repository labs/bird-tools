<!--cf-kernel-learn-->
##### Introduction

This case tests external route learning. We add and remove such routes, testing that BIRD reacts adequately.

<br>

##### Test suite

---

In this test case we are saving:
1. krt tables (`krt<4|6>_m<as_number><device_number>`).
2. bird tables (`master<4|6>_m<as_number><device_number>`),
