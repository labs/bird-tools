import pytest

import tests.kernel as tk
import tests.config as cf


LIMIT = 5
EXPECTED_DEVICES = ( "m1", )
LOG_OPT = []


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait():
    """Wait until the time (limit) runs out"""
    tk.wait(LIMIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv4(exp_devs: str):
    """IPv4: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt4", exp_devs, "inet")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv6(exp_devs: str):
    """IPv6: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt6", exp_devs, "inet6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4", exp_devs, "master4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv6(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6", exp_devs, "master6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_add_routes(exp_devs: str):
    """Add routes"""
    tk.run_on_machine(exp_devs, "ip route add 10.12.0.0/14 via 10.1.1.111 metric 42")
    tk.run_on_machine(exp_devs, "ip route add 10.12.0.0/14 via 10.1.1.112 metric 38")
    tk.run_on_machine(exp_devs, "ip route add 10.16.0.0/14 via 10.1.1.112 metric 38")

    tk.run_on_machine(exp_devs, "ip -6 route add 2001:db8:fee1::/48 via 2001:db8:1:1:1:1:1:1 metric 64")
    tk.run_on_machine(exp_devs, "ip -6 route add 2001:db8:fee1::/48 via 2001:db8:1:1:1:2:1:2 metric 66")
    tk.run_on_machine(exp_devs, "ip -6 route add 2001:db8:beef::/48 via 2001:db8:1:1:1:2:1:2 metric 66")


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait2():
    """Wait until the time (limit) runs out"""
    tk.wait(LIMIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv4_learnt(exp_devs: str):
    """IPv4: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt4-learnt", exp_devs, "inet")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv6_learnt(exp_devs: str):
    """IPv6: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt6-learnt", exp_devs, "inet6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4_learnt(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-learnt", exp_devs, "master4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv6_learnt(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6-learnt", exp_devs, "master6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_drop_routes(exp_devs: str):
    """Drop routes"""
    tk.run_on_machine(exp_devs, "ip route del 10.20.30.0/24 metric 64")
    tk.run_on_machine(exp_devs, "ip route del 10.12.0.0/14 metric 38")
    tk.run_on_machine(exp_devs, "ip route del 10.16.0.0/14 metric 38")
    tk.run_on_machine(exp_devs, "ip route del 10.16.0.0/14 metric 86")

    tk.run_on_machine(exp_devs, "ip -6 route del 2001:db8:feed::/48 metric 64")
    tk.run_on_machine(exp_devs, "ip -6 route del 2001:db8:fee1::/48 metric 64")
    tk.run_on_machine(exp_devs, "ip -6 route del 2001:db8:beef::/48 metric 66")
    tk.run_on_machine(exp_devs, "ip -6 route del 2001:db8:beef::/48 metric 86")


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait3():
    """Wait until the time (limit) runs out"""
    tk.wait(LIMIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv4_unlearnt(exp_devs: str):
    """IPv4: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt4-unlearnt", exp_devs, "inet")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv6_unlearnt(exp_devs: str):
    """IPv6: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt6-unlearnt", exp_devs, "inet6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4_unlearnt(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-unlearnt", exp_devs, "master4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv6_unlearnt(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6-unlearnt", exp_devs, "master6")



@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_logging(exp_devs: str):
    """Check the log files. There should only DBG, INFO and TRACE messages"""
    tk.test_logs(exp_devs, LOG_OPT, [])

