<!--cf-mpls-bgp-l3vpn-->
##### Introduction

This case tests L3VPN MPLS routing with EBGP used as (label distributing) IGP
routing protocol and IBGP used for VPN signalling.

The AS1 uses MPLS backbone to handle VPN traffic. Routers in this AS have both
regular routing tables (`master4` and `master4`), LSP routing tables (`lsptab4` and
`lsptab6`), and VPN routing table (`vpntab4` and `vpntab6`). Regular routing tables
are used to forward IP traffic, while LSP routing tables are used to resolve
recursive next hops.

Protocols `abgp*` serve as IGP routing protocol for both regular IP routing (in
SAFI 1) and LSP routing (in SAFI 4). These IGP routes are distinguished from
regular external routes by having preference 150.

Protocols `ibgp*` are regular IBGP to exchange external routes (not used in this
test case) and VPN routes (SAFI 128) inside AS1 (`m11` serves as a route
reflector). These protocols use LSP routing tables to resolve recursive next
hops, therefore VPN traffic is encapsulated in MPLS.

Nodes in AS1 also have two VRFs, representing VPN2 and VPN3, and associated IP
routing tables `vrf2*` and `vrf3*`. These tables receive routes from connected
CEs using BGP protocols `ebgp*`, while L3VPN protocols `l3vpn*` convert between
IP routes in tables `vrf*` and BGP/VPN routes in tables `vpntab*`.

Therefore, VPN IPv4 routing from e.g. `m22` to `m24` works this way:

- First, a route is propagated by EBGP from `m22` to the IP table `vrf2v4` in `m12`.

- Then it is translated by the L3VPN protocol `l3vpn2` to a BGP/VPN route with
  route distinguisher 1:11 and MPLS-labeled next hop, and stored in the VPN
  table `vpntab4`.

- Then it is propagated through the IBGP protocol `ibgp1v4` to `m11`, who
  serves as a route reflector, propagating it further to `m14` (and `m13`).

- At `m14`, it is stored in VPN table `vpntab4` and translated by the L3VPN
  protocol from BGP/VPN route back to IP route, which is stored in the IP table
  `vrf2v4`.

- Last, such IP route is propagated by EBGP from `m14` to `m24`.

Which BGP/VPN routes are translated to which VRFs is controlled by route target
extended communities. For VPN2, one route target `(rt, 1, 2)` is used to signal
point-to-point VPN. For VPN3, several route targets are used to signal
hub-and-spoke VPN (although without re-export on the hub).

Nodes `m21`-`m24` (for VPN2) and `m31`-`m34` (for VPN2) and just simple non-MPLS
CE endpoints, using EBGP to exchange IP routing information with PE routers from
AS1.

<br>

##### Topology

---

```

     ┌──────┐           ┌──────┐
     | VPN2 |           | VPN2 |
     |  m21 |           | m22  |
     └────\─┘           └─/────┘
           \             /
┌──────┐  ┌─\───────────/─┐  ┌──────┐
| VPN3 |  |  \         /  |  | VPN3 |
|  m31 ----- m11 ─── m12 ----- m32  |
└──────┘  |   |       |   |  └──────┘
          |   |  AS1  |   |
┌──────┐  |   |       |   |  ┌──────┐
| VPN3 ----- m14 ─── m13 ----- VPN3 |
|  m34 |  |  /         \  |  | m33  |
└──────┘  └─/───────────\─┘  └──────┘
           /             \
     ┌────/─┐           ┌─\────┐
     | VPN2 |           | VPN2 |
     |  m24 |           | m23  |
     └──────┘           └──────┘

```
