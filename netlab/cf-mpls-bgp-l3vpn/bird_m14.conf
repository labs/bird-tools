log "bird.log" all;

router id 10.0.1.4;

ipv4 table master4;
ipv6 table master6;

ipv4 table lsptab4;
ipv6 table lsptab6;

vpn4 table vpntab4;
vpn6 table vpntab6;

mpls domain mdom {
	label range dynamic  { start 4000; length 1000; };
	label range vrfrange { start 4800; length  200; };
};

mpls table mtab;

protocol device {
	scan time 10;
}

protocol kernel kernel1v4 {
	scan time 10;
	ipv4 { export all; };
}

protocol kernel kernel1v6 {
	scan time 10;
	ipv6 { export all; };
}

protocol kernel kernelm {
	scan time 10;
	mpls { export where dest = RTD_UNICAST; };
}

protocol direct direct1 {
	vrf default;
	ipv4;
	ipv6;
}

protocol static static1v4 {
	vrf default;
	ipv4 { table lsptab4; };
	mpls;
	route 10.1.4.1/32 via "lo";
}

protocol static static1v6 {
	ipv6 { table lsptab6; };
	mpls;
	route 2001:db8:1:4::1/128 via "lo";
}

filter set_metric {
	igp_metric = bgp_path.len * 100 + bgp_path.first;
	accept;
}

protocol bgp abgp1v4 {
	vrf default;
	ipv4 {
		import filter set_metric;
		export where preference > 100;
		preference 150;
	};
	ipv4 mpls {
		import filter set_metric;
		export where preference > 100;
		table lsptab4;
		preference 150;
	};
	mpls;
	local 10.1.14.1 as 14;
	neighbor 10.1.14.2 as 11;
}

protocol bgp abgp2v4 {
	vrf default;
	ipv4 {
		import filter set_metric;
		export where preference > 100;
		preference 150;
	};
	ipv4 mpls {
		import filter set_metric;
		export where preference > 100;
		table lsptab4;
		preference 150;
	};
	mpls;
	local 10.1.13.2 as 14;
	neighbor 10.1.13.1 as 13;
}

protocol bgp abgp1v6 {
	vrf default;
	ipv6 {
		import filter set_metric;
		export where preference > 100;
		preference 150;
	};
	ipv6 mpls {
		import filter set_metric;
		export where preference > 100;
		table lsptab6;
		preference 150;
	};
	mpls;
	local 2001:db8:1:14::1 as 14;
	neighbor 2001:db8:1:14::2 as 11;
}

protocol bgp abgp2v6 {
	vrf default;
	ipv6 {
		import filter set_metric;
		export where preference > 100;
		preference 150;
	};
	ipv6 mpls {
		import filter set_metric;
		export where preference > 100;
		table lsptab6;
		preference 150;
	};
	mpls;
	local 2001:db8:1:13::2 as 14;
	neighbor 2001:db8:1:13::1 as 13;
}


protocol bgp ibgp1v4 {
	vrf default;
	ipv4 {
		import all;
		export where preference = 100;
		igp table lsptab4;
		next hop self;
	};
	vpn4 mpls {
		import all;
		export all;
		igp table lsptab4;
		next hop self;
	};
	mpls {
		label policy aggregate;
	};
	local 10.1.4.1 as 1;
	neighbor 10.1.1.1 as 1;
}

protocol bgp ibgp1v6 {
	vrf default;
	ipv6 {
		import all;
		export where preference = 100;
		igp table lsptab6;
		next hop self;
	};
	vpn6 mpls {
		import all;
		export all;
		igp table lsptab6;
		next hop self;
	};
	mpls {
		label policy aggregate;
	};
	local 2001:db8:1:4::1 as 1;
	neighbor 2001:db8:1:1::1 as 1;
}


ipv4 table vrf2v4;
ipv6 table vrf2v6;

protocol kernel kernel2v4 {
	vrf "vrf2";
	ipv4 { table vrf2v4; export all; };
	kernel table 200;
	scan time 10;
}

protocol kernel kernel2v6 {
	vrf "vrf2";
	ipv6 { table vrf2v6; export all; };
	kernel table 200;
	scan time 10;
}

protocol direct direct2 {
	vrf "vrf2";
	ipv4 { table vrf2v4; };
	ipv6 { table vrf2v6; };
}

protocol l3vpn l3vpn2 {
	vrf "vrf2";
	ipv4 { table vrf2v4; };
	ipv6 { table vrf2v6; };
	vpn4 { table vpntab4; };
	vpn6 { table vpntab6; };
	mpls { label policy aggregate; label range vrfrange; };

	rd 1:41;
	route target (rt, 1, 2);
}

protocol bgp ebgp2v4 {
	vrf "vrf2";
	ipv4 { table vrf2v4; import all; export all; };
	local 10.1.14.1 as 1;
	neighbor 10.1.14.2 as 24;
}

protocol bgp ebgp2v6 {
	vrf "vrf2";
	ipv6 { table vrf2v6; import all; export all; };
	local 2001:db8:1:14::1 as 1;
	neighbor 2001:db8:1:14::2 as 24;
}


ipv4 table vrf3v4;
ipv6 table vrf3v6;

protocol kernel kernel3v4 {
	vrf "vrf3";
	ipv4 { table vrf3v4; export all; };
	kernel table 300;
	scan time 10;
}

protocol kernel kernel3v6 {
	vrf "vrf3";
	ipv6 { table vrf3v6; export all; };
	kernel table 300;
	scan time 10;
}

protocol direct direct3 {
	vrf "vrf3";
	ipv4 { table vrf3v4; };
	ipv6 { table vrf3v6; };
}

protocol l3vpn l3vpn3 {
	vrf "vrf3";
	ipv4 { table vrf3v4; };
	ipv6 { table vrf3v6; };
	vpn4 { table vpntab4; };
	vpn6 { table vpntab6; };
	mpls { label policy vrf; label range vrfrange; };

	rd 1:42;
	import target (rt, 1, 30);
	export target (rt, 1, 34);
}

protocol bgp ebgp3v4 {
	vrf "vrf3";
	ipv4 { table vrf3v4; import all; export all; };
	local 10.1.14.1 as 1;
	neighbor 10.1.14.2 as 34;
}

protocol bgp ebgp3v6 {
	vrf "vrf3";
	ipv6 { table vrf3v6; import all; export all; };
	local 2001:db8:1:14::1 as 1;
	neighbor 2001:db8:1:14::2 as 34;
}
