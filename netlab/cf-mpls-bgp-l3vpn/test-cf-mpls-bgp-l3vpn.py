import pytest

import tests.kernel as tk
import tests.config as cf


LIMIT = 60
MPLS_DEVICES = ["m11", "m12", "m13", "m14"]
ALL_DEVICES = ["m11", "m12", "m13", "m14", "m21", "m22", "m23", "m24", "m31", "m32", "m33", "m34"]
LOG_OPT = []


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait():
    tk.wait(LIMIT)


# BIRD table tests

@pytest.mark.parametrize("dev", ALL_DEVICES)
@pytest.mark.parametrize("tab", ["master4", "master6"])
def test_bird_routes_base(dev: str, tab: str):
    tk.test_bird_routes(tab, dev, tab)

@pytest.mark.parametrize("dev", MPLS_DEVICES)
@pytest.mark.parametrize("tab", ["lsptab4", "lsptab6", "vpntab4", "vpntab6", "vrf2v4", "vrf2v6", "vrf3v4", "vrf3v6"])
def test_bird_routes_mpls_m0(dev: str, tab: str):
    tk.test_bird_routes(tab + "-m0", dev, tab)

@pytest.mark.parametrize("dev", MPLS_DEVICES)
def test_mpls_map(dev: str):
    tk.make_mpls_map("mpls-map", dev, [])

@pytest.mark.parametrize("dev", MPLS_DEVICES)
@pytest.mark.parametrize("tab", ["lsptab4", "lsptab6", "vpntab4", "vpntab6", "vrf2v4", "vrf2v6", "vrf3v4", "vrf3v6", "mtab"])
def test_bird_routes_mpls_m1(dev: str, tab: str):
    tk.test_bird_routes_mpls(tab + "-m1", dev, tab)


# Kernel table tests

@pytest.mark.parametrize("dev", ALL_DEVICES)
@pytest.mark.parametrize("tab,fam", [("krt4", "inet"), ("krt6", "inet6")])
def test_krt_routes_base(dev: str, tab: str, fam: str):
    tk.test_krt_routes(tab, dev, fam)

@pytest.mark.parametrize("dev", MPLS_DEVICES)
@pytest.mark.parametrize("tab,fam,num", [("krt4vrf2", "inet", "200"), ("krt6vrf2", "inet6", "200"), ("krt4vrf3", "inet", "300"), ("krt6vrf3", "inet6", "300"), ("krtmpls", "mpls", "main")])
def test_krt_routes_vrf(dev: str, tab: str, fam: str, num: str):
    tk.test_krt_routes_mpls(tab, dev, fam, num)


# Empty logs

@pytest.mark.parametrize("dev", ALL_DEVICES)
def test_logging(dev: str):
    tk.test_logs(dev, LOG_OPT, [])
