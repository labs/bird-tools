<!--cf-mpls-bgp-->
##### Introduction

This case tests basic MPLS routing with EBGP used as (label distributing) IGP
routing protocol and IBGP used for usual distribution of external routing
information inside AS.

The AS1 uses MPLS backbone to handle transit traffic. Routers in this AS have
both regular routing tables (master4 and master4) and LSP routing tables
(lsptab4 and lsptab6). Regular routing tables are used to forward IP traffic,
while LSP routing tables are used to resolve recursive next hops.

Protocols `abgp*` serve as IGP routing protocol for both regular IP routing (in
SAFI 1) and LSP routing (in SAFI 4). These IGP routes are distinguished from
regular external routes by having preference 150.

Protocols `ebgp*` are regular EBGP to exchange external routes between AS1 and
AS2-AS4. Protocols `ibgp*` are regular IBGP to exchange external routes inside
AS1 (`m11` serves as a route reflector). These protocols use LSP routing tables
to resolve recursive next hops, therefore incoming external traffic is
encapsulated in MPLS, forwarded through AS1 and then decapsulated at egress.

Nodes `m20`-`m50` are just simple non-MPLS endpoints, announcing their ranges
through EBGP to AS1.

<br>

##### Topology

---

```

    ┌─────┐           ┌─────┐
    | AS2 |           | AS3 |
    | m20 |           | m30 |
    └───\─┘           └─/───┘
         \             /
        ┌─\───────────/─┐
        |  \         /  |
        |  m11 ─── m12  |
        |   |       |   |
        |   |  AS1  |   |
        |   |       |   |
        |  m14 ─── m13  |
        |  /         \  |
        └─/───────────\─┘
         /             \
    ┌───/─┐           ┌─\───┐
    | AS5 |           | AS4 |
    | m50 |           | m40 |
    └─────┘           └─────┘

```
