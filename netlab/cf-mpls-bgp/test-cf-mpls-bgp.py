import pytest

import tests.kernel as tk
import tests.config as cf


LIMIT = 60
MPLS_DEVICES = ["m11", "m12", "m13", "m14"]
ALL_DEVICES = ["m11", "m12", "m13", "m14", "m20", "m30", "m40", "m50"]
LOG_OPT = []


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait():
    tk.wait(LIMIT)


# BIRD table tests

@pytest.mark.parametrize("dev", ALL_DEVICES)
@pytest.mark.parametrize("tab", ["master4", "master6"])
def test_bird_routes_base(dev: str, tab: str):
    tk.test_bird_routes(tab, dev, tab)

@pytest.mark.parametrize("dev", MPLS_DEVICES)
@pytest.mark.parametrize("tab", ["lsptab4", "lsptab6"])
def test_bird_routes_mpls_m0(dev: str, tab: str):
    tk.test_bird_routes(tab + "-m0", dev, tab)

@pytest.mark.parametrize("dev", MPLS_DEVICES)
def test_mpls_map(dev: str):
    tk.make_mpls_map("mpls-map", dev, [])

@pytest.mark.parametrize("dev", MPLS_DEVICES)
@pytest.mark.parametrize("tab", ["lsptab4", "lsptab6", "mtab"])
def test_bird_routes_mpls_m1(dev: str, tab: str):
    tk.test_bird_routes_mpls(tab + "-m1", dev, tab)


# Kernel table tests

@pytest.mark.parametrize("dev", ALL_DEVICES)
@pytest.mark.parametrize("tab,fam", [("krt4", "inet"), ("krt6", "inet6")])
def test_krt_routes_base(dev: str, tab: str, fam: str):
    tk.test_krt_routes_mpls(tab, dev, fam)

@pytest.mark.parametrize("dev", MPLS_DEVICES)
def test_krt_routes_mpls(dev: str):
    tk.test_krt_routes_mpls("krtmpls", dev, "mpls")



@pytest.mark.skipif(cf.save == True, reason="mode: save")
@pytest.mark.parametrize("exp_devs", MPLS_DEVICES)
@pytest.mark.parametrize("proto", ( f"\"{p}bgp*\"" for p in "aie"))
def test_bird_reload_once(exp_devs: str, proto: str):
    tk.reload_bird_protocol(exp_devs, proto, "bgp")


def test_wait_reload_once():
    """Wait until the time (limit) runs out"""
    tk.wait(2)


# Empty logs

@pytest.mark.parametrize("dev", ALL_DEVICES)
def test_logging(dev: str):
    tk.test_logs(dev, LOG_OPT, [])
