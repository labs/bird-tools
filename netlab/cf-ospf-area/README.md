<!--cf-ospf-area-->
##### Introduction
This case tests the **ospf** protocols among different areas. The backbone area
also contains list of propagated IP ranges (`networks`). Also devices
in the area 2 are configured as NSSA(limited propagation of external routes).

There are four central devices on broadcast network. Each device has one
neighbor outside the central network (connected with point-to-point networks).

In this test case, the topology is divided into three different areas(with
the indexes `0`, `1` and `2`).

List of areas with relevant devices:
- `area 0` - (backbone) m3, m4, m5, m6,
- `area 1` - m1, m2, m3, m4,
- `area 2` - (not-so-stubby-area ~ NSSA) m5, m6, m7, m8.

There are three different OSPF protocols. Protocols `ospf4` is OSPFv2, `ospf5`
is OSPFv3-IPv4 and protocol `ospf6` is OSPFv3-IPv6.

The links between devices `m1-m4` are type point-to-point, whereas links
between devices `m5-m8` are type broadcast.

<br>

##### Topology

---

```
       ┌─────┐
  m1---|m3 m5|---m7
  |    | \ / |    |
  |    |  O  |    |
  |    | / \ |    |
  m2---|m4 m6|---m8
       └─────┘
```

<br>

##### Test suite

---

In this test case we are saving:
1. ospf neighbors (`neighbors<4|6>_m<device_number>`),
2. bird tables (`master<4|6>_m<device_numebr>`),
3. krt tables (`krt<4|6>_m<device_number>`).
