<!--cf-ospf-authentication-->
##### Introduction
This case tests the **authentication** option for the **ospf** protocols.
Furthermore, this test case has three successive stages:
1. testing the original settings,
2. reconfiguration with another config files (suffix `_v2`) and testing them,
3. bird command `configure undo` + testing the original settings.

There are four central devices on broadcast network. Each device has one
neighbor outside the central network (connected with point-to-point networks).

Under this case, interfaces have option `authentication` with different
values(`simple|cryptographic`).

There are three different OSPF protocols. Protocols `ospf4` is OSPFv2, `ospf5`
is OSPFv3-IPv4 and protocol `ospf6` is OSPFv3-IPv6.

The links between devices `m1-m4` are type point-to-point, whereas links
between devices `m5-m8` are type broadcast.

<br>

##### Topology

---

Original settings IPv4:
```
       ┌─────┐
  m1-x-|m3 m5|---m7
  |    | \ / |    |
  |    |  O  |    |
  |    | / \ |    |
  m2---|m4 m6|-x-m8
       └─────┘
```

List of protocols with related authentication:
- OSPFv2: `plaintext` (m1-m3) --> with mismatched passwords,
- OSPFv2: `md5` (m1-m2) --> matching passwords,
- OSPFv3-IPv4: `hmac-sha-256` (m7-m8) --> matching passwords,
- OSPFv2: `hmac-sha-256` (m2-m4) --> matching passwords,
- OSPFv3-IPv4: `hmac-sha-512` (m6-m8) --> with mismatched passwords.

Original settings IPv6:
```
       ┌─────┐
  m1---|m3 m5|---m7
  |    | \ / |    |
  |    |  O  |    |
  |    | / \ |    |
  m2---|m4 m6|-x-m8
       └─────┘
```

List of protocols with related authentication:
- OSPFv3-IPv6: `hmac-sha-1` (m5-m7) --> matching passwords,
- OSPFv3-IPv6: `hmac-sha-256` (m7-m8) --> matching passwords,
- OSPFv3-IPv6: `hmac-sha-512` (m6-m8) --> with mismatched passwords.

After reconfiguration IPv4:
```
       ┌─────┐
  m1───|m3 m5|───m7
  |    | \ / |    |
  x    |  O  |    x
  |    | / \ |    |
  m2─x─|m4 m6|───m8
       └─────┘
```

List of protocols with related authentication after reconfiguration:
- OSPFv2: `plaintext` (m1-m3) --> matching passwords,
- OSPFv2: `md5` (m1-m2) --> with mismatched passwords,
- OSPFv3-IPv4: `hmac-sha-256` (m7-m8) --> with mismatched passwords,
- OSPFv2: `hmac-sha-256` (m2-m4) --> with mismatched passwords,
- OSPFv3-IPv4: `hmac-sha-512` (m6-m8) --> matching passwords.

After reconfiguration IPv6:
```
       ┌─────┐
  m1───|m3 m5|─x─m7
  |    | \ / |    |
  |    |  O  |    x
  |    | / \ |    |
  m2─x─|m4 m6|───m8
       └─────┘
```

List of protocols with related authentication after reconfiguration:
- OSPFv3-IPv6: `hmac-sha-1` (m5-m7) --> with mismatched passwords,
- OSPFv3-IPv6: `hmac-sha-256` (m7-m8) --> with mismatched passwords,
- OSPFv3-IPv6: `hmac-sha-512` (m6-m8) --> matching passwords.

<br>

##### Test suite

---

In this test case we are saving:
1. original ospf neighbors (`neighbors<4|6>_m<device_number>`),
2. original bird tables (`master<4|6>_m<device_numebr>`),
3. original krt tables (`krt<4|6>_m<device_number>`),
4. after reconf. ospf neighbors (`neighbors<4|6>_v2_m{device_number}`),
5. after reconf. bird tables (`neighbors<4|6>_v2_m{device_number}`),
6. reconf. undo ospf neighbors (`neighbors<4|6>_v3_m{device_number}`),
7. reconf. undo bird tables (`master<4|6>_v3_m{device_number}`),
