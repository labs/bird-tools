import pytest

import tests.kernel as tk
import tests.config as cf


LIMIT = 60
EXPECTED_DEVICES = ("m1", "m2", "m3", "m4", "m5", "m6", "m7", "m8")
LOG_OPT = [
    "<WARN> ospf.: Cannot find next hop for LSA",
    "<AUTH> ..."
]


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait():
    """Wait until the time (limit) runs out"""
    tk.wait(LIMIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_neighbors_ospf4(exp_devs: str):
    """IPv4: get the list of BIRD neighbors and check it"""
    tk.test_ospf_neighbors(key="neighbors4", dev=exp_devs, opts="ospf4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_neighbors_ospf5(exp_devs: str):
    """IPv4: get the list of BIRD neighbors and check it"""
    tk.test_ospf_neighbors(key="neighbors5", dev=exp_devs, opts="ospf5")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_neighbors_ospf6(exp_devs: str):
    """IPv6: get the list of BIRD neighbors and check it"""
    tk.test_ospf_neighbors(key="neighbors6", dev=exp_devs, opts="ospf6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4", exp_devs, "master4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4_ospf3(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master5", exp_devs, "master5")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv6(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6", exp_devs, "master6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv4(exp_devs: str):
    """IPv4: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt4", exp_devs, "inet")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv4_ospf3(exp_devs: str):
    """IPv4: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt5", exp_devs, "inet", "100")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv6(exp_devs: str):
    """IPv6: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt6", exp_devs, "inet6")


LOGS_M1 = [
    "<AUTH> ospf4: Authentication failed for nbr 10.0.0.3 on ve2 - wrong password",
    "<AUTH> ospf5: Authentication failed for nbr 10.0.0.3 on ve2 - wrong authentication code",
    "<AUTH> ospf6: Authentication failed for nbr 10.0.0.3 on ve2 - wrong authentication code"
]

LOGS_M8 = [
    "<AUTH> ...",
    "<AUTH> ospf4: Authentication failed for nbr 10.0.0.6 on ve1 - wrong authentication code",
    "<AUTH> ospf5: Authentication failed for nbr 10.0.0.6 on ve1 - wrong authentication code",
    "<AUTH> ospf6: Authentication failed for nbr 10.0.0.6 on ve1 - wrong authentication code",
]


@pytest.mark.parametrize(
    "exp_devs, exp_messages",
    [
        pytest.param("m1", LOGS_M1),
        pytest.param("m4", []),
        pytest.param("m7", []),
        pytest.param("m8", LOGS_M8)
    ],
)
def test_logging_reconfigure(exp_devs: str, exp_messages: list):
    """Check the log files. There should be only DBG, INFO and TRACE messages"""
    tk.test_logs(exp_devs, LOG_OPT, exp_messages)
    tk.clean_log(exp_devs)


@pytest.mark.parametrize("exp_devs", ["m1", "m4", "m7", "m8"])
def test_ospf_reconfigure(exp_devs: str):
    """Reconfigure some devices with config file 'bird_v2.conf'"""
    tk.run_configure(exp_devs, "bird_v2.conf")


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait_v2():
    """Wait until the time (limit) runs out"""
    tk.wait(60)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_neighbors_ospf4_v2(exp_devs: str):
    """IPv4: get the list of BIRD neighbors and check it"""
    tk.test_ospf_neighbors(key="neighbors4-v2", dev=exp_devs, opts="ospf4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_neighbors_ospf5_v2(exp_devs: str):
    """IPv4: get the list of BIRD neighbors and check it"""
    tk.test_ospf_neighbors(key="neighbors5-v2", dev=exp_devs, opts="ospf5")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_neighbors_ospf6_v2(exp_devs: str):
    """IPv6: get the list of BIRD neighbors and check it"""
    tk.test_ospf_neighbors(key="neighbors6-v2", dev=exp_devs, opts="ospf6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4_v2(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-v2", exp_devs, "master4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4_ospf3_v2(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master5-v2", exp_devs, "master5")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv6_v2(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6-v2", exp_devs, "master6")


LOGS_M1 = [
    "<AUTH> ospf4: Authentication failed for nbr 10.0.0.2 on ve1 - wrong authentication code",
    "<AUTH> ospf5: Authentication failed for nbr 10.0.0.3 on ve2 - wrong authentication code",
    "<AUTH> ospf6: Authentication failed for nbr 10.0.0.3 on ve2 - wrong authentication code"
]

LOGS_M4 = [
    "<AUTH> ospf4: Authentication failed for nbr 10.0.0.2 on ve2 - wrong authentication code",
]

LOGS_M7 = [
    "<AUTH> ospf6: Authentication failed for nbr 10.0.0.8 on ve1 - wrong authentication code",
]

LOGS_M8 = [
    "<AUTH> ospf4: Authentication failed for nbr 10.0.0.6 on ve1 - wrong authentication code",
    "<AUTH> ospf6: Authentication failed for nbr 10.0.0.6 on ve1 - wrong authentication code",
    "<AUTH> ospf6: Authentication failed for nbr 10.0.0.7 on ve2 - wrong authentication code",
]

@pytest.mark.parametrize(
    "exp_devs, exp_messages",
    [
        pytest.param("m1", LOGS_M1),
        pytest.param("m4", LOGS_M4),
        pytest.param("m7", LOGS_M7),
        pytest.param("m8", LOGS_M8)
    ],
)
def test_logging_configure_undo(exp_devs: str, exp_messages: list):
    """Check the log files. There should only DBG, INFO and TRACE messages"""
    tk.test_logs(exp_devs, LOG_OPT, exp_messages)
    tk.clean_log(exp_devs)


@pytest.mark.parametrize("exp_devs", ["m1", "m4", "m7", "m8"])
def test_ospf_configure_undo(exp_devs: str):
    """Reconfigure some devices with command 'configure undo'"""
    tk.run_configure_undo(exp_devs)


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait_v3():
    """Wait until the time (limit) runs out"""
    tk.wait(60)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_neighbors_ospf4_v3(exp_devs: str):
    """IPv4: get the list of BIRD neighbors and check it"""
    tk.test_ospf_neighbors(key="neighbors4-v3", dev=exp_devs, opts="ospf4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_neighbors_ospf5_v3(exp_devs: str):
    """IPv4: get the list of BIRD neighbors and check it"""
    tk.test_ospf_neighbors(key="neighbors5-v3", dev=exp_devs, opts="ospf5")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_neighbors_ospf6_v3(exp_devs: str):
    """IPv6: get the list of BIRD neighbors and check it"""
    tk.test_ospf_neighbors(key="neighbors6-v3", dev=exp_devs, opts="ospf6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4_v3(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4-v3", exp_devs, "master4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4_ospf3_v3(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master5-v3", exp_devs, "master5")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv6_v3(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6-v3", exp_devs, "master6")


LOGS_M1 = [
    "<AUTH> ospf4: Authentication failed for nbr 10.0.0.3 on ve2 - wrong password",
    "<AUTH> ospf5: Authentication failed for nbr 10.0.0.3 on ve2 - wrong authentication code",
    "<AUTH> ospf6: Authentication failed for nbr 10.0.0.3 on ve2 - wrong authentication code"
]

LOGS_M2 = [
    "<AUTH> ospf4: Authentication failed for nbr 10.0.0.1 on ve2 - wrong authentication code",
    "<AUTH> ospf4: Authentication failed for nbr 10.0.0.4 on ve1 - wrong authentication code",
]

LOGS_M3 = [
    "<AUTH> ospf4: Authentication failed for nbr 10.0.0.1 on ve1 - wrong password",
    "<AUTH> ospf5: Authentication failed for nbr 10.0.0.1 on ve1 - wrong authentication code",
    "<AUTH> ospf6: Authentication failed for nbr 10.0.0.1 on ve1 - wrong authentication code",
]

LOGS_M6 = [
    "<AUTH> ospf4: Authentication failed for nbr 10.0.0.8 on ve2 - wrong authentication code",
    "<AUTH> ospf5: Authentication failed for nbr 10.0.0.8 on ve2 - wrong authentication code",
    "<AUTH> ospf6: Authentication failed for nbr 10.0.0.8 on ve2 - wrong authentication code",
]

LOGS_M8 = [
    "<AUTH> ospf4: Authentication failed for nbr 10.0.0.6 on ve1 - wrong authentication code",
    "<AUTH> ospf5: Authentication failed for nbr 10.0.0.6 on ve1 - wrong authentication code",
    "<AUTH> ospf6: Authentication failed for nbr 10.0.0.6 on ve1 - wrong authentication code"
]

@pytest.mark.parametrize(
    "exp_devs, exp_messages",
    [
        pytest.param("m1", LOGS_M1),
        pytest.param("m2", LOGS_M2),
        pytest.param("m3", LOGS_M3),
        pytest.param("m4", []),
        pytest.param("m5", []),
        pytest.param("m6", LOGS_M6),
        pytest.param("m7", []),
        pytest.param("m8", LOGS_M8)
    ],
)
def test_logging(exp_devs: str, exp_messages: list):
    """Check the log files. There should only DBG, INFO and TRACE messages"""
    tk.test_logs(exp_devs, LOG_OPT, exp_messages)

