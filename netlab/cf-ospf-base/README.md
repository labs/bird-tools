##### Introduction
This case tests the basic topology with **ospf** protocols. There are no
additional options. Also there are three different OSPF protocols. Protocols
`ospf4` is OSPFv2, `ospf5` is OSPFv3-IPv4 and protocol `ospf6` is OSPFv3-IPv6.

There are four central devices on broadcast network. Each device has one
neighbor outside the central network (connected with point-to-point networks).

The links between devices `m1-m4` are type point-to-point, whereas links
between devices `m5-m8` are type broadcast.

<br>

##### Topology

---

```
       ┌─────┐
  m1---|m3 m5|---m7
  |    | \ / |    |
  |    |  O  |    |
  |    | / \ |    |
  m2---|m4 m6|---m8
       └─────┘
```

<br>

##### Test suite

---

In this test case we are saving:
1. ospf neighbors (`neighbors<4|6>_m<device_number>`),
2. bird tables (`master<4|6>_m<device_numebr>`),
3. krt tables (`krt<4|6>_m<device_number>`).
