<!--cf-ospf-bfd-->
##### Introduction
This case tests the **bfd** option within the **ospf** protocols. Unlike
the ospf **base** case, in this case protocol bfd is added. Also all the
ospf protocols are using option `bfd` with the value `yes`. If the value is set
to `yes` Bird setups bfd session for each OSPF neighbor.

There are four central devices on broadcast network. Each device has one
neighbor outside the central network (connected with point-to-point networks).

The **ospf-bfd** test case has also three different OSPF protocols. Protocols
`ospf4` is OSPFv2, `ospf5` is OSPFv3-IPv4 and protocol `ospf6` is OSPFv3-IPv6.

<br>

##### Topology

---

```
       ┌─────┐
  m1---|m3 m5|---m7
  |    | \ / |    |
  |    |  O  |    |
  |    | / \ |    |
  m2---|m4 m6|---m8
       └─────┘
```

<br>

##### Test suite

---

In this test case we are saving:
1. krt tables (`krt<4|6>_m<device_number>`),
2. bird tables (`master<4|6>_m<device_numebr>`),
