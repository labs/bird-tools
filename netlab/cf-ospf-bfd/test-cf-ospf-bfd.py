import pytest

import tests.kernel as tk
import tests.config as cf


LIMIT = 60
EXPECTED_DEVICES = ("m1", "m2", "m3", "m4", "m5", "m6", "m7", "m8")
LOG_OPT = ["<WARN> ospf.: Cannot find next hop for LSA"]


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait():
    """Wait until the time (limit) runs out"""
    tk.wait(LIMIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_neighbors_ospf4(exp_devs: str):
    """IPv4: get the list of BIRD neighbors and check it"""
    tk.test_ospf_neighbors(key="neighbors4", dev=exp_devs, opts="ospf4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_neighbors_ospf5(exp_devs: str):
    """IPv4: get the list of BIRD neighbors and check it"""
    tk.test_ospf_neighbors(key="neighbors5", dev=exp_devs, opts="ospf5")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_neighbors_ospf6(exp_devs: str):
    """IPv6: get the list of BIRD neighbors and check it"""
    tk.test_ospf_neighbors(key="neighbors6", dev=exp_devs, opts="ospf6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4", exp_devs, "master4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4_ospf3(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master5", exp_devs, "master5")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv6(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6", exp_devs, "master6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv4(exp_devs: str):
    """IPv4: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt4", exp_devs, "inet")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv4_ospf3(exp_devs: str):
    """IPv4: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt5", exp_devs, "inet", "100")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv6(exp_devs: str):
    """IPv6: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt6", exp_devs, "inet6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bfd_sessions(exp_devs: str):
    """Get the list of BFD sessions and check them"""
    tk.test_bfd_sessions(key="bfd", dev=exp_devs, opts="")


@pytest.mark.skipif(cf.save == True, reason="mode: save")
@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_ospf_reconfigure(exp_devs: str):
    """Reconfigure some devices with config file 'bird.conf'"""
    tk.run_configure(exp_devs, "bird.conf")


@pytest.mark.skipif(cf.save == True, reason="mode: save")
@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4_reconf(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4", exp_devs, "master4")


@pytest.mark.skipif(cf.save == True, reason="mode: save")
@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4_ospf3_reconf(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master5", exp_devs, "master5")


@pytest.mark.skipif(cf.save == True, reason="mode: save")
@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv6_reconf(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6", exp_devs, "master6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_logging(exp_devs: str):
    """Check the log files. There should only DBG, INFO and TRACE messages"""
    tk.test_logs(exp_devs, LOG_OPT, [])
