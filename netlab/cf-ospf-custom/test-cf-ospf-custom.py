import pytest
from typing import List

import tests.kernel as tk
import tests.config as cf


LIMIT = 60
EXPECTED_DEVICES = ("m1", "m2", "m3", "m4", "m5", "m6", "m7", "m8")

LOG_OPT = ["<WARN> ospf.: Cannot find next hop for LSA"]
LOG_M5 = ["<RMT> ospf6: Bad packet from .* via ve1 - wrong TTL \\(1\\)"]
LOG_M8 = [
    "<RMT> ospf4: Bad packet from .* via ve1 - wrong TTL \\(1\\)",
    "<RMT> ospf5: Bad packet from .* via ve1 - wrong TTL \\(1\\)",
    "<RMT> ospf6: Bad packet from .* via ve1 - wrong TTL \\(1\\)",
]


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait():
    """Wait until the time (limit) runs out"""
    tk.wait(LIMIT)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_neighbors_ospf4(exp_devs: str):
    """IPv4: get the list of BIRD neighbors and check it"""
    tk.test_ospf_neighbors(key="neighbors4", dev=exp_devs, opts="ospf4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_neighbors_ospf5(exp_devs: str):
    """IPv4: get the list of BIRD neighbors and check it"""
    tk.test_ospf_neighbors(key="neighbors5", dev=exp_devs, opts="ospf5")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_neighbors_ospf6(exp_devs: str):
    """IPv6: get the list of BIRD neighbors and check it"""
    tk.test_ospf_neighbors(key="neighbors6", dev=exp_devs, opts="ospf6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4", exp_devs, "master4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4_ospf3(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master5", exp_devs, "master5")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv6(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6", exp_devs, "master6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv4(exp_devs: str):
    """IPv4: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt4", exp_devs, "inet")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv4_ospf3(exp_devs: str):
    """IPv4: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt5", exp_devs, "inet", "100")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv6(exp_devs: str):
    """IPv6: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt6", exp_devs, "inet6")


@pytest.mark.parametrize(
    "exp_devs, exp_messages",
    [
        pytest.param("m1", []),
        pytest.param("m2", []),
        pytest.param("m3", []),
        pytest.param("m4", []),
        pytest.param("m5", LOG_M5),
        pytest.param("m6", []),
        pytest.param("m7", []),
        pytest.param("m8", LOG_M8),
    ],
)
def test_logging(exp_devs: str, exp_messages: List[str]):
    """Check the log files. There should only DBG, INFO and TRACE messages"""
    tk.test_logs(exp_devs, LOG_OPT, exp_messages)
