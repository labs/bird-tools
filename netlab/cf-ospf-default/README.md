<!--cf-ospf-default-->
##### Introduction
This case tests the topology, where the part of the device has
default values, part has not. Unlike the ospf **ospf-base** case, in this case
interfaces does or does not have explicitly set values for the options `type`
and `hello`.

Furthermore, this test case has three successive stages:
1. testing the original settings,
2. reconfiguration with another config files (suffix `_v2`) and testing them,
3. bird command `configure undo` + testing the original settings.

There are four central devices on broadcast network. Each device has one
neighbor outside the central network (connected with point-to-point networks).

The **ospf-default** test case has three different OSPF protocols. Protocols
`ospf4` is OSPFv2, `ospf5` is OSPFv3-IPv4 and protocol `ospf6` is OSPFv3-IPv6.

<br>

##### Topology

---

Original settings (IPv4 + IPv6):
- device **m1** does not have explicitly set `type` and `hello`,
- device **m4** does not have explicitly set `type` and `hello`,
- device **m6** does not have explicitly set `type` and `hello`,
- device **m7** does not have explicitly set `type` and `hello`,
- device **m2** does have explicitly set `type = bcast` and `hello = 10`,
- device **m3** does have explicitly set `type = bcast` and `hello = 10`,
- device **m5** does have explicitly set `type = bcast` and `hello = 10`,
- device **m8** does have explicitly set `type = bcast` and `hello = 10`.
```
       ┌─────┐
  m1---|m3 m5|---m7
  |    | \ / |    |
  |    |  O  |    |
  |    | / \ |    |
  m2---|m4 m6|---m8
       └─────┘
```

After reconfiguration (IPv4 + IPv6):
- device **m1** does have explicitly set `type = bcast` and `hello = 10`,
- device **m4** does have explicitly set `type = bcast` and `hello = 10`,
- device **m6** does not have explicitly set `type` and `hello`,
- device **m7** does not have explicitly set `type` and `hello`,
- device **m2** does have explicitly set `type = bcast` and `hello = 10`,
- device **m3** does have explicitly set `type = bcast` and `hello = 10`,
- device **m5** does not have explicitly set `type` and `hello`,
- device **m8** does not have explicitly set `type` and `hello`.

<br>

##### Test suite

---

In this test case we are saving:
1. ospf neighbors (`neighbors<4|6>_m<device_number>`)
1. krt tables (`krt<4|6>_m<device_number>`),
2. bird tables (`master<4|6>_m<device_number>`).
