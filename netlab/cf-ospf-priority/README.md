<!--cf-ospf-priority-->
##### Introduction
This case tests the **ospf** protocols with explicitly set values of
`priority` option. Unlike the ospf **ospf-base** case, in this case the devices
on the central network (`m3, m4, m5, m6`) do have explicitly specified priority
values.

Furthermore, this test case has three successive stages:
1. testing the original settings,
2. reconfiguration with another config files (suffix `_v2`) and testing them,
3. bird command `configure undo` + testing the original settings.

There are four central devices on broadcast network. Each device has one
neighbor outside the central network (connected with point-to-point networks).

The **ospf-priority** test case has three different OSPF protocols. Protocols
`ospf4` is OSPFv2, `ospf5` is OSPFv3-IPv4 and protocol `ospf6` is OSPFv3-IPv6.

<br>

##### Topology

---

Original settings (IPv4 + IPv6) and priority values:
- device `m3` priority is 10,
- device `m4` priority is 0,
- device `m5` priority is 1,
- device `m6` priority is 0.

```
       ┌─────┐
  m1---|m3 m5|---m7
  |    | \ / |    |
  |    |  O  |    |
  |    | / \ |    |
  m2---|m4 m6|---m8
       └─────┘
```
After reconfiguration (IPv4 + IPv6) and priority values:
- device `m3` priority is 0,
- device `m4` priority is 10,
- device `m5` priority is 0,
- device `m6` priority is 1.

<br>

##### Test suite

---

In this test case we are saving:
1. original ospf neighbors (`neighbors<4|6>_m<device_number>`),
2. original bird tables (`master<4|6>_m<device_numebr>`),
3. original krt tables (`krt<4|6>_m<device_number>`),
4. after reconf. ospf neighbors (`neighbors<4|6>_v2_m{device_number}`),
5. after reconf. bird tables (`neighbors<4|6>_v2_m{device_number}`),
6. reconf. undo ospf neighbors (`neighbors<4|6>_v3_m{device_number}`),
7. reconf. undo bird tables (`master<4|6>_v3_m{device_number}`),
