<!--cf-ospf-vrf-->
##### Introduction
This case tests the topology with three different **vrf** cycles with single
common device. There are three different cycles(`vrf1, vrf2, vrf3`) with single
common device (`m1`). The links between all the devices are type
point-to-point (ptp).

List of devices in specific cycles(vrfs):
- `vrf1` - m1, m2, m3, m4,
- `vrf2` - m1, m5, m6, m7,
- `vrf3` - m1, m8, m9, m10.

There are total six protocol in the *vrf* test case. Only the device `m1` has
all of the protocols (because it is located in the center). Other devices have
only one IPv4 protocol and one IPv6 protocol with the index of their vrf.

List of OSPF protocols in `m1`:
`ospf1v4` is OSPFv2-IPv4,
`ospf2v4` is OSPFv2-IPv4,
`ospf3v4` is OSPFv2-IPv6,
`ospf1v6` is OSPFv3-IPv6,
`ospf2v6` is OSPFv3-IPv6,
`ospf3v6` is OSPFv3-IPv6.

List of OSPF protocols in the other devices:
`ospf<1|2|3>v4` is OSPFv2-IPv4,
`ospf<1|2|3>v6` is OSPFv2-IPv6.

<br>

##### Topology

---

```
       ┌────┐            ┌────┐
  ┌────| m8 |────┐  ┌────| m7 |────┐
┌────┐ └────┘    |  |    └────┘  ┌────┐
| m9 |  vrf3   ┌──────┐   vrf2   | m6 |
└────┘ ┌────┐  |      |  ┌────┐  └────┘
  └────|m10 |──|  m1  |──| m5 |────┘
       └────┘ ┌|      |┐ └────┘
              |└──────┘|
              |  vrf1  |
            ┌────┐  ┌────┐
            | m2 |  | m4 |
            └────┘  └────┘
              | ┌────┐ |
              └─| m3 |─┘
                └────┘
```

<br>

##### Test suite

---

In this test case we are saving:
1. bird tables (`master<4|6>_m<device_numebr>`),
2. krt tables (`krt<4|6>_m<device_number>`),
3. krt tables (`krt<4|6>vrf2-m<device_number>`),
4. krt tables (`krt<4|6>vrf3-m<device_number>`).
