import pytest

import tests.kernel as tk
import tests.config as cf

LIMIT = 60
EXPECTED_DEVICES = ("m1", "m2", "m3", "m4", "m5", "m6", "m7", "m8", "m9", "m10")
LOG_OPT = ["<WARN> ospf.: Cannot find next hop for LSA"]


@pytest.mark.skipif(cf.save == False, reason="mode: save")
def test_wait():
    """Wait until the time (limit) runs out"""
    tk.wait(LIMIT)


def test_neighbors_ospf1v4():
    """IPv4: get the list of BIRD neighbors and check it"""
    tk.test_ospf_neighbors(key="neighbors1v4", dev="m1", opts="ospf1v4")


def test_neighbors_ospf2v4():
    """IPv4: get the list of BIRD neighbors and check it"""
    tk.test_ospf_neighbors(key="neighbors2v4", dev="m1", opts="ospf2v4")


def test_neighbors_ospf3v4():
    """IPv4: get the list of BIRD neighbors and check it"""
    tk.test_ospf_neighbors(key="neighbors3v4", dev="m1", opts="ospf3v4")


def test_neighbors_ospf1v6():
    """IPv6: get the list of BIRD neighbors and check it"""
    tk.test_ospf_neighbors(key="neighbors1v6", dev="m1", opts="ospf1v6")


def test_neighbors_ospf2v6():
    """IPv6: get the list of BIRD neighbors and check it"""
    tk.test_ospf_neighbors(key="neighbors2v6", dev="m1", opts="ospf2v6")


def test_neighbors_ospf3v6():
    """IPv6: get the list of BIRD neighbors and check it"""
    tk.test_ospf_neighbors(key="neighbors3v6", dev="m1", opts="ospf3v6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES[1:])
def test_neighbors_ospf4(exp_devs: str):
    """IPv4: get the list of BIRD neighbors and check it"""
    tk.test_ospf_neighbors(key="neighbors4", dev=exp_devs, opts="ospf4")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES[1:])
def test_neighbors_ospf6(exp_devs: str):
    """IPv6: get the list of BIRD neighbors and check it"""
    tk.test_ospf_neighbors(key="neighbors6", dev=exp_devs, opts="ospf6")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv4(exp_devs: str):
    """IPv4: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master4", exp_devs, "master4")


@pytest.mark.parametrize("tab", ("t100v4", "t200v4", "t300v4"))
def test_bird_routes_ipv4_vrfs(tab: str):
    tk.test_bird_routes(tab, "m1", tab)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_bird_routes_ipv6(exp_devs: str):
    """IPv6: get the content of BIRD tables and check it"""
    tk.test_bird_routes("master6", exp_devs, "master6")


@pytest.mark.parametrize("tab", ("t100v6", "t200v6", "t300v6"))
def test_bird_routes_ipv6_vrfs(tab: str):
    tk.test_bird_routes(tab, "m1", tab)


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv4(exp_devs: str):
    """IPv4: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt4", exp_devs, "inet")


def test_krt_routes_ipv4_vrf2():
    tk.test_krt_routes("krt4vrf2", "m1", "inet", "200")


def test_krt_routes_ipv4_vrf3():
    tk.test_krt_routes("krt4vrf3", "m1", "inet", "300")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_krt_routes_ipv6(exp_devs: str):
    """IPv6: get the content of KERNEL tables and check it"""
    tk.test_krt_routes("krt6", exp_devs, "inet6")


def test_krt_routes_ipv6_vrf2():
    tk.test_krt_routes("krt6vrf2", "m1", "inet6", "200")


def test_krt_routes_ipv6_vrf3():
    tk.test_krt_routes("krt6vrf3", "m1", "inet6", "300")


@pytest.mark.parametrize("exp_devs", EXPECTED_DEVICES)
def test_logging(exp_devs: str):
    """Check the log files. There should only DBG, INFO and TRACE messages"""
    tk.test_logs(exp_devs, LOG_OPT, [])
