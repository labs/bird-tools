<!--cf-rip-base-->
##### Introduction
This case tests the basic topology with **rip** protocols. There are no
additional options. There are protocol **direct**(neccessary for announcing)
and two protocols rip(both IPv4 and IPv6).

There are four central devices. Each device has one neighbor outside
the central network.

<br>

##### Topology

---

```
       ┌─────┐
  m1---|m3 m5|---m7
  |    | \ / |    |
  |    |  O  |    |
  |    | / \ |    |
  m2---|m4 m6|---m8
       └─────┘
```

<br>

##### Test suite

---

In this test case we are saving:
1. krt tables (`krt<4|6>_m<device_number>`),
2. bird tables (`master<4|6>_m<device_numebr>`).
