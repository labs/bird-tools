#!/usr/bin/perl

use common::sense;

sub main {
  #open F, "|-", "grep -n '.*' m*/bird.log | sed 's/ \[/: [/' | sort -k3,3 -k4,4 -k5,5 -k2,2n -t:" or die $!;
  open F, "|-", "cat zz" or die $!;

  while (<F>) {
    m#m(?<mach>\d+)/bird.log:(?<lineno>\d+):(?<datetime>\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}\.\d{3}) \[(?<thread>[0-9a-f]{4})\] (?<msg>.*)$# or die "bad line: $_";
    Machine->get($+{mach})->logged(%+);
  }
}

package Machine;
use Moose;
has 'name' => ( is => 'ro', isa => 'Str' );
has 'id' => ( is => 'rw', isa => 'Str', default => "" );
has 'addresses' => ( is => 'rw', isa => 'HashRef[Str]', default => sub { {} } );
has 'protocols' => ( is => 'rw', isa => 'HashRef[Protocol]', default => sub { {} } );

our %machines;
our %machines_by_addr;

sub get {
  my ($class, $name) = @_;
  return $machines{$name} if exists $machines{$name};
  return $machines{$name} = Machine->new(name => $name);
}

sub logged {
  my ($self, %args) = @_;

  if ($args{$msg} =~ /<TRACE> (?<proto>[^.]*): Initializing$/) {
    die "Duplicate protocol $+{proto}" if exists $self->protocols->{$+{proto}};
    $self->protocols->{$+{proto}} = Protocol->new(name => $+{proto}, machine => $self);
    return;
  }

  if ($args{$msg} =~ /<TRACE> (?<proto>[^.]*): (?<msg>.*)$/) {
    return $self->protocols->{$+{proto}}->logged($+{msg}) if exists $self->protocols->{$+{proto}};
    $self->tables->{$+{proto}} = Table->new(name => $+{proto}, machine => $self) unless exists $self->tables->{$+{proto}};
    return $self->tables->{$+{proto}}->logged($+{msg});
  }

  if ($args{$msg} =~ /<TRACE> (?<proto>[^.]*)\.(?<channel>[^.]*): (?<msg>.*)$/) {
    die "Unknown protocol $+{proto}" unless exists $self->protocols->{$+{proto}};
    return $self->protocols->{$+{proto}}->channel_logged($+{channel}, $+{msg});
  }

  if ($args{$msg} =~ /<TRACE> (?<proto>[^.]*)\.(?<channel>[^.]*)\.(?<imex>import|export): (?<msg>.*)$/) {
    die "Unknown protocol $+{proto}" unless exists $self->protocols->{$+{proto}};
    return $self->protocols->{$+{proto}}->channel_table_logged($+{channel}, $+{imex}, $+{msg});
  }
}

package Protocol;
use Moose;
has 'name' => (is => 'ro', isa => 'Str' );
has 'machine' => (is => 'ro', isa => 'Machine' );


package Route;
use Moose;
has 'dest' => ( is => 'ro', isa => 'Str' );
has 'src' => ( is => 'ro', isa => 'RteSrc' );
has 'events' => ( is => 'rw', isa => 'ArrayRef[Event]' );

package Event;

use Moose;

package main;

my %routes;

my @machines = map { Machine->new(fn => $_) } (glob());
say $_->name foreach (@machines);
say scalar @{$_->log} foreach (@machines);
