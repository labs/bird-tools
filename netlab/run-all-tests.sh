#!/bin/bash

set -e

VERSION=v3

if [ -d "$1" ]; then
  ALL_TESTS="$@"
else
  PIPELINE="cat"
  while [ -n "$1" ]; do
    if [ "${1:0:1}" = "-" ]; then
      PIPELINE="$PIPELINE | egrep -v '${1:1}'"
    else
      PIPELINE="$PIPELINE | egrep '$1'"
    fi
    shift
  done
  ALL_TESTS=$(find -name data-$VERSION | sed 's$^./$$;s^/data-'$VERSION'$^^;' | eval $PIPELINE)
fi

RED='\033[0;31m'
GREEN='\033[0;32m'
NOCOLOR='\033[0m'

./stop -f

for f in $ALL_TESTS; do
  rm -f bird-m*

  NOW=$(date)
  TS=$(date +%s)
  echo -n "$NOW: $f ... "
  set +e
  python3 -B -u ./runtest -s $VERSION $f > runtest.log 2>&1
  if [ $? = 0 ]; then
    DIR=result-ok-$f-$TS
    echo -e "$GREEN[  OK  ]$NOCOLOR"
  else
    DIR=result-bad-$f-$TS
    echo -en "$RED[ STOP ]"
    ./stop -f >> runtest.log 2>&1
    echo -e "\033[8D[ FAIL ]$NOCOLOR"
  fi
  set -e

  mkdir $DIR
  pushd $DIR >/dev/null 2>&1
  mv ../runtest.log ./
  mv ../bird-m* ./
  if [ -d ../temp ]; then mv ../temp ./; fi
  ln -s $(readlink -f ../$f) config
  ln -s config/data-$VERSION data
  ln $(readlink -f ../common/bird) bird
  popd >/dev/null 2>&1
  chown -R $SUDO_UID:$SUDO_GID $DIR
done

NOW=$(date)
echo "$NOW: [ DONE ]"
