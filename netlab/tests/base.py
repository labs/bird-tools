import os
import pytest

import tests.config as cf


def init(testdir, dataset, save):
    cf.testdir = testdir
    cf.save = save
    cf.dataset = dataset

    result = os.system(f"./start -c {testdir}")
    if result != 0:
        return False

    init_dirs()
    init_nodes()
    return True


def run():
    modename = "save" if cf.save else "check"
    print("Running", cf.testdir, "in", modename, "mode")
    return pytest.main(["-x", "-v", f"{cf.testdir}/test-{cf.testdir}.py"]) == 0


def cleanup():
    clean_dirs()
    return os.system(f"./stop") == 0


def init_dir(dir):
    if os.path.isdir(dir):
        for _ in os.listdir(dir):
            os.remove(f"{dir}/{_}")
    else:
        os.mkdir(dir)


def init_dirs():
    cf.tempdir = "temp"
    cf.datadir = f"{cf.testdir}/data-{cf.dataset}"

    init_dir(cf.tempdir)
    if cf.save:
        init_dir(cf.datadir)


def clean_dirs():
    for _ in os.listdir(cf.tempdir):
        os.remove(f"{cf.tempdir}/{_}")

    try:
        os.rmdir(cf.tempdir)
    except:
        print("Error in removing temp directory")


def init_nodes():
    cf.nodes = list(filter(lambda n: n[0] == "m", os.listdir(".")))
