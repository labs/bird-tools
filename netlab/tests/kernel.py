import asyncio
import os
import time
import subprocess

import tests.config as cf

def run_on_machine(dev: str, cmd: str) -> None:
    ws = os.system(f"ip netns exec {dev} {cmd}")
    if os.WIFEXITED(ws):
        ec = os.WEXITSTATUS(ws)
    else:
        ec = False

    if ec != 0:
        raise Exception(f"Command {cmd} on device {dev} exited with exit code {ws}")


def test_krt_routes(key: str, dev: str, family: str, table: str = "main") -> None:
    if cf.save:
        save_krt_routes(key, dev, family, table)
    else:
        check_krt_routes_timeout(key, dev, family, table)


def save_krt_routes(key: str, dev: str, family: str, table: str, loc: str = cf.datadir) -> None:
    run_on_machine(dev, f"./tests/get_stdout_krt '{family}' 'table {table}' > {loc}/{key}-{dev}")


def check_krt_routes_timeout(key: str, dev: str, family: str, table: str) -> None:
    timeout = 60
    for sec in range(timeout):
        if check_krt_routes(key, dev, family, table):
            return
        else:
            time.sleep(1)
    assert 0


def check_krt_routes(key: str, dev: str, family: str, table: str) -> None:
    save_krt_routes(key, dev, family, table, cf.tempdir)
    current_table = read_file(f"{cf.tempdir}/{key}-{dev}")
    saved_table = read_file(f"{cf.datadir}/{key}-{dev}")
    return saved_table == current_table


# Task test_krt_routes_mpls() requires MPLS maps produced by make_mpls_map()
def test_krt_routes_mpls(key: str, dev: str, family: str, table: str = "main") -> None:
    if cf.save:
        save_krt_routes_mpls(key, dev, family, table)
    else:
        check_krt_routes_mpls_timeout(key, dev, family, table)


def save_krt_routes_mpls(key: str, dev: str, family: str, table: str, loc: str = cf.datadir) -> None:
    run_on_machine(dev, f"./tests/get_stdout_krt_mpls '{family}' 'table {table}' > {loc}/{key}-{dev}")


def check_krt_routes_mpls_timeout(key: str, dev: str, family: str, table: str) -> None:
    timeout = 60
    for sec in range(timeout):
        if check_krt_routes_mpls(key, dev, family, table):
            return
        else:
            time.sleep(1)
    assert 0


def check_krt_routes_mpls(key: str, dev: str, family: str, table: str) -> None:
    save_krt_routes_mpls(key, dev, family, table, cf.tempdir)
    current_table = read_file(f"{cf.tempdir}/{key}-{dev}")
    saved_table = read_file(f"{cf.datadir}/{key}-{dev}")
    return saved_table == current_table


def test_bird_routes(key: str, dev: str, table: str, opts: str = "all", prefix: str = "") -> None:
    if cf.save:
        save_bird_routes(key, dev, table, opts, prefix)
    else:
        check_bird_routes_timeout(key, dev, table, opts, prefix)


def save_bird_routes(key: str, dev: str, table: str, opts: str, prefix: str, loc: str = cf.datadir, order: str = "") -> None:
    os.system(f""" \
        ./tests/get_stdout_bird '{dev}' '{prefix} table {table}' '{opts}' \
        > {loc}/{key}-{dev}{order} \
        """)


def check_bird_routes_timeout(key: str, dev: str, table: str, opts: str, prefix: str) -> None:
    timeout = 80
    for i in range(timeout):
        if check_bird_routes(key, dev, table, opts, prefix, order=f"-{i}"):
            return
        else:
            time.sleep(1)
    assert 0


def check_bird_routes(key: str, dev: str, table: str, opts: str, prefix: str, order: str) -> None:
    save_bird_routes(key, dev, table, opts, prefix, cf.tempdir, order)
    current_table = read_file(f"{cf.tempdir}/{key}-{dev}{order}")
    saved_table = read_file(f"{cf.datadir}/{key}-{dev}")
    return saved_table == current_table


def wait(sec: int):
    time.sleep(sec)


# Task make_mpls_map() requires that BIRD routing tables already converged
def make_mpls_map(key: str, dev: str, tabs: list, opts: str = "") -> None:
    tables = ""
    for tab in tabs:
        tables += f" table {tab}"
    else:
        tables = "table all"

    os.system(f""" \
        ./tests/get_mpls_map '{dev}' '{tables}' '{opts}' \
        > {cf.tempdir}/{key}-{dev} \
        """)


# Task test_bird_routes_mpls() requires MPLS maps produced by make_mpls_map()
def test_bird_routes_mpls(key: str, dev: str, table: str, opts: str = "all") -> None:
    if cf.save:
        save_bird_routes_mpls(key, dev, table, opts)
    else:
        check_bird_routes_mpls_timeout(key, dev, table, opts)


def save_bird_routes_mpls(key: str, dev: str, table: str, opts: str, loc: str = cf.datadir, order: str = "") -> None:
    os.system(f""" \
        ./tests/get_stdout_bird_mpls '{dev}' 'table {table}' '{opts}' \
        > {loc}/{key}-{dev}{order} \
        """)


def check_bird_routes_mpls_timeout(key: str, dev: str, table: str, opts: str) -> None:
    timeout = 80
    for i in range(timeout):
        if check_bird_routes_mpls(key, dev, table, opts, order=f"-{i}"):
            return
        else:
            time.sleep(1)
    assert 0


def check_bird_routes_mpls(key: str, dev: str, table: str, opts: str, order: str) -> None:
    save_bird_routes_mpls(key, dev, table, opts, cf.tempdir, order)
    current_table = read_file(f"{cf.tempdir}/{key}-{dev}{order}")
    saved_table = read_file(f"{cf.datadir}/{key}-{dev}")
    return saved_table == current_table


def write_krt_routes(name: str, content: str) -> None:
    with open(name, "w") as txt:
        txt.write(content)


def read_file(name: str) -> list:
    with open(name, "r") as txt:
        return txt.read().split("\n")


def test_logs(dev: str, log_optional: list, log_mandatory: list,
              filename: str = "bird.log") -> None:
    pattern = "DBG|TRACE|INFO"
    logfile = f"{dev}/{filename}"

    # Add default optional log messages
    log_optional += [
        "<WARN> KRT: Netlink strict checking failed, will scan all tables at once",
        "<RMT> .*: Received: Connection collision resolution",
        "<RMT> .*: Received: Cease",
        "<RMT> ospf.*: Bad DBDES packet from .*",
        "<WARN> BGP: Unexpected connect from unknown address",
    ]

    if log_optional:
        pattern = f"{pattern}|{'|'.join(log_optional)}"

    if log_mandatory:
        pattern = f"{pattern}|{'|'.join(log_mandatory)}"
        check_expected_logs_timeout(log_mandatory, logfile)

    if not os.system(f"egrep -v '{pattern}' {logfile}"):
        assert False, "Log file contains incorrect message"


def check_expected_logs_timeout(log_messages: list, logfile: str) -> None:
    for _ in range(61):
        if check_expected_logs(log_messages, logfile):
            return
        else:
            time.sleep(1)
    assert False, "Log file contains incorrect message"


def check_expected_logs(log_messages: list, logfile: str) -> bool:
    """
    TRUE -> if there IS NOT variable `message` in variable `logfile`
    FALSE -> if there IS variable `message` in variable `logfile`
    """
    for message in log_messages:
        if os.system(f"egrep '{message}' {logfile}"):
            return False
    return True


def test_bfd_sessions(**kwargs) -> None:
    if cf.save:
        save_bfd_sessions(kwargs["key"], kwargs["dev"], kwargs["opts"])
    else:
        check_bfd_sessions(kwargs["key"], kwargs["dev"], kwargs["opts"])


def save_bfd_sessions(key: str, dev: str, opts: str,
                      loc: str = cf.datadir) -> None:
    os.system(f"./tests/get_stdout_bfd '{dev}' '{opts}' > {loc}/{key}-{dev}")


def timeout_decorator(func):
    def inner(key, dev, opts):
        for _ in range(150):
            if func(key, dev, opts):
                return
            time.sleep(1)
        assert 0
    return inner


@timeout_decorator
def check_bfd_sessions(key, dev, opts) -> bool:
    save_bfd_sessions(key, dev, opts, cf.tempdir)
    current_table = read_file(f"{cf.tempdir}/{key}-{dev}")
    saved_table = read_file(f"{cf.datadir}/{key}-{dev}")
    return current_table == saved_table


def test_ospf_neighbors(**kwargs) -> None:
    if cf.save:
        save_ospf_neighbors(kwargs["key"], kwargs["dev"], kwargs["opts"])
    else:
        check_ospf_neighbors(kwargs["key"], kwargs["dev"], kwargs["opts"])


def save_ospf_neighbors(key: str, dev: str, opts: str,
                        loc: str = cf.datadir) -> None:
    os.system(f"""\
              ./tests/get_stdout_ospf_neighbors "{dev}" "{opts}" > \
              {loc}/{key}-{dev}""")


@timeout_decorator
def check_ospf_neighbors(key: str, dev: str, opts: str) -> bool:
    save_ospf_neighbors(key, dev, opts, cf.tempdir)
    current_table = read_file(f"{cf.tempdir}/{key}-{dev}")
    saved_table = read_file(f"{cf.datadir}/{key}-{dev}")
    return current_table == saved_table


def test_babel_neighbors(**kwargs) -> None:
    if cf.save:
        save_babel_neighbors(kwargs["key"], kwargs["dev"], kwargs["opts"])
    else:
        check_babel_neighbors(kwargs["key"], kwargs["dev"], kwargs["opts"])


def save_babel_neighbors(key: str, dev: str, opts: str,
                        loc: str = cf.datadir) -> None:
    os.system(f"""\
              ./tests/get_stdout_babel_neighbors "{dev}" "{opts}" > \
              {loc}/{key}-{dev}""")


@timeout_decorator
def check_babel_neighbors(key: str, dev: str, opts: str) -> bool:
    save_babel_neighbors(key, dev, opts, cf.tempdir)
    current_table = read_file(f"{cf.tempdir}/{key}-{dev}")
    saved_table = read_file(f"{cf.datadir}/{key}-{dev}")
    return current_table == saved_table


def test_protocols(**kwargs) -> None:
    if cf.save:
        save_protocols(kwargs["key"], kwargs["dev"], kwargs["opts"])
    else:
        check_protocols(kwargs["key"], kwargs["dev"], kwargs["opts"])


def save_protocols(key: str, dev: str, opts: str, loc: str = cf.datadir) -> None:
    os.system(f"""./tests/get_stdout_protocols "{dev}" "{opts}" > {loc}/{key}-{dev}""")


@timeout_decorator
def check_protocols(key: str, dev: str, opts: str) -> bool:
    save_protocols(key, dev, opts, cf.tempdir)
    current_protos = read_file(f"{cf.tempdir}/{key}-{dev}")
    saved_protos = read_file(f"{cf.datadir}/{key}-{dev}")
    return current_protos == saved_protos


def clean_log(dev: str) -> None:
    os.system(f"rm {dev}/bird.log")


def process_command(command: str) -> list:
    """Run parameter 'command' inside the Shell and capture the stdout."""
    stdout = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True
    ).communicate()[0]
    return stdout.decode("utf-8").splitlines()


async def async_process_command(*args) -> list:
    """Run parameter 'command' inside the Shell and capture the stdout."""
    process = await asyncio.create_subprocess_shell(*args, stdout=asyncio.subprocess.PIPE)
    stdout, _ = await process.communicate()
    return stdout.decode("utf-8").splitlines()


def disable_bird_protocol(dev: str, proto: str) -> None:
    os.system(f"cd {dev} && ./birdc -l disable {proto}")


def enable_bird_protocol(dev: str, proto: str) -> None:
    os.system(f"cd {dev} && ./birdc -l enable {proto}")


def reload_bird_protocol(dev: str, proto: str, what: str) -> None:
    command = f"cd {dev} && ./birdc -l reload {what} '{proto}'"
    captured_stdout = process_command(command)
    try:
        assert("reload" in captured_stdout[1])
    except Exception:
        if len(what) > 0:
            return reload_bird_protocol(dev, proto, "")

        raise Exception(f"Failed to reload {proto} on {dev}: got {captured_stdout}")


async def async_reload_bird_protocol(dev: str, proto: str, what: str) -> None:
    command = f"cd {dev} && ./birdc -l reload {what} '{proto}'"
    captured_stdout = await async_process_command(command)
    try:
        assert("reload" in captured_stdout[1])
    except Exception:
        if len(what) > 0:
            return await async_reload_bird_protocol(dev, proto, "")

        raise Exception(f"Failed to reload {proto} on {dev}: got {captured_stdout}")


def run_configure(dev: str, conf_file: str) -> None:
    """
    Run the specified Bird command and check the output.

    The output must contain:
    - name of the file ("bird_v2.conf"),
    - "Reconfigured" or "Reconfiguration in progress".
    """
    command = f"cd {dev} && ./birdc -l configure \'\"{conf_file}\"\'"
    captured_stdout = process_command(command)
    assert (
        conf_file in captured_stdout[1] \
        and ("Reconfigured" in captured_stdout[2] \
        or "Reconfiguration in progress" in captured_stdout[2])
    )


def run_configure_undo(dev: str) -> None:
    command = f"cd {dev} && ./birdc -l configure undo"
    captured_stdout = process_command(command)
    assert (
        "Undo requested" in captured_stdout[1] \
        and ("Reconfigured" in captured_stdout[2] \
        or "Reconfiguration in progress" in captured_stdout[2])
    )


def kill_process(dev: str, pidfile: str, sig: str) -> None:
    """Kill a process given by pidfile."""
    pid = read_file(f"{dev}/{pidfile}")[0]
    command = f"kill -{sig} {pid}"
    process_command(command)


def kill_node(dev: str, sig: str) -> None:
    """Kill a running BIRD."""
    kill_process(dev, "bird.pid", sig)
