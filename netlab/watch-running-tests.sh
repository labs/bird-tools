#!/bin/bash

vs() {
  for f in $(seq $(tput cols)); do
    echo -n "="
  done
  echo
}

if [ "$1" = "inner" ]; then
  LINES=$(tput lines)
  CORELINES=5
  LOGLINES=$(((LINES - 8 - CORELINES)/2))

  tail -n$LOGLINES runtest.log

  echo; vs; echo

  COREFILES=$(mktemp)
  find -name core > $COREFILES
  echo "Found $(wc -l $COREFILES | cut -f1 -d' ') core files …"
  head -n$CORELINES $COREFILES
  rm $COREFILES

  echo; vs; echo

  FIRSTLOGFILE=$(find m*/ -name 'bird.log' -o -name 'bird.clog' | head -n1)
  if [ -f "$FIRSTLOGFILE" ]; then
    echo "Log file $FIRSTLOGFILE:"
    tail -n$LOGLINES $FIRSTLOGFILE
  else
    echo "No log available"
  fi
else
  watch -n0.2 $0 inner
fi
