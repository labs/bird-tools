#!/bin/bash

set -e

CONF=/srv/rs-perf/feed.conf

RUNDIR=/run/rs-perf
mkdir -p "$RUNDIR"
chown root:bird "$RUNDIR"
chmod 2775 "$RUNDIR"

cd /srv/rs-perf
ulimit -c unlimited
ulimit -n 65536

LOG=/srv/rs-perf/feed.log
touch $LOG
chown root:bird $LOG
chmod 0664 $LOG

case $1 in
  start)
    ip r add local 2001:1488:ac15:ff90:131::/80 dev lo
    ip r add 2001:1488:ac15:ff90:134::/80 via 2001:1488:ac15:ff90::134

    /srv/rs-perf/bird3 -s $RUNDIR/feeder.ctl -u bird -g bird -P $RUNDIR/feeder.pid -c $CONF
    ;;

  stop)
    ip r del 2001:1488:ac15:ff90:134::/80 via 2001:1488:ac15:ff90::134
    ip r del local 2001:1488:ac15:ff90:131::/80 dev lo
    ;;

  *)
    echo "Bad command $1"
    exit 1
    ;;
esac
