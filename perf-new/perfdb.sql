CREATE TABLE requests (
  id INTEGER PRIMARY KEY,
  _type text,
  _size text,
  threads int,
  version char(40)
);

CREATE TABLE results (
  id INTEGER UNIQUE,
  cpu_ns int,
  memory int,
  time_ns int,
  prefixes int,
  routes int,
  FOREIGN KEY (id) REFERENCES requests (id)
);

CREATE TABLE probe_data (
  id INTEGER,
  time int,
  prefixes int,
  routes int,
  FOREIGN KEY (id) REFERENCES requests (id)
);

CREATE TABLE system_data (
  id INTEGER,
  time int,
  cpu_ns int,
  memory int,
  FOREIGN KEY (id) REFERENCES requests (id)
);
