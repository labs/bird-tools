#!/usr/bin/python3 -u

import ipaddress
import json
import random
import time
import sys

class Client:
    prefix_arm_client = int(ipaddress.IPv6Address("2001:1488:ac15:ff90:134:0:8000:0"))
    prefix_arm_test = int(ipaddress.IPv6Address("2001:1488:ac15:ff90:134:0:c000:0"))
    prefix_amd_client = int(ipaddress.IPv6Address("2001:1488:ac15:ff90:131:0:8000:0"))
    prefix_amd_test = int(ipaddress.IPv6Address("2001:1488:ac15:ff90:131:0:c000:0"))
    asn_prefix = 4210000000

    def __init__(self, suite, asn_offset, ident):
        self.asn = self.asn_prefix + asn_offset
        self.suite = suite
        self.ident = ident
        self.prefixes = set()
        self.dumped = set()
        self.tables = set()
        self.main_table = suite.table((self.asn, ))
        self.main_table.client = self
        if ident == "probe":
            self.main_table.index = "probe"

    def route(self, prefix):
        assert(not self.dumped)
        assert(prefix not in self.prefixes)
        path = (self.asn, ) + prefix.last

        if prefix.originator is None:
            prefix.originator = self
        else:
            t = self.suite.table(prefix.last)
            t.prefixes.remove(prefix)
            t.prefixes_inherited += 1

        t = self.suite.table(path)
        t.prefixes.add(prefix)
        prefix.last = path

        self.prefixes.add(prefix)
        self.tables.add(t)

    def ip_arm_client(self):
        return ipaddress.IPv6Address(self.prefix_arm_client + self.asn - self.asn_prefix)

    def ip_amd_client(self):
        return ipaddress.IPv6Address(self.prefix_amd_client + self.asn - self.asn_prefix)

    def ip_arm_test(self):
        return ipaddress.IPv6Address(self.prefix_arm_test + self.asn - self.asn_prefix)

    def ip_amd_test(self):
        return ipaddress.IPv6Address(self.prefix_amd_test + self.asn - self.asn_prefix)

    def dump_sources(self, f):
        for t in self.tables:
            if t is not self.main_table:
                f.write(f"""
###### Source of prefixes for client {self.ident} ({self.main_table.index})
# Path: {t.path}
# Prefixes: {len(t.prefixes) + t.prefixes_inherited}
protocol pipe source_{self.main_table.index}_{t.index} {"{"}
    import filter setfinal;
    export none;
    max generation 96;
    table table_{self.main_table.index};
    peer table table_{t.index};
{"}"}
""")

    def dump_bgp(self, f, target):
        variant = {
                "amd": {
                    "local_ip": self.ip_amd_client(),
                    "neighbor_ip": self.ip_arm_test(),
                    },
                "arm": {
                    "local_ip": self.ip_arm_client(),
                    "neighbor_ip": self.ip_amd_test(),
                    },
                }[target]

        total = sum([len(t.prefixes) + t.prefixes_inherited for t in self.tables])
        f.write(f"""
###### Feeder of client {self.ident}
# ASN: {self.asn}
# Prefixes: {total}
protocol bgp feeder_{self.asn} {"{"}
    description "Feeder {self.ident} of {total} routes";
    local {variant['local_ip']} as {self.asn};
    neighbor {variant['neighbor_ip']} as 4220000000;
    strict bind;
    free bind;
    multihop 3;
    passive;
    ipv6 {"{"}
        export { "none" if self.ident == "probe" else "all" };
        import { "all" if self.ident == "probe" else "none" };
        table table_{self.main_table.index};
        { "add paths;" if self.ident == "probe" else "" }
    {"}"};
{"}"}
""")

    def dump_test(self, t, f, mt, aux, st):
        ip = {
                "amd": {
                    "local": self.ip_amd_test(),
                    "neighbor": self.ip_arm_client(),
                    },
                "arm": {
                    "local": self.ip_arm_test(),
                    "neighbor": self.ip_amd_client(),
                    },
                }[t]

        stx = {
                "secondary": {
                    "bgp": "secondary;",
                    "tab": "sorted",
                    },
                "best": {
                    "bgp": "",
                    "tab": "",
                    },
                }[st]

        if mt == "multi":
            f.write(f"""
ipv6 table table{self.asn} {stx["tab"]};
protocol pipe {"{"}
    import where testImportPipe({self.asn});
    export where testExportPipe({self.asn});
    table master6;
    peer table table{self.asn};
{"}"}
""")

        mtx = {
                "multi": {
                    "importFilter": f"testImportProto({self.asn})",
                    "exportFilter": f"testExportProto({self.asn})",
                    "table": f"table{self.asn}",
                    },
                "single": {
                    "importFilter": f"testImportProto({self.asn}) && testImportPipe({self.asn})",
                    "exportFilter": f"testExportPipe({self.asn}) && testExportProto({self.asn})",
                    "table": "master6",
                    },
                }[mt]

        export_table = {
                "plain": "off",
                "aux": "on",
                }[aux]

        import_table = "on" if aux == "aux" or mt == "single" else "off"

        total = sum([len(t.prefixes) + t.prefixes_inherited for t in self.tables])
        f.write(f"""
###### Client {self.ident}
# ASN: {self.asn}
# Prefixes: {total}
protocol bgp bgp_{self.asn} {"{"}
    description "Client {self.ident} with {total} routes";
    local {ip["local"]} as 4220000000;
    neighbor {ip["neighbor"]} as {self.asn};
    strict bind;
    free bind;
    multihop 3;
    connect delay time 0;
    rs client;
    ipv6 {"{"}
        { stx["bgp"] }
        import where { mtx["importFilter"] };
        export where { mtx["exportFilter"] };
        import table { import_table };
        export table { export_table };
        table { mtx["table"] };
        { "add paths;" if self.ident == "probe" else "" }
    {"}"};
{"}"}
""")


class Prefix:
    def __init__(self):
        self.originator = None
        self.last = tuple()

class PrefixIPv6(Prefix):
    exp = 40
    baselen = 50
    common = 0xfc << 120
    def __init__(self):
        super().__init__()
        r = random.randrange(2**self.exp - 1) + 1
        l = self.baselen

        while r % 2 == 0:
            l -= 1
            r //= 2

        self.prefix = ipaddress.IPv6Address(self.common + ((r // 2) << (128 - l)))
        self.len = l

    def __eq__(self, other):
        return \
                type(self) == type(other) and \
                self.len == other.len and \
                self.prefix == other.prefix

    def __hash__(self):
        return int(self.prefix) + self.len

class Test:
    def __init__(self, level, clients):
        self.level = level
        self.clients = clients

    def total_routes(self):
        return sum([ len(c.prefixes) for c in self.clients ])

    def all_prefixes(self):
        return set().union(*[ c.prefixes for c in self.clients ])

    def total_prefixes(self):
        return len(self.all_prefixes())

    def dump(self, _dir):
        px = list(self.all_prefixes())

        self.client_count = len(self.clients)
        self.prefix_count = len(px)
        self.route_count = self.total_routes()

        numstem = f"{self.level}-{self.client_count}"
        print(f"Dumping test {numstem}")

        for kind in [{
            "t": t,
            "mt": mt,
            "aux": aux,
            "st": st,
            "stem": f"test-{t}-{mt}-{aux}-{st}-{numstem}",
            }
            for t in ("amd", "arm")
            for mt in ("single", "multi")
            for aux in ("plain", "aux")
            for st in ("best", "secondary")
            ]:

            self.dump_kind(_dir, **kind)

        self.dump_roa(_dir, "irrdb", px)
        self.dump_roa(_dir, "signed", random.sample(px, int(len(px) * 0.35)+1))

    def dump_kind(self, _dir, t, mt, aux, st, stem):
        with open(f"{_dir}/{stem}.conf", "w") as f:
            f.write(f'# Clients: {self.client_count}\n')
            f.write(f'# Prefixes: {self.prefix_count}\n')
            f.write(f'# Routes: {self.route_count}\n')

            if st == "secondary" and mt == "single":
                f.write(f"ipv6 table master6 sorted;\n")

            for c in self.clients:
                c.dump_test(t, f, mt, aux, st)

    def dump_roa(self, _dir, name, px):
        numstem = f"{self.level}-{len(self.clients)}"
        now = time.time()
        with open(f"{_dir}/roa-{self.level}-{len(self.clients)}-{name}.json", "w") as f:
            json.dump({
                "metadata": {
                    "counts": len(px),
                    "generated": now,
                    "valid": now + 86400 * 365 * 1000, # 100 years of validity
                    "signature": "",
                    "signatureDate": time.strftime("%Y-%m-%dT%H:%M:%SZ00:00", time.gmtime()),
                    },
                "roas": [
                    {
                        "prefix": f"{p.prefix}/{p.len}",
                        "maxLength": p.len,
                        "asn": f"AS{p.originator.asn}",
                        "ta": "test",
                        } for p in px ]
                    }, f)

class Table:
    def __init__(self, path, index, shorter):
        self.path = path
        self.index = index
        self.shorter = shorter
        self.prefixes = set()
        self.prefixes_inherited = 0
        self.client = None

    def _format_path_internal(self, stub, remaining):
        stub = f"prepend({stub},{remaining[-1]})"
        if len(remaining) == 1:
            return stub
        else:
            return self._format_path_internal(stub, remaining[:-1])

    def format_path(self):
        return self._format_path_internal("+empty+", self.path)

    def dump_head(self, f):
        final = 0
        if self.client is not None:
            for t in self.client.tables:
                if t is not self:
                    assert(t.client is None)
                    final += len(t.prefixes) + t.prefixes_inherited

        f.write(f"""
###### Table {self.index}
# Own prefixes: {len(self.prefixes)}
# Inherited: {self.prefixes_inherited}
# Final: {final if self.client is not None else "N/A"}
# Path: {self.path}
# Shorter: {'none' if self.shorter is None else self.shorter.index}
ipv6 table table_{self.index};
""")

    def dump_pipe(self, f):
        if self.shorter is None:
            return

        f.write(f"""
###### Shortener from {self.index} to {self.shorter.index}
# Long path: {self.path}
# Short path: {self.shorter.path}
# Prefixes: {len(self.prefixes) + self.prefixes_inherited}
protocol pipe shortener_{self.index}_{self.shorter.index} {'{'}
    import none;
    export filter stripfirst;
    max generation 96;
    table table_{self.index};
    peer table table_{self.shorter.index};
{'}'}
""")

    def dump_own(self, f):
        if len(self.prefixes) == 0:
            return

        f.write(f"""
###### Own prefixes for {self.index}
# Path: {self.path}
# Prefixes: {len(self.prefixes)}
protocol static own_{self.index} {"{"}
    ipv6 {"{"}
        import filter {"{"}
            bgp_path = {self.format_path()};
            accept;
        {"}"};
        table table_{self.index};
    {"}"};
""")
        for p in self.prefixes:
            f.write(f"    route {p.prefix}/{p.len} unreachable;\n")
        f.write("}\n")


class Suite:
    def pc(self, a, k):
        return int(self.base * (k * 2**a)**(-0.814))

    def all_prefixes(self):
        return set().union(*[ self.clients[c].prefixes for c in self.clients ])

    def table(self, path):
        if path not in self.tables:
            self.tables[path] = Table(path, len(self.tables), self.table(path[1:]) if len(path) > 1 else None)
        return self.tables[path]

    def __init__(self, base = 15000000, client_count = 15000):
        # Initialization of lists
        self.clients = dict()
        self.tests = dict()
        self.tables = dict()
        self.base = base

        self.probe = Client(self, 0xffffff, "probe")

        # Find the least level for the given base.
        # It's that one where f(L, 3) has minimal non-zero value
        self.levels = 0
        while self.pc(self.levels, 3) > 1:
            self.levels += 1

        while self.pc(self.levels, 3) < 1:
            self.levels -= 1

        # Find the size at the least level.
        kmax_init = 3
        while self.pc(self.levels, kmax_init + 1) > 0:
            kmax_init += 1

        # Seed the least level.
        self.add_test(self.levels, kmax_init)

        # Populate other levels.
        sizes = [ kmax_init ]

        for a in range(self.levels-1, -1, -1):
            # Find the next max size
            if sizes[-1] < client_count:
                l = sizes[-1]
                r = client_count*3
                while l < r-1:
                    m = (l + r) // 2
                    if self.pc(a, m) == 0:
                        r = m
                    else:
                        l = m
                assert(self.pc(a, l) > 0)
                assert(self.pc(a, r) == 0)
#                if l - sizes[-1] > 100:
#                    sizes.append(int((l * sizes[-1])**0.5))
                sizes.append(l)

            # Add tests until they are too big
            for s in sizes:
                self.add_test(a, s)
                if self.tests[(a,s)].total_routes() > self.base:
                    break

        print("Initialized")

    def add_test(self, a, kmax):
        print(f"Adding test a={a}, kmax={kmax}")
        clients = set()
        px = []
        populate = []

        p = int(sum([ self.pc(a,k) for k in range(1,kmax+1)]) / 2.5 + 0.5)
        p = max(p, self.pc(a,1))

        begin = time.time_ns()
        for k in range(1,kmax+1):
            if (a,k) in self.clients:
                c = self.clients[(a,k)]
                clients.add(c)
                px += list(c.prefixes)
            elif (k % 2 == 0) and (a+1, k//2) in self.clients:
                c = self.clients[(a+1,k//2)]
                self.clients[(a,k)] = c
                clients.add(c)
                px += list(c.prefixes)
            else:
                c = Client(self, len(self.clients) + 1, (a,k))
                self.clients[(a,k)] = c
                clients.add(c)
                populate.append((c, self.pc(a,k)))
        px = set(px)
        end = time.time_ns()

        print(f"Inspection with unions-by-list took {(end-begin) / 1_000_000_000} s.")

        print(f"Generating {p - len(px)} prefixes of {p} total needed")

        while len(px) < p:
            px.add(PrefixIPv6())

        print(f"Populating {len(populate)} clients")

        zero = [ p for p in px if len(p.last) == 0 ]
        one = [ p for p in px if len(p.last) == 1 ]
        two = [ p for p in px if len(p.last) == 2 ]
        others = [ p for p in px if len(p.last) > 2 ]

        print(f"Population input: zero {len(zero)}, one {len(one)}, two {len(two)}, others {len(others)}")

        for c, n in reversed(populate):
            onex = []
            twox = []
            otherx = []
            while n > 0 and len(zero) > 0:
                x = zero.pop()
                c.route(x)
                onex.append(x)
                n -= 1

            while n > 0 and len(one) > 0:
                x = one.pop()
                c.route(x)
                twox.append(x)
                n -= 1

            while n > 0 and len(two) > 0:
                x = two.pop()
                c.route(x)
                otherx.append(x)
                n -= 1

            while n > 0 and len(others) > 0:
                x = others.pop(0)
                c.route(x)
                otherx.append(x)
                n -= 1

            one += onex
            two += twox
            others += otherx

        t = Test(a, clients)
        t.clients.add(self.probe)
        self.tests[(a, kmax)] = t

    def dump(self, _dir):
        print("Generating unique client list")
        unique_clients = sorted(list(set(self.clients.values())), key=lambda c: c.ident) + [ self.probe ]

        for target in "amd", "arm":
            with open(f"{_dir}/feed-{target}.conf", "w") as f:
                print(f"Creating feeder configuration for {target}")
                f.write("""
router id 6666666;
log "/srv/rs-perf/feed.log" all;
threads 24;

protocol device {}

attribute int final;
filter stripfirst {
    if defined(final) then reject;
    bgp_path = delete(bgp_path, bgp_path.first);
    accept;
};
filter setfinal {
    final = 1;
    accept;
};
""")

                print(f"Dumping table declarations")
                for t in self.tables.values():
                    t.dump_head(f)

                print(f"Dumping table pipes")
                for t in self.tables.values():
                    t.dump_pipe(f)

                print(f"Dumping client pipes")
                for c in unique_clients:
                    c.dump_sources(f)

                print(f"Dumping prefixes")
                for t in self.tables.values():
                    t.dump_own(f)

                print(f"Dumping client BGPs")
                for c in unique_clients:
                    c.dump_bgp(f, target)

        print(f"Dumping {len(self.tests)} tests")
        for ti in self.tests:
            self.tests[ti].dump(_dir)


if __name__ == "__main__":
    n = len(sys.argv)
    if n > 4:
        print("Usage: prefgen.py [output-directory [max-routes [max-clients]]]\n\nDefault: \"conf\" 15000000 15000")
        sys.exit(2)

    client_count = 15000 if n < 4 else int(sys.argv[3])
    base = 15000000 if n < 3 else int(sys.argv[2])
    _dir = "conf" if n < 2 else sys.argv[1]
    Suite(base=base, client_count=client_count).dump(_dir)
