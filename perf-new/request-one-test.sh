#!/bin/bash

usage() {
  echo -e "Usage: $0 <type> <size> <threads> <version>"
  echo
  echo -e "e.g. $0 single-aux-best 11-2590 16 v2.15.1"
  echo -e "version\t\tcan be any git commit identifier"
  echo -e "threads\t\tare up to 32 (hopefully)"
  echo -e "size\t\tis a two-number string with a dash"
  echo -e "\t\tbefore dash: 0 to 13"
  echo -e "\t\tafter dash: 6, 11, 21, 41, 81, 162, 324, 648, 1295, 2590, 5179"
  echo -e "\t\tsome combinations are impossible, sorry"
  echo -e "type\t\tis (multi|single)-(aux|plain)-(best|secondary)"
}

sqlsan() {
  NAME="$1"
  VAR="$2"

  if echo ${VAR} | grep -aqv '^[a-z0-9.-]*$'; then
    echo "Forbidden characters in $NAME"
    echo
    usage
    exit 1
  fi

  if [ -z "$VAR" ]; then
    echo "Empty $NAME"
    echo
    usage
    exit 1
  fi
}

TYPE="$1"
SIZE="$2"
THREADS="$3"
VERSION="$4"

sqlsan TYPE "$TYPE"
sqlsan SIZE "$SIZE"
sqlsan THREADS "$THREADS"
sqlsan VERSION "$VERSION"

echo "insert into requests (_type, _size, threads, version) values ('${TYPE}', '${SIZE}', '${THREADS}', '${VERSION}')" | sqlite3 test-db.sqlite
