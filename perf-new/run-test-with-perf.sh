#!/bin/bash

set -e

insert_test=$(dirname $(readlink -f $0))/request-one-test.sh

taskstate() {
  systemctl show rs-perf-test | sed -n 's/^ActiveState=//p'
}

p=0
while [ $(taskstate) == "active" ]; do
  if [ $p -eq 0 ]; then
    now=$(date)
    echo -ne "${now}\t\tPrevious task active, waiting ..."
  fi
  printf "% 6i\b\b\b\b\b\b" $p
  p=$((p+1))
  sleep 1
done
echo

DIR=/srv/rs-perf/bird-perf-data-$(date +%s)
mkdir -p $DIR

$insert_test "$@"

while [ $(taskstate) != "active" ]; do
  if [ $p -eq 0 ]; then
    now=$(date)
    echo -ne "${now}\t\tNot started yet, waiting ..."
  fi
  printf "% 6i\b\b\b\b\b\b" $p
  p=$((p+1))
  sleep 1
done
echo

now=$(date)
echo -e "${now}\t\tStarting perf, outputs in $DIR"

pushd $DIR >/dev/null
perf record --call-graph=dwarf,8192 --switch-output=200M --pid=$(</run/rs-perf/test/bird.pid)
popd >/dev/null

chgrp -R labs $DIR
chmod -R g+r $DIR
chmod g+w $DIR

now=$(date)
echo -e "${now}\t\tDone, outputs in $DIR"
