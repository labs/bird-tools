#!/usr/bin/python3 -u

import asyncio
import contextlib
import datetime
from pathlib import Path
import psutil
import sqlite3
import traceback

class RTRFeederException(Exception):
    pass

class RTRFeeder:
    kinds = {
            "signed": {
                "bind": 18282,
                "metrics": 18882,
                },
            "irrdb": {
                "bind": 18283,
                "metrics": 18883,
                },
            }

    def __init__(self, runner, _type, kind):
        self.runner = runner

        if kind not in self.kinds:
            raise RTRFeederException(f"Unknown kind {kind}: known {list(self.kinds)}")

        self.conffile = self.runner.confdir.joinpath(f"roa-{_type}-{kind}.json")
        if not self.conffile.is_file():
            raise RTRFeederException(f"Config file for type {_type} kind {kind} not found")

        self.kind = kind

        self.logfile = self.runner.rundir.joinpath(f"roa-{kind}.log")

        self.proc = None

    async def run(self):
        print(f"rtr run {self.kind}")
        if self.runner.feeder[self.kind] is self:
            if self.proc is not None:
                return
            else:
                raise RTRFeederException(f"Should be running but isn't (kind {self.kind})")
        elif self.runner.feeder[self.kind] is not None:
            await self.runner.feeder[self.kind].stop()

        if self.runner.feeder[self.kind] is not None:
            raise RTRFeederException(f"Runner info not None (kind {self.kind})")

        self.runner.feeder[self.kind] = self

        logfh = self.logfile.open("w")
        self.proc = await asyncio.create_subprocess_exec(
            self.runner.bindir.joinpath("stayrtr"),
            "-cache", self.conffile,
            "-bind", f'[::1]:{self.kinds[self.kind]["bind"]}',
            "-metrics.addr", f'[::1]:{self.kinds[self.kind]["metrics"]}',
            "-checktime=false",
            stdout=logfh,
            stderr=logfh)

        pid = await self.wait_for_listening()

        if pid is None:
            print(f"RTR running for {self.kind}")
        else:
            print(f"RTR not running for {self.kind}, port used by pid {pid}")
            self.runner.feeder[self.kind] = None

    def check_running(self):
        if self.proc.returncode is not None:
            raise RTRFeederException(f"Stopped unexpectedly and returned {self.proc.returncode}")

    def get_listening(self):
        for c in psutil.net_connections():
            if c.status == "LISTEN" and c.laddr.ip == "::1" and c.laddr.port == self.kinds[self.kind]["bind"]:
                return c.pid


    async def wait_for_listening(self):
        while True:
            self.check_running()
            pid = self.get_listening()

            if pid == self.proc.pid:
                return None
            elif pid is not None:
                return pid

            with contextlib.suppress(asyncio.TimeoutError):
                await asyncio.wait_for(self.proc.wait(), 0.2)

    async def stop(self):
        if self.proc is None:
            raise RTRFeederException(f"Should be running but isn't (kind {kind})")

        self.proc.terminate()
        await self.proc.wait()
        self.runner.feeder[self.kind] = None


class TestException(Exception):
    pass

class Test:
    def __init__(self, runner, id, _type, size, threads, version):
        self.runner = runner
        self.id = id
        self._type = _type
        self.size = size

        self.conffile = runner.confdir.joinpath(f"test-amd-{_type}-{size}.conf")
        if not self.conffile.is_file():
            raise TestException(f"Config for type {_type} and size {size} not found")

        self.threads = threads
        self.validate_thread_count()

        self.proc = None
        self.version = version

    def validate_thread_count(self):
        if self.threads == "single":
            return

        if type(self.threads) is not int:
            raise TestException(f"Thread count must be a number")

        if self.threads < 2 or self.threads > 40:
            raise TestException(f"Thread count range is 2 to 40")

    async def cleanup(self):
        for _ in range(3):
            a = ( await self.get_systemd_state() )["ActiveState"]
            if a != "inactive":
                print(f"Cleaning up previous process in state {a}")
            if a == "inactive":
                return
            elif a == "failed":
                cmd = await asyncio.create_subprocess_exec( "systemctl", "reset-failed", "rs-perf-test" )
                res = await cmd.wait()
                if res != 0:
                    raise VersionException(f"systemctl reset-failed: { res }")
            else:
                cmd = await asyncio.create_subprocess_exec( "systemctl", "stop", "rs-perf-test" )
                res = await cmd.wait()
                if res != 0:
                    raise VersionException(f"systemctl stop: { res }")

        raise VersionException(f"systemctl process stuck in state {a}")

    async def run(self):
        print("running test")
        if self.runner.test is not None:
            raise TestException(f"Another test is running")

        self.runner.test = self

        mainconf = self.runner.rundir.joinpath("bird.conf")
        with mainconf.open(mode="w") as c:
            c.write(f"""
log "{ self.runner.rundir.joinpath('bird.log') }" all;

{ "" if self.threads == "single" else f"threads { self.threads };" }

protocol device {"{}"}

include "{ self.runner.filters }";
include "{ self.conffile }";
""")

        with self.conffile.open() as c:
            c.readline() # Clients:

            px = c.readline().split(": ")
            assert(len(px) == 2)
            assert(px[0] == "# Prefixes")
            px = int(px[1])

            rt = c.readline().split(": ")
            assert(len(rt) == 2)
            assert(rt[0] == "# Routes")
            rt = int(rt[1])

        logfh = self.runner.rundir.joinpath("bird.stderr").open("w")
        pidfile = self.runner.rundir.joinpath("bird.pid")
        self.ctlsock = self.runner.rundir.joinpath("bird.ctl")
        unit = await asyncio.create_subprocess_exec(
                "systemd-run",
                "--wait",
                "-d",
                "-p", "Type=forking",
                "-p", "CPUAccounting=yes",
                "-p", "MemoryAccounting=yes",
                "-p", "IPAccounting=yes",
                "-p", "LimitNOFILE=32786",
                "-p", "LimitCORE=90G",
                "--slice=rs-perf-test",
                "-u", "rs-perf-test",
                self.version.binary,
                "-c", mainconf,
                "-P", pidfile,
                "-s", self.ctlsock,
                "-D", "bird.debug",
                stdout=logfh,
                stderr=logfh)

#        if (result := await unit.wait()) != 0:
#            raise TestException(f"Systemd unit failed to run: { result }")

        loop = asyncio.get_running_loop()
        self.start_time = loop.time()

        print(f"Test started at { datetime.datetime.now().strftime('%c') }")
        finished = loop.create_future()
        self.sys_stats = []
        self.systemd_state_task = asyncio.create_task(self.check_systemd(unit, finished))

        self.rt_stats = []
        self.probe_state_task = asyncio.create_task(self.check_probe(rt, px))

        try:
            await asyncio.gather(self.systemd_state_task, self.probe_state_task)
        except Exception as e:
            for t in (self.systemd_state_task, self.probe_state_task):
                if not t.done() and not t.cancelled():
                    t.cancel()
            raise e

        cpumem = await finished
        stop_time = self.rt_stats[-1]["time"]

        while True:
            try:
                print(f"Inserting data into DB")
                self.runner.db.executemany("INSERT INTO probe_data (id, time, prefixes, routes) VALUES (?, ?, ?, ?)",
                        [ (self.id, r["time"], r["px"], r["rt"]) for r in self.rt_stats ])
                self.runner.db.executemany("INSERT INTO system_data (id, time, cpu_ns, memory) VALUES (?, ?, ?, ?)",
                        [ (self.id, r["time"], r["cpu"], r["mem"]) for r in self.sys_stats ])
                self.runner.db.execute("INSERT INTO results (id, cpu_ns, memory, time_ns, prefixes, routes) VALUES (?, ?, ?, ?, ?, ?)", (self.id, cpumem["cpu"], cpumem["mem"], stop_time, px, rt))
                self.runner.db_con.commit()
            except sqlite3.OperationalError as e:
                print(f"SQLite Operational Error: {e}, retry")
                self.runner.db_con.rollback()
                await asyncio.sleep(1)
                continue

            break

        while True:
            now = loop.time()
            probe_state = await self.get_probe_state()
            if probe_state["px"] == 0 and probe_state["rt"] == 0:
                self.runner.test = None
                return
            else:
                print(f"Probe still sees { probe_state['rt'] } routes for { probe_state['px'] } prefixes")

            after = loop.time()
            if after - now < 0.2:
                await asyncio.sleep(now + 0.3 - after)

    async def get_systemd_state(self):
        systemd_show = await asyncio.create_subprocess_exec(
                "systemctl", "show", "rs-perf-test",
                stdout=asyncio.subprocess.PIPE )

        mem = None
        cpu = None
        active = None
        stdout, _ = await systemd_show.communicate()
        lines = stdout.decode().split('\n')
#        print(f"{ len(lines) } of type { type(lines) }")

        state = {}

        for line in lines:
            if "=" not in line:
                #               print(f"no = in line: {line}")
                continue

            name, value = line.split("=", maxsplit=1)
            state[name] = value

        return state

    async def check_systemd(self, unit, finished):
        await asyncio.sleep(0.3)

        loop = asyncio.get_running_loop()
        maxmem = 0
        maxcpu = 0
        while True:
            now = loop.time()
            print("systemd check")
            state = await self.get_systemd_state()

            if (active := state["ActiveState"]) == "inactive":
                finished.set_result({ "cpu": maxcpu, "mem": maxmem })
                if (result := await unit.wait()) == 0:
                    return

                raise TestException(f"systemd unit ended unexpectedly: {result}")
            elif active == "failed":
                result = await unit.wait()
                raise TestException(f"systemd unit ended unexpectedly: {result}")

            mem = int(state["MemoryCurrent"])
            cpu = int(state["CPUUsageNSec"])

            self.sys_stats.append({
                "time": int((now - self.start_time) * 1000000000),
                "mem": mem,
                "cpu": cpu,
                })

            if mem > maxmem:
                maxmem = mem
            if cpu > maxcpu:
                maxcpu = cpu

            print(f"{ now - self.start_time } | Current usage: { mem } of memory, { cpu / 1e9 } of CPU")

            after = loop.time()
            if unit.returncode is not None:
                raise TestException(f"Systemd failed: {unit.returncode}")

            if after - now < 0.2:
                await asyncio.sleep(now + 0.3 - after)

    async def check_probe(self, rt, px):
        # Minimal convergence time of 1 seconds is allowed
        await asyncio.sleep(1)

        loop = asyncio.get_running_loop()
        while True:
            now = loop.time()
            probe_state = await self.get_probe_state()

            print(f"{ now - self.start_time } | { probe_state['rt'] } of { rt } routes for { probe_state['px'] } of { px } prefixes")
            probe_state["time"] = int((now - self.start_time) * 1000000000)
            self.rt_stats.append(probe_state)

            if probe_state["time"] > 3600 * 1000000000:
                raise TestException(f"BIRD failed to converge")

            if probe_state["px"] == px and probe_state["rt"] == rt:
                stop = await asyncio.create_subprocess_exec(
                        "birdc",
                        "-s", self.ctlsock,
                        "down")

                if result := await stop.wait() != 0:
                    raise TestException(f"BIRD failed to stop: {result}")

                return

            after = loop.time()
            if after - now < 0.2:
                await asyncio.sleep(now + 0.3 - after)

            for kind in self.runner.feeder:
                self.runner.feeder[kind].check_running()

    async def get_probe_state(self):
        loop = asyncio.get_running_loop()
        probe_state_fut = loop.create_future()
        birdc, birdc_proto = await loop.create_connection(
                lambda: TestProbeChecker(probe_state_fut),
                '2001:1488:ac15:ff90::134', 18555)

        try:
            probe_state = await probe_state_fut
        finally:
            birdc.close()
        return probe_state


class TestProbeChecker(asyncio.Protocol):
    def __init__(self, probe_state):
        self.probe_state = probe_state

    def data_received(self, data):
#        print(f"data received: {data}")
        d = data.decode().split('\n')
        assert(len(d) == 3)
        l = d[1].split(' ')
        assert(len(l) == 10)
        rtA = int(l[0])
        rtB = int(l[2])
        assert(rtA == rtB)
        px = int(l[5])
        self.probe_state.set_result({ "rt": rtA, "px": px })

class VersionException(Exception):
    pass

class Version:
    def __init__(self, runner, revision):
        self.runner = runner

        assert(revision not in self.runner.versions)
        self.runner.versions[revision] = self

        self.revision = revision

    async def make(self):
        self.binary = self.runner.bindir.joinpath(f"bird-{self.revision}")
        if self.binary.is_file():
            return

        print(f"Building version {self.revision}")

        gitdir = self.runner.bindir.joinpath(f"bird")
        assert(gitdir.is_dir())

        cmd = await asyncio.create_subprocess_exec(
                "git", "clean", "-fdx",
                cwd=gitdir)
        res = await cmd.wait()
        if res != 0:
            raise VersionException(f"git clean: { res }")

        cmd = await asyncio.create_subprocess_exec(
                "git", "fetch", "origin",
                cwd=gitdir)
        res = await cmd.wait()
        if res != 0:
            raise VersionException(f"git fetch: { res }")

        cmd = await asyncio.create_subprocess_exec(
                "git", "checkout", "-f", self.revision,
                cwd=gitdir)
        res = await cmd.wait()
        if res != 0:
            raise VersionException(f"git checkout: { res }")

        cmd = await asyncio.create_subprocess_exec(
                "autoreconf", "-i",
                cwd=gitdir)
        res = await cmd.wait()
        if res != 0:
            raise VersionException(f"autoreconf: { res }")

        cmd = await asyncio.create_subprocess_exec(
                "./configure",
                cwd=gitdir)
        res = await cmd.wait()
        if res != 0:
            raise VersionException(f"configure: { res }")

        cmd = await asyncio.create_subprocess_exec(
                "make",
                cwd=gitdir)
        res = await cmd.wait()
        if res != 0:
            raise VersionException(f"make: { res }")

        gitdir.joinpath("bird").rename(self.binary)


class Runner:
    def __init__(self, rundir="/run/rs-perf/test", bindir="/srv/rs-perf/bin", confdir="/srv/rs-perf/conf", db="/srv/rs-perf/test-db.sqlite", filters="/srv/rs-perf/rsfilters.conf"):
        self.rundir = Path(rundir)
        self.rundir.mkdir(parents=True, exist_ok=True)

        self.confdir = Path(confdir)
        assert(self.confdir.is_dir())

        self.bindir = Path(bindir)
        assert(self.bindir.is_dir())

        self.filters = Path(filters)
        assert(self.filters.is_file())

        self.feeder = {
                "irrdb": None,
                "signed": None,
                }
        self.test = None

        self.db_con = sqlite3.connect(db)
        self.db = self.db_con.cursor()

        self.versions = {}
        self.rtr_feeders = {}

    def get_version(self, revision):
        if revision in self.versions:
            return self.versions[revision]
        else:
            return Version(self, revision)

    def get_feeder(self, _type, kind):
        if (k := (_type, kind)) not in self.rtr_feeders:
            self.rtr_feeders[k] = RTRFeeder(runner=self, _type=_type, kind=kind)
        return self.rtr_feeders[k]

    async def run(self):
        last_rtr_type = None

        try:
            while True:
                row = list(self.db.execute("SELECT requests.id, _type, _size, threads, version FROM requests LEFT JOIN results ON (requests.id == results.id ) WHERE results.id is NULL ORDER BY requests.id LIMIT 1"))
                if len(row) == 0:
                    await asyncio.sleep(1)
                    continue

                assert(len(row) == 1)
                print(f"Found a row! { row }")
                row = row[0]

                version = self.get_version(row[4])
                rtr_type = row[2]

                prepare = [ version.make() ]

                if last_rtr_type != rtr_type:
                    for kind in "irrdb", "signed":
                        prepare.append(self.get_feeder(rtr_type, kind).run())

                try:
                    results = await asyncio.gather(*prepare)
                    print(f"Test prepared")
                except VersionException as e:
                    assert(self.test is None)
                    print(f"Preparation failed: { e }")
                    traceback.print_tb(e.__traceback__)
                    print(f"(end of traceback)")
                    print(f"marking test {row[0]} as bad")
                    self.db.execute("INSERT INTO results (id, cpu_ns, memory, time_ns, prefixes, routes) VALUES (?, ?, ?, ?, ?, ?)", (row[0], 0, 0, 0, 0, 0))
                    self.db_con.commit()
                    continue

                for kind in "irrdb", "signed":
                    if self.feeder[kind] is None:
                        raise RTRFeederException("RTR failed to start")

                try:
                    t = Test(runner=self, id=row[0], _type=row[1], size=row[2], threads=row[3], version=version)
                    await t.cleanup()
                    await t.run()
                    print(f"Test finished")
                except TestException as e:
                    print(f"Test failed: { e }")
                    traceback.print_tb(e.__traceback__)
                    print(f"(end of traceback)")
                    self.test.probe_state_task.cancel()
                    self.test.systemd_state_task.cancel()
                    print(f"marking test {self.test.id} as bad")
                    self.db.execute("INSERT INTO results (id, cpu_ns, memory, time_ns, prefixes, routes) VALUES (?, ?, ?, ?, ?, ?)", (self.test.id, 0, 0, 0, 0, 0))
                    self.db_con.commit()
                    self.test = None
        finally:
            for kind in "irrdb", "signed":
                if self.feeder[kind] is not None:
                    await self.feeder[kind].stop()

if __name__ == '__main__':
    asyncio.run(Runner().run())
